/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.message.ClientMessageSubscriber;
import au.id.adamallister.message.messages.ChatMessage;
import au.id.adamallister.message.messages.HandShakeMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientMessageSubscriberTest {
  
  public ClientMessageSubscriberTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of clientMessage method, of class ClientMessageSubscriber.
   */
  @Test
  public void testClientMessage() {
    System.out.println("clientMessage");
    ChatMessage chatMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientMessageSubscriber instance = new ClientMessageSubscriberImpl();
    instance.clientMessage(chatMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheck method, of class ClientMessageSubscriber.
   */
  @Test
  public void testClientHealthCheck() {
    System.out.println("clientHealthCheck");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientMessageSubscriber instance = new ClientMessageSubscriberImpl();
    instance.clientHealthCheck(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheckFail method, of class ClientMessageSubscriber.
   */
  @Test
  public void testClientHealthCheckFail() {
    System.out.println("clientHealthCheckFail");
    InetAddress address = null;
    int port = 0;
    ClientMessageSubscriber instance = new ClientMessageSubscriberImpl();
    instance.clientHealthCheckFail(address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheckResponse method, of class ClientMessageSubscriber.
   */
  @Test
  public void testClientHealthCheckResponse() {
    System.out.println("clientHealthCheckResponse");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientMessageSubscriber instance = new ClientMessageSubscriberImpl();
    instance.clientHealthCheckResponse(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHandShake method, of class ClientMessageSubscriber.
   */
  @Test
  public void testClientHandShake() {
    System.out.println("clientHandShake");
    HandShakeMessage hndMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientMessageSubscriber instance = new ClientMessageSubscriberImpl();
    instance.clientHandShake(hndMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class ClientMessageSubscriberImpl implements ClientMessageSubscriber {

    public void clientMessage(ChatMessage chatMsg, InetAddress address, int port) {
    }

    public void clientHealthCheck(HealthCheckMessage hckMsg, InetAddress address, int port) {
    }

    public void clientHealthCheckFail(InetAddress address, int port) {
    }

    public void clientHealthCheckResponse(HealthCheckMessage hckMsg, InetAddress address, int port) {
    }

    public void clientHandShake(HandShakeMessage hndMsg, InetAddress address, int port) {
    }
  }
  
}
