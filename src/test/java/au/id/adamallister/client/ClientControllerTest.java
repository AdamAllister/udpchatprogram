/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.message.messages.ChatMessage;
import au.id.adamallister.message.messages.ChatRequestMessage;
import au.id.adamallister.message.messages.ChatRequestResponseMessage;
import au.id.adamallister.message.messages.HandShakeMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import au.id.adamallister.message.messages.ClientRegistrationMessage;
import java.net.InetAddress;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientControllerTest {
  
  public ClientControllerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of createChat method, of class ClientController.
   */
  @Test
  public void testCreateChat() {
    System.out.println("createChat");
    ArrayList<String> clientUsernames = null;
    ClientController instance = null;
    String expResult = "";
    String result = instance.createChat(clientUsernames);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of sendChatMessage method, of class ClientController.
   */
  @Test
  public void testSendChatMessage() {
    System.out.println("sendChatMessage");
    String chatID = "";
    String msg = "";
    ClientController instance = null;
    instance.sendChatMessage(chatID, msg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getChat method, of class ClientController.
   */
  @Test
  public void testGetChat() {
    System.out.println("getChat");
    String chatID = "";
    ClientController instance = null;
    Chat expResult = null;
    Chat result = instance.getChat(chatID);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of isRegistered method, of class ClientController.
   */
  @Test
  public void testIsRegistered() {
    System.out.println("isRegistered");
    ClientController instance = null;
    boolean expResult = false;
    boolean result = instance.isRegistered();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientMessage method, of class ClientController.
   */
  @Test
  public void testClientMessage() {
    System.out.println("clientMessage");
    ChatMessage chatMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientController instance = null;
    instance.clientMessage(chatMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheck method, of class ClientController.
   */
  @Test
  public void testClientHealthCheck() {
    System.out.println("clientHealthCheck");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientController instance = null;
    instance.clientHealthCheck(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheckFail method, of class ClientController.
   */
  @Test
  public void testClientHealthCheckFail() {
    System.out.println("clientHealthCheckFail");
    InetAddress serverAddress = null;
    int serverPort = 0;
    ClientController instance = null;
    instance.clientHealthCheckFail(serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheckResponse method, of class ClientController.
   */
  @Test
  public void testClientHealthCheckResponse() {
    System.out.println("clientHealthCheckResponse");
    HealthCheckMessage hckMsg = null;
    InetAddress serverAddress = null;
    int serverPort = 0;
    ClientController instance = null;
    instance.clientHealthCheckResponse(hckMsg, serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHandShake method, of class ClientController.
   */
  @Test
  public void testClientHandShake() {
    System.out.println("clientHandShake");
    HandShakeMessage hndMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientController instance = null;
    instance.clientHandShake(hndMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverRegistration method, of class ClientController.
   */
  @Test
  public void testServerRegistration() {
    System.out.println("serverRegistration");
    ClientRegistrationMessage regMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientController instance = null;
    instance.serverRegistration(regMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverChatRequest method, of class ClientController.
   */
  @Test
  public void testServerChatRequest() {
    System.out.println("serverChatRequest");
    ChatRequestMessage rqstMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientController instance = null;
    instance.serverChatRequest(rqstMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverChatRequestResponse method, of class ClientController.
   */
  @Test
  public void testServerChatRequestResponse() {
    System.out.println("serverChatRequestResponse");
    ChatRequestResponseMessage rspnMsg = null;
    InetAddress address = null;
    int port = 0;
    ClientController instance = null;
    instance.serverChatRequestResponse(rspnMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheck method, of class ClientController.
   */
  @Test
  public void testServerHealthCheck() {
    System.out.println("serverHealthCheck");
    HealthCheckMessage hckMsg = null;
    InetAddress serverAddress = null;
    int serverPort = 0;
    ClientController instance = null;
    instance.serverHealthCheck(hckMsg, serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheckFail method, of class ClientController.
   */
  @Test
  public void testServerHealthCheckFail() {
    System.out.println("serverHealthCheckFail");
    InetAddress serverAddress = null;
    int serverPort = 0;
    ClientController instance = null;
    instance.serverHealthCheckFail(serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheckResponse method, of class ClientController.
   */
  @Test
  public void testServerHealthCheckResponse() {
    System.out.println("serverHealthCheckResponse");
    HealthCheckMessage hckMsg = null;
    InetAddress serverAddress = null;
    int serverPort = 0;
    ClientController instance = null;
    instance.serverHealthCheckResponse(hckMsg, serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of connectToServer method, of class ClientController.
   */
  @Test
  public void testConnectToServer() {
    System.out.println("connectToServer");
    InetAddress serverIP = null;
    int port = 0;
    String aUsername = "";
    ClientController instance = null;
    instance.connectToServer(serverIP, port, aUsername);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
