/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.packet.SessionEndpoint;
import java.net.InetAddress;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientTest {
  
  public ClientTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of getUsername method, of class Client.
   */
  @Test
  public void testGetUsername() {
    System.out.println("getUsername");
    Client instance = null;
    String expResult = "";
    String result = instance.getUsername();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of isConnected method, of class Client.
   */
  @Test
  public void testIsConnected() {
    System.out.println("isConnected");
    Client instance = null;
    boolean expResult = false;
    boolean result = instance.isConnected();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of hasEndpoint method, of class Client.
   */
  @Test
  public void testHasEndpoint() {
    System.out.println("hasEndpoint");
    InetAddress address = null;
    int port = 0;
    Client instance = null;
    boolean expResult = false;
    boolean result = instance.hasEndpoint(address, port);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getAddress method, of class Client.
   */
  @Test
  public void testGetAddress() {
    System.out.println("getAddress");
    Client instance = null;
    InetAddress expResult = null;
    InetAddress result = instance.getAddress();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getPort method, of class Client.
   */
  @Test
  public void testGetPort() {
    System.out.println("getPort");
    Client instance = null;
    int expResult = 0;
    int result = instance.getPort();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of setUsername method, of class Client.
   */
  @Test
  public void testSetUsername() {
    System.out.println("setUsername");
    String name = "";
    Client instance = null;
    instance.setUsername(name);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of healthFail method, of class Client.
   */
  @Test
  public void testHealthFail() {
    System.out.println("healthFail");
    Client instance = null;
    boolean expResult = false;
    boolean result = instance.healthFail();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of healthSuccess method, of class Client.
   */
  @Test
  public void testHealthSuccess() {
    System.out.println("healthSuccess");
    Client instance = null;
    instance.healthSuccess();
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getHealth method, of class Client.
   */
  @Test
  public void testGetHealth() {
    System.out.println("getHealth");
    Client instance = null;
    short expResult = 0;
    short result = instance.getHealth();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of setOnline method, of class Client.
   */
  @Test
  public void testSetOnline() {
    System.out.println("setOnline");
    boolean aOnline = false;
    Client instance = null;
    instance.setOnline(aOnline);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of isOnline method, of class Client.
   */
  @Test
  public void testIsOnline() {
    System.out.println("isOnline");
    Client instance = null;
    boolean expResult = false;
    boolean result = instance.isOnline();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of setEndpoints method, of class Client.
   */
  @Test
  public void testSetEndpoints() {
    System.out.println("setEndpoints");
    ArrayList<SessionEndpoint> enpoints = null;
    Client instance = null;
    instance.setEndpoints(enpoints);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getEndpoints method, of class Client.
   */
  @Test
  public void testGetEndpoints() {
    System.out.println("getEndpoints");
    Client instance = null;
    ArrayList<SessionEndpoint> expResult = null;
    ArrayList<SessionEndpoint> result = instance.getEndpoints();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of isCurrentEndpoint method, of class Client.
   */
  @Test
  public void testIsCurrentEndpoint() {
    System.out.println("isCurrentEndpoint");
    InetAddress address = null;
    int port = 0;
    Client instance = null;
    boolean expResult = false;
    boolean result = instance.isCurrentEndpoint(address, port);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of setConnected method, of class Client.
   */
  @Test
  public void testSetConnected() {
    System.out.println("setConnected");
    boolean isConnected = false;
    InetAddress address = null;
    int port = 0;
    Client instance = null;
    instance.setConnected(isConnected, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
