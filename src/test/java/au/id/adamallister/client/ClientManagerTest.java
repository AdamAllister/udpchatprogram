/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientManagerTest {
  
  public ClientManagerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of getClient method, of class ClientManager.
   */
  @Test
  public void testGetClient_String() {
    System.out.println("getClient");
    String username = "";
    ClientManager instance = new ClientManager();
    Client expResult = null;
    Client result = instance.getClient(username);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getClient method, of class ClientManager.
   */
  @Test
  public void testGetClient_InetAddress_int() {
    System.out.println("getClient");
    InetAddress address = null;
    int port = 0;
    ClientManager instance = new ClientManager();
    Client expResult = null;
    Client result = instance.getClient(address, port);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of hasClient method, of class ClientManager.
   */
  @Test
  public void testHasClient_InetAddress_int() {
    System.out.println("hasClient");
    InetAddress address = null;
    int port = 0;
    ClientManager instance = new ClientManager();
    boolean expResult = false;
    boolean result = instance.hasClient(address, port);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of hasClient method, of class ClientManager.
   */
  @Test
  public void testHasClient_Client() {
    System.out.println("hasClient");
    Client client = null;
    ClientManager instance = new ClientManager();
    boolean expResult = false;
    boolean result = instance.hasClient(client);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of hasClient method, of class ClientManager.
   */
  @Test
  public void testHasClient_String() {
    System.out.println("hasClient");
    String username = "";
    ClientManager instance = new ClientManager();
    boolean expResult = false;
    boolean result = instance.hasClient(username);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getClients method, of class ClientManager.
   */
  @Test
  public void testGetClients() {
    System.out.println("getClients");
    ClientManager instance = new ClientManager();
    Collection<Client> expResult = null;
    Collection<Client> result = instance.getClients();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of addClients method, of class ClientManager.
   */
  @Test
  public void testAddClients() {
    System.out.println("addClients");
    ArrayList<Client> aClients = null;
    ClientManager instance = new ClientManager();
    instance.addClients(aClients);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of addClient method, of class ClientManager.
   */
  @Test
  public void testAddClient() {
    System.out.println("addClient");
    Client client = null;
    ClientManager instance = new ClientManager();
    instance.addClient(client);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of removeClients method, of class ClientManager.
   */
  @Test
  public void testRemoveClients() {
    System.out.println("removeClients");
    ArrayList<Client> aClients = null;
    ClientManager instance = new ClientManager();
    instance.removeClients(aClients);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of removeClient method, of class ClientManager.
   */
  @Test
  public void testRemoveClient() {
    System.out.println("removeClient");
    Client client = null;
    ClientManager instance = new ClientManager();
    instance.removeClient(client);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clear method, of class ClientManager.
   */
  @Test
  public void testClear() {
    System.out.println("clear");
    ClientManager instance = new ClientManager();
    instance.clear();
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
