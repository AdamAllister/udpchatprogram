/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({au.id.adamallister.client.ChatTest.class, au.id.adamallister.client.ClientManagerTest.class, au.id.adamallister.client.ClientControllerTest.class, au.id.adamallister.client.ServerConnectionTest.class, au.id.adamallister.client.ClientTest.class, au.id.adamallister.client.ClientChatManagerTestMainTest.class, au.id.adamallister.client.ClientMainTest.class, au.id.adamallister.client.TestSubscriberTest.class})
public class ClientSuite {

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
}
