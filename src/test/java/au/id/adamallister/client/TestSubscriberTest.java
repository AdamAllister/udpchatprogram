/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.message.messages.ChatMessage;
import au.id.adamallister.message.messages.HandShakeMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class TestSubscriberTest {
  
  public TestSubscriberTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of clientMessage method, of class TestSubscriber.
   */
  @Test
  public void testClientMessage() {
    System.out.println("clientMessage");
    ChatMessage chatMsg = null;
    InetAddress address = null;
    int port = 0;
    TestSubscriber instance = new TestSubscriber();
    instance.clientMessage(chatMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheck method, of class TestSubscriber.
   */
  @Test
  public void testClientHealthCheck() {
    System.out.println("clientHealthCheck");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    TestSubscriber instance = new TestSubscriber();
    instance.clientHealthCheck(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheckFail method, of class TestSubscriber.
   */
  @Test
  public void testClientHealthCheckFail() {
    System.out.println("clientHealthCheckFail");
    InetAddress serverAddress = null;
    int serverPort = 0;
    TestSubscriber instance = new TestSubscriber();
    instance.clientHealthCheckFail(serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHealthCheckResponse method, of class TestSubscriber.
   */
  @Test
  public void testClientHealthCheckResponse() {
    System.out.println("clientHealthCheckResponse");
    HealthCheckMessage hckMsg = null;
    InetAddress serverAddress = null;
    int serverPort = 0;
    TestSubscriber instance = new TestSubscriber();
    instance.clientHealthCheckResponse(hckMsg, serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientHandShake method, of class TestSubscriber.
   */
  @Test
  public void testClientHandShake() {
    System.out.println("clientHandShake");
    HandShakeMessage hndMsg = null;
    InetAddress address = null;
    int port = 0;
    TestSubscriber instance = new TestSubscriber();
    instance.clientHandShake(hndMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
