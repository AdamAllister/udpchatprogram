/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.message.messages.ChatMessage;
import au.id.adamallister.util.UUIDWrapper;
import java.util.ArrayList;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ChatTest {
  
  public ChatTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of addClient method, of class Chat.
   */
  @Test
  public void testAddClient() {
    System.out.println("addClient");
    Client client = null;
    Chat instance = null;
    instance.addClient(client);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of removeClient method, of class Chat.
   */
  @Test
  public void testRemoveClient() {
    System.out.println("removeClient");
    Client client = null;
    Chat instance = null;
    instance.removeClient(client);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of disconnectedClient method, of class Chat.
   */
  @Test
  public void testDisconnectedClient() {
    System.out.println("disconnectedClient");
    Client client = null;
    Chat instance = null;
    instance.disconnectedClient(client);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getClientManager method, of class Chat.
   */
  @Test
  public void testGetClientManager() {
    System.out.println("getClientManager");
    Chat instance = null;
    ClientManager expResult = null;
    ClientManager result = instance.getClientManager();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getClients method, of class Chat.
   */
  @Test
  public void testGetClients() {
    System.out.println("getClients");
    Chat instance = null;
    ArrayList<Client> expResult = null;
    ArrayList<Client> result = instance.getClients();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getClientsAsString method, of class Chat.
   */
  @Test
  public void testGetClientsAsString() {
    System.out.println("getClientsAsString");
    Chat instance = null;
    String expResult = "";
    String result = instance.getClientsAsString();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getName method, of class Chat.
   */
  @Test
  public void testGetName() {
    System.out.println("getName");
    Chat instance = null;
    String expResult = "";
    String result = instance.getName();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of addMessage method, of class Chat.
   */
  @Test
  public void testAddMessage() {
    System.out.println("addMessage");
    ChatMessage newMsg = null;
    Chat instance = null;
    instance.addMessage(newMsg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getID method, of class Chat.
   */
  @Test
  public void testGetID() {
    System.out.println("getID");
    Chat instance = null;
    UUIDWrapper expResult = null;
    UUIDWrapper result = instance.getID();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
