/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({au.id.adamallister.client.view.ClientViewTest.class, au.id.adamallister.client.view.ClientGUITest.class, au.id.adamallister.client.view.SplitPanePanelTest.class, au.id.adamallister.client.view.ChatTabPanelTest.class, au.id.adamallister.client.view.ChatPanelTest.class, au.id.adamallister.client.view.WelcomePanelTest.class})
public class ViewSuite {

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
}
