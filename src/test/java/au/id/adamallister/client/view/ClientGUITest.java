/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientGUITest {
  
  public ClientGUITest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of chatUpdate method, of class ClientGUI.
   */
  @Test
  public void testChatUpdate() {
    System.out.println("chatUpdate");
    int index = 0;
    String formattedMessage = "";
    String chatId = "";
    ClientGUI instance = new ClientGUI();
    instance.chatUpdate(index, formattedMessage, chatId);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientsUpdate method, of class ClientGUI.
   */
  @Test
  public void testClientsUpdate() {
    System.out.println("clientsUpdate");
    ArrayList<String> clientsOnline = null;
    ClientGUI instance = new ClientGUI();
    instance.clientsUpdate(clientsOnline);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverUpdate method, of class ClientGUI.
   */
  @Test
  public void testServerUpdate() {
    System.out.println("serverUpdate");
    boolean connected = false;
    String error = "";
    ClientGUI instance = new ClientGUI();
    instance.serverUpdate(connected, error);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of sendMsg method, of class ClientGUI.
   */
  @Test
  public void testSendMsg() {
    System.out.println("sendMsg");
    String chatID = "";
    String msg = "";
    ClientGUI instance = new ClientGUI();
    instance.sendMsg(chatID, msg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of createChat method, of class ClientGUI.
   */
  @Test
  public void testCreateChat() {
    System.out.println("createChat");
    ArrayList<String> clientAddressPorts = null;
    ClientGUI instance = new ClientGUI();
    instance.createChat(clientAddressPorts);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of main method, of class ClientGUI.
   */
  @Test
  public void testMain() {
    System.out.println("main");
    String[] args = null;
    ClientGUI.main(args);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of registrationStatus method, of class ClientGUI.
   */
  @Test
  public void testRegistrationStatus() {
    System.out.println("registrationStatus");
    String error = "";
    ClientGUI instance = new ClientGUI();
    instance.registrationStatus(error);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
