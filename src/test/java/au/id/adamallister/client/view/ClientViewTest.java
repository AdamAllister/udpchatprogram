/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientViewTest {
  
  public ClientViewTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of chatUpdate method, of class ClientView.
   */
  @Test
  public void testChatUpdate() {
    System.out.println("chatUpdate");
    int index = 0;
    String formattedMessage = "";
    String chatId = "";
    ClientView instance = new ClientViewImpl();
    instance.chatUpdate(index, formattedMessage, chatId);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of clientsUpdate method, of class ClientView.
   */
  @Test
  public void testClientsUpdate() {
    System.out.println("clientsUpdate");
    ArrayList<String> clientsOnline = null;
    ClientView instance = new ClientViewImpl();
    instance.clientsUpdate(clientsOnline);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverUpdate method, of class ClientView.
   */
  @Test
  public void testServerUpdate() {
    System.out.println("serverUpdate");
    boolean connected = false;
    String error = "";
    ClientView instance = new ClientViewImpl();
    instance.serverUpdate(connected, error);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of sendMsg method, of class ClientView.
   */
  @Test
  public void testSendMsg() {
    System.out.println("sendMsg");
    String chatID = "";
    String msg = "";
    ClientView instance = new ClientViewImpl();
    instance.sendMsg(chatID, msg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of createChat method, of class ClientView.
   */
  @Test
  public void testCreateChat() {
    System.out.println("createChat");
    ArrayList<String> clientAddressPorts = null;
    ClientView instance = new ClientViewImpl();
    instance.createChat(clientAddressPorts);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }
  }

  /**
   * Test of registrationStatus method, of class ClientView.
   */
  @Test
  public void testRegistrationStatus() {
    System.out.println("registrationStatus");
    String error = "";
    ClientView instance = new ClientViewImpl();
    instance.registrationStatus(error);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }

    public void registrationStatus(String error) {
    }
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }

    public void registrationStatus(String error) {
    }
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }

    public void registrationStatus(String error) {
    }
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }

    public void registrationStatus(String error) {
    }
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }

    public void registrationStatus(String error) {
    }
  }

  public class ClientViewImpl implements ClientView {

    public void chatUpdate(int index, String formattedMessage, String chatId) {
    }

    public void clientsUpdate(ArrayList<String> clientsOnline) {
    }

    public void serverUpdate(boolean connected, String error) {
    }

    public void sendMsg(String chatID, String msg) {
    }

    public void createChat(ArrayList<String> clientAddressPorts) {
    }

    public void registrationStatus(String error) {
    }
  }
  
}
