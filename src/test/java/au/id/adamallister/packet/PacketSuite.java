/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({au.id.adamallister.packet.PacketReceiverTest.class, au.id.adamallister.packet.PayloadTest.class, au.id.adamallister.packet.PacketUtilityTest.class, au.id.adamallister.packet.SessionEndpointTest.class, au.id.adamallister.packet.PacketTest.class, au.id.adamallister.packet.PacketTimerTest.class, au.id.adamallister.packet.PacketGroupTest.class, au.id.adamallister.packet.PacketManagerTest.class})
public class PacketSuite {

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
}
