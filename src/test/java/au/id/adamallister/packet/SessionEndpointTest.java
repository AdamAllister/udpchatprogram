/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class SessionEndpointTest {
  
  private SessionEndpoint testGoodEndpoint;
  private InetAddress testGoodAddress;
  private int testGoodPort;
  
  public SessionEndpointTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testGoodPort = 57331;
    testGoodAddress = mock(InetAddress.class);

    testGoodEndpoint = new SessionEndpoint(testGoodAddress, testGoodPort);
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of ip method, of class SessionEndpoint.
   */
  @Test
  public void testIp() {
    System.out.println("ip");
    SessionEndpoint instance = testGoodEndpoint;
    InetAddress expResult = testGoodAddress;
    InetAddress result = instance.ip();
    assertEquals(expResult, result);
  }

  /**
   * Test of port method, of class SessionEndpoint.
   */
  @Test
  public void testPort() {
    System.out.println("port");
    SessionEndpoint instance = testGoodEndpoint;
    int expResult = testGoodPort;
    int result = instance.port();
    assertEquals(expResult, result);
  }
  
}
