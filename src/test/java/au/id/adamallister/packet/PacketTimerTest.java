/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketTimerTest {
  
  private static PacketTimer testTimer;
  private static ExpirationObserver testExpired;
  
  public PacketTimerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
    testExpired = new ExpirationObserver();
    testTimer = new PacketTimer(5L, testExpired);
    testTimer.start();
  }
  
  @AfterClass
  public static void tearDownClass() {
    testTimer.kill();
    testTimer = null;
    testExpired = null;
  }
  
  @Before
  public void setUp() {
    testExpired.reset();
  }
  
  @After
  public void tearDown() {
    
  }

  /**
   * Test of run method, of class PacketTimer.
   */
  @Test
  public void testRun() {
    System.out.println("run");
    PacketTimer instance = testTimer;
    boolean result = testTimer.addMessage("123456");
    long time = System.nanoTime();
    boolean loop = true;
    while(loop) {
      if(System.nanoTime() - time >= 100000000L)
        loop = false;
    }
    assertTrue(testExpired.gotExpired());
  }

  /**
   * Test of addMessage method, of class PacketTimer.
   */
  @Test
  public void testAddMessage() {
    System.out.println("addMessage");
    PacketTimer instance = testTimer;
    boolean result = testTimer.addMessage("123456");
    assertTrue(result);
  }
    
  public static class ExpirationObserver implements PacketExpirationObserver {
    private boolean somethingExpired;
    public ExpirationObserver() {
      somethingExpired = false;
    }
    
    public boolean gotExpired(){
      return somethingExpired;
    }
    
    public void reset(){
      somethingExpired = false;
    }
    
    @Override
    public void expired(ArrayList<String> expired){
      somethingExpired = true;
    }
  } 

  /**
   * Test of isRunning method, of class PacketTimer.
   */
  @Test
  public void testIsRunning() {
    System.out.println("isRunning");
    PacketTimer instance = null;
    boolean expResult = false;
    boolean result = instance.isRunning();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of subscribe method, of class PacketTimer.
   */
  @Test
  public void testSubscribe() {
    System.out.println("subscribe");
    PacketExpirationObserver sub = null;
    PacketTimer instance = null;
    instance.subscribe(sub);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of kill method, of class PacketTimer.
   */
  @Test
  public void testKill() {
    System.out.println("kill");
    PacketTimer instance = null;
    instance.kill();
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
}
