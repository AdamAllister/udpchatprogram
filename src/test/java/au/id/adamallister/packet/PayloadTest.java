/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.util.UUIDWrapper;
import java.nio.charset.Charset;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PayloadTest {
  
  private Payload testPayload;
  private UUIDWrapper mockUUID;
  private byte[] testData;
  private Payload corruptPayload;
  private int testPacketNumber;
  private int testPacketTotal;
  private int testSizeAsBytes;
  
  public PayloadTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    mockUUID = mock(UUIDWrapper.class);
    when(mockUUID.toString()).thenReturn("123456789");
    testPacketNumber = 1;
    testPacketTotal = 2;
    String testDataString = "test";
    testData = testDataString.getBytes(Charset.forName("UTF-8"));
    testPayload = new Payload(testPacketTotal, testPacketNumber, mockUUID, testData);
    testSizeAsBytes = 26 + testData.length;
    
    
  }
  
  @After
  public void tearDown() {
    mockUUID = null;
    testData = null;
    testPacketNumber = 0;
    testPacketTotal = 0;
    testPayload = null;
    corruptPayload = null;
  }
  
  /**
   * Test of getTotal method, of class Payload.
   */
  @Test
  public void testGetTotal() {
    System.out.println("getTotal");
    Payload instance = testPayload;
    int expResult = testPacketTotal;
    int result = instance.getTotal();
    assertEquals(expResult, result);
  }

  /**
   * Test of getGroupID method, of class Payload.
   */
  @Test
  public void testGetGroupID() {
    System.out.println("getGroupID");
    Payload instance = testPayload;
    UUIDWrapper expResult = mockUUID;
    UUIDWrapper result = instance.getGroupID();
    assertEquals(expResult, result);
  }

  /**
   * Test of getData method, of class Payload.
   */
  @Test
  public void testGetData() {
    System.out.println("getData");
    Payload instance = testPayload;
    byte[] expResult = testData;
    byte[] result = instance.getData();
    assertArrayEquals(expResult, result);
  }
  
  /**
   * Test of isCorrupt method, of class Payload.
   */
  @Test
  public void testIsCorrupt() {
    System.out.println("isCorrupt");
    corruptPayload = new Payload(testData);
    Payload instance = corruptPayload;
    boolean expResult = true;
    boolean result = instance.isCorrupt();
    assertEquals(expResult, result);
  }
  
  /**
   * Test of isCorrupt method, of class Payload.
   */
  @Test
  public void testIsNotCorrupt() {
    System.out.println("isNotCorrupt");
    Payload instance = testPayload;
    boolean expResult = false;
    boolean result = instance.isCorrupt();
    assertEquals(expResult, result);
  }

  /**
   * Test of getNumber method, of class Payload.
   */
  @Test
  public void testGetNumber() {
    System.out.println("getNumber");
    Payload instance = testPayload;
    int expResult = 1;
    int result = instance.getNumber();
    assertEquals(expResult, result);
  }

  /**
   * Test of getBytes method, of class Payload.
   */
  @Test
  public void testGetBytes() {
    System.out.println("getBytes");
    Payload instance = testPayload;
    int expResult = testSizeAsBytes;
    int result = instance.getBytes().length;
    assertEquals(expResult, result);
  }

  /**
   * Test of getGroupIdAsString method, of class Payload.
   */
  @Test
  public void testGetGroupIdAsString() {
    System.out.println("getGroupIdAsString");
    Payload instance = testPayload;
    String expResult = mockUUID.toString();
    String result = instance.getGroupIdAsString();
    assertEquals(expResult, result);
  }
  
}
