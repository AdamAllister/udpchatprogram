/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketGroupSubscriberTest {
  
  public PacketGroupSubscriberTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of onNewPacketGroup method, of class PacketGroupSubscriber.
   */
  @Test
  public void testOnNewPacketGroup() {
    System.out.println("onNewPacketGroup");
    PacketGroup pg = null;
    PacketGroupSubscriber instance = new PacketGroupSubscriberImpl();
    instance.onNewPacketGroup(pg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onFailedHealthPacket method, of class PacketGroupSubscriber.
   */
  @Test
  public void testOnFailedHealthPacket() {
    System.out.println("onFailedHealthPacket");
    Packet pg = null;
    PacketGroupSubscriber instance = new PacketGroupSubscriberImpl();
    instance.onFailedHealthPacket(pg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onHealthPacketResponse method, of class PacketGroupSubscriber.
   */
  @Test
  public void testOnHealthPacketResponse() {
    System.out.println("onHealthPacketResponse");
    Packet pg = null;
    PacketGroupSubscriber instance = new PacketGroupSubscriberImpl();
    instance.onHealthPacketResponse(pg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class PacketGroupSubscriberImpl implements PacketGroupSubscriber {

    public void onNewPacketGroup(PacketGroup pg) {
    }

    public void onFailedHealthPacket(Packet pg) {
    }

    public void onHealthPacketResponse(Packet pg) {
    }
  }
  
}
