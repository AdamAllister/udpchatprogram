/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.io.IOException;
import java.net.DatagramPacket;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class DatagramPacketSubscriberTest {
  
  public DatagramPacketSubscriberTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of processDatagramPacket method, of class DatagramPacketSubscriber.
   */
  @Test
  public void testProcessDatagramPacket() throws Exception {
    System.out.println("processDatagramPacket");
    DatagramPacket packet = null;
    DatagramPacketSubscriber instance = new DatagramPacketSubscriberImpl();
    instance.processDatagramPacket(packet);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class DatagramPacketSubscriberImpl implements DatagramPacketSubscriber {

    public void processDatagramPacket(DatagramPacket packet) throws IOException {
    }
  }
  
}
