/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.message.JSONMessageUtility;
import au.id.adamallister.message.messages.HandShakeMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import au.id.adamallister.message.messages.ClientRegistrationMessage;
import au.id.adamallister.util.UUIDWrapper;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.mockito.Matchers.any;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketManagerTest {

  private static DatagramSocket mockSocket;
  private static PacketManager testPM;
  private static ImplPacketGroupSubscriber testSub;
  private static PacketReceiver mockPR;
  private static PacketTimer mockTimer;
  private static UUIDWrapper mockID;
  private static InetAddress mockAddress;
  private static int testPort;
  
  private static Packet mockPacket;
  private static Payload mockPayload;
  private static byte[] data;
  private static ArrayList<DatagramPacket> sentDatagrams;
  
  public PacketManagerTest() {
  }

  @BeforeClass
  public static void setUpClass() {
    mockID = mock(UUIDWrapper.class);
    when(mockID.getMostSignificantBits()).thenReturn(1L);
    when(mockID.getLeastSignificantBits()).thenReturn(2L);
    when(mockID.toString()).thenReturn("12");
    testSub = new ImplPacketGroupSubscriber();
    mockTimer = mock(PacketTimer.class);
    when(mockTimer.addMessage(anyString())).thenReturn(true);
    when(mockTimer.isRunning()).thenReturn(true);
    mockSocket = mock(DatagramSocket.class);
    sentDatagrams = new ArrayList<>();
    try {
      doAnswer(new Answer<Object>() {
        
        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
          sentDatagrams.add(invocation.getArgumentAt(0, DatagramPacket.class));
          return null;
        }
      }).when(mockSocket).send(any(DatagramPacket.class));
    } catch (IOException ex) {
      fail("Answer things");
    }

    mockPR = mock(PacketReceiver.class);
    when(mockPR.isRunning()).thenReturn(true);
    testPM = new PacketManager(mockSocket, 512, mockTimer, mockPR);

    mockPayload = mock(Payload.class);
    data = new byte[1];
    data[0] = 8;
    when(mockPayload.getBytes()).thenReturn(data);
    mockPacket = mock(Packet.class);
    when(mockPacket.getPayload()).thenReturn(mockPayload);
    mockAddress = mock(InetAddress.class);
    when(mockPacket.getIP()).thenReturn(mockAddress);
    testPort = 57330;
    when(mockPacket.getPort()).thenReturn(testPort);
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
    testSub.reset();
    if (!testPM.isRunning()) {
      testPM = new PacketManager(mockSocket, 512, mockTimer, mockPR);
    }
    System.out.println("SETUP");
  }

  @After
  public void tearDown() {

    sentDatagrams.clear();
  }

  /**
   * Test of send method, of class PacketManager.
   */
  @Test
  public void testSend_PacketGroup() {
    System.out.println("testSend_PacketGroup");
    String msg = "ahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
        + "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
        + "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
        + "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh";
    PacketGroup pg = PacketUtility.generatePacketGroup(msg, 512, mockAddress, testPort);

    PacketManager instance = testPM;
    try {
      instance.send(pg.getPackets()[0]);
      for(int i = 0; i < sentDatagrams.size(); i++) {
        assertEquals(mockAddress, sentDatagrams.get(i).getAddress());
        assertEquals(testPort, sentDatagrams.get(i).getPort());
      }
    } catch (IOException e) {
      fail("Socket error");
    }
  }

  /**
   * Test of send method, of class PacketManager.
   */
  @Test
  public void testSend_Packet_Simple_DatagramSentToSocket() {
    System.out.println("testSend_Packet");

    PacketManager instance = testPM;
    try {
      instance.send(mockPacket);
      assertEquals(mockAddress, sentDatagrams.get(0).getAddress());
      assertEquals(testPort, sentDatagrams.get(0).getPort());
      assertEquals(data[0], sentDatagrams.get(0).getData()[0]);
    } catch (IOException e) {
      fail("Socket error");
    }
  }

  /**
   * Test of processDatagramPacket method, of class PacketManager.
   */
  @Test
  public void testProcessDatagramPacket() {
    System.out.println("testProcessDatagramPacket");
    String msg = "testTube";
    testPM.subscribeToCompletePacketGroups(testSub);
    PacketGroup pg = PacketUtility.generatePacketGroup(msg, 512, mockAddress, 57330);
    PacketManager instance = testPM;
    DatagramPacket datagram = new DatagramPacket(pg.getPackets()[0].getPayload().getBytes(), pg.getPackets()[0].getPayload().getBytes().length) ;
    datagram.setAddress(mockAddress);
    datagram.setPort(testPort);
    try {
      testPM.processDatagramPacket(datagram);
    } catch (IOException ex) {
      fail("IOException");
    }
    assertNotNull(testSub.getPG());

  }

  /**
   * Test of expired method, of class PacketManager.
   */
  @Test
  public void testExpired() {
    System.out.println("testExpired");
    String msg = "Health Check";
    PacketGroup pg = PacketUtility.generatePacketGroup(msg, 512, mockAddress, testPort);
    PacketManager instance = testPM;
    instance.subscribeToCompletePacketGroups(testSub);
    try {
      testPM.sendHealthCheck(pg.getPackets()[0]);
      ArrayList<String> expired = new ArrayList<>();
      expired.add(pg.getGroupIDAsString() + "1");
      testPM.expired(expired);
    } catch (IOException ex) {
      fail("IOException");
    }
    assertEquals(pg.getPackets()[0].getPayload().getGroupIdAsString(), testSub.getP().getPayload().getGroupIdAsString());
  }

  public static class ImplPacketGroupSubscriber implements PacketGroupSubscriber {

    private PacketGroup testPg = null;
    private Packet testP = null;

    public PacketGroup getPG() {
      return testPg;
    }

    public void reset() {
      testPg = null;
      testP = null;
    }

    public Packet getP() {
      return testP;
    }

    public void onNewPacketGroup(PacketGroup pg) {
      testPg = pg;
    }

    public void onFailedHealthPacket(Packet pg) {
      testP = pg;
    }

    public void onHealthPacketResponse(Packet pg) {
      testP = pg;
    }
  }

  /**
   * Test of isRunning method, of class PacketManager.
   */
  @Test
  public void testIsRunning() {
    System.out.println("isRunning");
    PacketManager instance = testPM;
    boolean result = instance.isRunning();
    assertTrue(result);
  }

  /**
   * Test of kill method, of class PacketManager.
   */
  @Test
  public void testKill() {
    System.out.println("kill");
    PacketManager instance = testPM;
    instance.kill();
    long time = System.nanoTime();
    boolean loop = true;
    while (loop) {
      if (System.nanoTime() - time >= 10000000L) {
        loop = false;
      }
    }
    boolean result = instance.isRunning();
    assertFalse(result);
  }

  /**
   * Test of subscribeToCompletePacketGroups method, of class PacketManager.
   */
  @Test
  public void testSubscribeToCompletePacketGroups() {
    System.out.println("subscribeToCompletePacketGroups");
    String msg = "Health Check";
    PacketGroup pg = PacketUtility.generatePacketGroup(msg, 512, mockAddress, testPort);
    PacketManager instance = testPM;
    instance.subscribeToCompletePacketGroups(testSub);
    try {
      testPM.sendHealthCheck(pg.getPackets()[0]);
      ArrayList<String> expired = new ArrayList<>();
      expired.add(pg.getGroupIDAsString() + "1");
      testPM.expired(expired);
    } catch (IOException ex) {
      fail("IOException");
    }
    assertNotNull(testSub.getP());
  }

  /**
   * Test of sendHealthCheck method, of class PacketManager.
   */
  @Test
  public void testSendHealthCheck() throws Exception {
    System.out.println("sendHealthCheck");
    Packet packet = mockPacket;
    PacketManager instance = testPM;
    instance.sendHealthCheck(packet);
    assertEquals(mockAddress, sentDatagrams.get(0).getAddress());
    assertEquals(testPort, sentDatagrams.get(0).getPort());
    assertEquals(data[0], sentDatagrams.get(0).getData()[0]);
  }

  /**
   * Test of send method, of class PacketManager.
   */
  @Test
  public void testSend_Packet() throws Exception {
    System.out.println("send");
    Packet packet = null;
    PacketManager instance = null;
    instance.send(packet);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of sendOnce method, of class PacketManager.
   */
  @Test
  public void testSendOnce() throws Exception {
    System.out.println("sendOnce");
    PacketGroup pg = null;
    PacketManager instance = null;
    instance.sendOnce(pg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getWaitingCount method, of class PacketManager.
   */
  @Test
  public void testGetWaitingCount() {
    System.out.println("getWaitingCount");
    PacketManager instance = null;
    int expResult = 0;
    int result = instance.getWaitingCount();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
}
