/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.message.JSONMessageUtility;
import au.id.adamallister.message.messages.HealthCheckMessage;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketUtilityTest {
  
  public PacketUtilityTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of generatePacketGroup method, of class PacketUtility.
   */
  @Test
  public void testGeneratePacketGroup() {
    System.out.println("generatePacketGroup");
    
    String msg = "AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH";
    int packetSize = 512;
    InetAddress address;
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException ex) {
      address = null;
    }
    int port = 57331;
 
    PacketGroup result = PacketUtility.generatePacketGroup(msg, packetSize, address, port);
    

    try {
      assertEquals(msg, new String(result.getCompleteData(), "UTF-8"));
    } catch (UnsupportedEncodingException ex) {
      fail("Could not convert to string");
    }
  }
  
}
