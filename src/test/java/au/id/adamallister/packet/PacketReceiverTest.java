/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.net.DatagramSocket;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketReceiverTest {
  
  private PacketReceiver testReceiver;
  
  public PacketReceiverTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    PacketManager pm = mock(PacketManager.class);
    DatagramSocket ds = mock(DatagramSocket.class);
    testReceiver = new PacketReceiver(ds, 512, pm);
    testReceiver.start();
  }
  
  @After
  public void tearDown() {
    testReceiver.kill();
    testReceiver = null;
  }

  /**
   * Test of kill method, of class PacketReceiver.
   */
  @Test
  public void testKill() {
    System.out.println("kill");
    PacketReceiver instance = testReceiver;
    instance.kill();
    assertEquals(false, instance.isRunning());
  }

  /**
   * Test of isRunning method, of class PacketReceiver.
   */
  @Test
  public void testIsRunning() {
    System.out.println("isRunning");
    PacketReceiver instance = null;
    boolean expResult = false;
    boolean result = instance.isRunning();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of subscribeToPackets method, of class PacketReceiver.
   */
  @Test
  public void testSubscribeToPackets() {
    System.out.println("subscribeToPackets");
    DatagramPacketSubscriber sub = null;
    PacketReceiver instance = null;
    instance.subscribeToPackets(sub);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of run method, of class PacketReceiver.
   */
  @Test
  public void testRun() {
    System.out.println("run");
    PacketReceiver instance = null;
    instance.run();
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
