/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.util.UUIDWrapper;
import java.net.InetAddress;
import java.nio.charset.Charset;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketGroupTest {
  
  private PacketGroup testPacketGroup;
  private Packet packet;
  private InetAddress address;
  private int port;
  private Payload payload;
  private static UUIDWrapper testID;
  private static UUIDWrapper testID2;
  private JSONObject testJSON;
  
  public PacketGroupTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
    testID = mock(UUIDWrapper.class,RETURNS_DEEP_STUBS);
    testID2 = mock(UUIDWrapper.class, RETURNS_DEEP_STUBS);
    when(testID.toString()).thenReturn("123456789");
    when(testID.getMostSignificantBits()).thenReturn(1234L);
    when(testID.getLeastSignificantBits()).thenReturn(2345L);
    when(testID2.toString()).thenReturn("987654321");
    when(testID2.getMostSignificantBits()).thenReturn(4321L);
    when(testID2.getMostSignificantBits()).thenReturn(5432L);
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    address = mock(InetAddress.class);

    port = 1234;
    testJSON = new JSONObject();
    testJSON.put("test", "test");
    
    payload = new Payload(1, 1, testID, testJSON.toString().getBytes(Charset.forName("UTF-8")));
    packet = new Packet(address, port, payload);
    testPacketGroup = new PacketGroup(packet);
  }
  
  @After
  public void tearDown() {
    packet = null;
    payload = null;
    port = 0;
    address = null;
  }
  
  /**
   * Test of isComplete method, of class PacketGroup.
   */
  @Test
  public void testIsComplete_Default() {
    System.out.println("testIsComplete_Default");
    PacketGroup instance = new PacketGroup(packet);
    boolean expResult = true;
    boolean result = instance.isComplete();
    assertEquals(expResult, result);
  }

  /**
   * Test of addPacket method, of class PacketGroup.
   */
  @Test
  public void testAddPacket_Default() {
    System.out.println("testAddPacket_Default");
    PacketGroup instance = new PacketGroup(1, testID, packet.getIP(),packet.getPort());
    boolean expResult = true;
    boolean result = instance.addPacket(packet);
    assertEquals(expResult, result);
  }
  
    /**
   * Test of addPacket method, of class PacketGroup.
   */
  @Test
  public void testAddPacket_WithWrongID() {
    System.out.println("testAddPacket_WithWrongID");
    PacketGroup instance = new PacketGroup(1, testID2, packet.getIP(),packet.getPort());
    boolean expResult = false;
    boolean result = instance.addPacket(packet);
    assertEquals(expResult, result);
  }

  /**
   * Test of addPayload method, of class PacketGroup.
   */
  @Test
  public void testAddPayload_Default() {
    System.out.println("testAddPayload_Default");
    PacketGroup instance = new PacketGroup(1, testID, packet.getIP(),packet.getPort());
    boolean expResult = true;
    boolean result = instance.addPayload(payload);
    assertEquals(expResult, result);
  }

  /**
   * Test of addPayload method, of class PacketGroup.
   */
  @Test
  public void testAddPayload_WithWrongID() {
    System.out.println("testAddPayload_WithWrongID");
    PacketGroup instance = new PacketGroup(1, testID2, packet.getIP(),packet.getPort());
    boolean expResult = false;
    boolean result = instance.addPayload(payload);
    assertEquals(expResult, result);
  }  
  
  /**
   * Test of getCompleteData method, of class PacketGroup.
   */
  @Test
  public void testGetCompleteData_WithOnePacket() {
    System.out.println("testGetCompleteData_WithOnePacket");
    PacketGroup instance = new PacketGroup(packet);
    byte[] expResult = testJSON.toString().getBytes(Charset.forName("UTF-8"));
    byte[] result = instance.getCompleteData();
    assertArrayEquals(expResult, result);
  }
  
  /**
   * Test of getCompleteData method where there is more the one packet, 
   * of class PacketGroup.
   */
  @Test
  public void testGetCompleteData_WithMultiplePackets() {
    System.out.println("testGetCompleteData_WithMultiplePackets");
    String testMsg = "ahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
        + "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
        + "dfgdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
        + "fffffffffffffffffffffffffffffffffffffffffffffffffffffff"
        + "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
        + "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt"
        + "eweeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
        + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
        + "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
    testPacketGroup = PacketUtility.generatePacketGroup(testMsg, 256, address, port);
    
    PacketGroup instance = testPacketGroup;
    byte[] expResult = testMsg.getBytes(Charset.forName("UTF-8"));
    byte[] result = instance.getCompleteData();
    assertArrayEquals(expResult, result);
  }

  /**
   * Test of getPackets method, of class PacketGroup.
   */
  @Test
  public void testGetPackets_Default() {
    System.out.println("testGetPackets_Default");
    PacketGroup instance = testPacketGroup;
    Packet[] expResult = new Packet[1];
    expResult[0] = packet;
    Packet[] result = instance.getPackets();
    assertEquals(expResult[0].getPayload(), result[0].getPayload());
  }

  /**
   * Test of getGroupID method, of class PacketGroup.
   */
  @Test
  public void testGetGroupID_Default() {
    System.out.println("testGetGroupID_Default");
    PacketGroup instance = testPacketGroup;
    UUIDWrapper expResult = testID;
    UUIDWrapper result = instance.getGroupID();
    assertEquals(expResult.toString(), expResult.toString());
  }

  /**
   * Test of getAddress method, of class PacketGroup.
   */
  @Test
  public void testGetAddress_Default() {
    System.out.println("testGetAddress_Default");
    PacketGroup instance = testPacketGroup;
    InetAddress expResult = address;
    InetAddress result = instance.getAddress();
    assertEquals(expResult, result);
  }

  /**
   * Test of getPort method, of class PacketGroup.
   */
  @Test
  public void testGetPort_Default() {
    System.out.println("getPort");
    PacketGroup instance = testPacketGroup;
    int expResult = 1234;
    int result = instance.getPort();
    assertEquals(expResult, result);
  }

  /**
   * Test of getGroupIDAsString method, of class PacketGroup.
   */
  @Test
  public void testGetGroupIDAsString_Default() {
    System.out.println("getGroupIDAsString");
    PacketGroup instance = testPacketGroup;
    String expResult = testID.toString();
    String result = instance.getGroupIDAsString();
    assertEquals(expResult, result);
  }

  /**
   * Test of isComplete method, of class PacketGroup.
   */
  @Test
  public void testIsComplete() {
    System.out.println("isComplete");
    PacketGroup instance = null;
    boolean expResult = false;
    boolean result = instance.isComplete();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of addPacket method, of class PacketGroup.
   */
  @Test
  public void testAddPacket() {
    System.out.println("addPacket");
    Packet packet = null;
    PacketGroup instance = null;
    boolean expResult = false;
    boolean result = instance.addPacket(packet);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of addPayload method, of class PacketGroup.
   */
  @Test
  public void testAddPayload() {
    System.out.println("addPayload");
    Payload payload = null;
    PacketGroup instance = null;
    boolean expResult = false;
    boolean result = instance.addPayload(payload);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getCompleteData method, of class PacketGroup.
   */
  @Test
  public void testGetCompleteData() {
    System.out.println("getCompleteData");
    PacketGroup instance = null;
    byte[] expResult = null;
    byte[] result = instance.getCompleteData();
    assertArrayEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getPackets method, of class PacketGroup.
   */
  @Test
  public void testGetPackets() {
    System.out.println("getPackets");
    PacketGroup instance = null;
    Packet[] expResult = null;
    Packet[] result = instance.getPackets();
    assertArrayEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getGroupID method, of class PacketGroup.
   */
  @Test
  public void testGetGroupID() {
    System.out.println("getGroupID");
    PacketGroup instance = null;
    UUIDWrapper expResult = null;
    UUIDWrapper result = instance.getGroupID();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getGroupIDAsString method, of class PacketGroup.
   */
  @Test
  public void testGetGroupIDAsString() {
    System.out.println("getGroupIDAsString");
    PacketGroup instance = null;
    String expResult = "";
    String result = instance.getGroupIDAsString();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getAddress method, of class PacketGroup.
   */
  @Test
  public void testGetAddress() {
    System.out.println("getAddress");
    PacketGroup instance = null;
    InetAddress expResult = null;
    InetAddress result = instance.getAddress();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of getPort method, of class PacketGroup.
   */
  @Test
  public void testGetPort() {
    System.out.println("getPort");
    PacketGroup instance = null;
    int expResult = 0;
    int result = instance.getPort();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
