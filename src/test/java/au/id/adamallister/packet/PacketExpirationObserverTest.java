/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketExpirationObserverTest {
  
  public PacketExpirationObserverTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of expired method, of class PacketExpirationObserver.
   */
  @Test
  public void testExpired() {
    System.out.println("expired");
    ArrayList<String> expPackets = null;
    PacketExpirationObserver instance = new PacketExpirationObserverImpl();
    instance.expired(expPackets);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class PacketExpirationObserverImpl implements PacketExpirationObserver {

    public void expired(ArrayList<String> expPackets) {
    }
  }
  
}
