/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class PacketTest {
  
  private Packet packet;
  private static InetAddress mockAddress;
  private static int testPort;
  private static Payload mockPayload;
  
  public PacketTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
    mockAddress = mock(InetAddress.class);
    testPort = 57330;
    mockPayload = mock(Payload.class);
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    
    
    packet = new Packet(mockAddress, testPort, mockPayload);
  }
  
  @After
  public void tearDown() {
    packet = null;
  }

  /**
   * Test of getPayload method, of class Packet.
   */
  @Test
  public void testGetPayload() {
    System.out.println("getPayload");
    Packet instance = packet;
    Payload expResult = mockPayload;
    Payload result = instance.getPayload();
    assertEquals(expResult, result);
  }

  /**
   * Test of getPort method, of class Packet.
   */
  @Test
  public void testGetPort() {
    System.out.println("getPort");
    Packet instance = packet;
    int expResult = testPort;
    int result = instance.getPort();
    assertEquals(expResult, result);
  }

  /**
   * Test of getIP method, of class Packet.
   */
  @Test
  public void testGetIP() {
    System.out.println("getIP");
    Packet instance = packet;
    InetAddress expResult = mockAddress;
    InetAddress result = instance.getIP();
    assertEquals(expResult, result);
  }
  
}
