/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.server;

import au.id.adamallister.message.messages.ChatRequestMessage;
import au.id.adamallister.message.messages.ChatRequestResponseMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import au.id.adamallister.message.messages.Message;
import au.id.adamallister.message.messages.RegistrationMessage;
import au.id.adamallister.packet.Packet;
import au.id.adamallister.packet.PacketGroup;
import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ServerControllerTest {
  
  public ServerControllerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of onNewPacketGroup method, of class ServerController.
   */
  @Test
  public void testOnNewPacketGroup() {
    System.out.println("onNewPacketGroup");
    PacketGroup pg = null;
    ServerController instance = null;
    instance.onNewPacketGroup(pg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onFailedHealthPacket method, of class ServerController.
   */
  @Test
  public void testOnFailedHealthPacket() {
    System.out.println("onFailedHealthPacket");
    Packet failedPacket = null;
    ServerController instance = null;
    instance.onFailedHealthPacket(failedPacket);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onHealthPacketResponse method, of class ServerController.
   */
  @Test
  public void testOnHealthPacketResponse() {
    System.out.println("onHealthPacketResponse");
    Packet packet = null;
    ServerController instance = null;
    instance.onHealthPacketResponse(packet);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of isRunning method, of class ServerController.
   */
  @Test
  public void testIsRunning() {
    System.out.println("isRunning");
    ServerController instance = null;
    boolean expResult = false;
    boolean result = instance.isRunning();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of kill method, of class ServerController.
   */
  @Test
  public void testKill() {
    System.out.println("kill");
    ServerController instance = null;
    instance.kill();
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverRegistration method, of class ServerController.
   */
  @Test
  public void testServerRegistration() {
    System.out.println("serverRegistration");
    RegistrationMessage regMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerController instance = null;
    instance.serverRegistration(regMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of sendToAll method, of class ServerController.
   */
  @Test
  public void testSendToAll() {
    System.out.println("sendToAll");
    Message msg = null;
    ServerController instance = null;
    instance.sendToAll(msg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverChatRequest method, of class ServerController.
   */
  @Test
  public void testServerChatRequest() {
    System.out.println("serverChatRequest");
    ChatRequestMessage rqstMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerController instance = null;
    instance.serverChatRequest(rqstMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverChatRequestResponse method, of class ServerController.
   */
  @Test
  public void testServerChatRequestResponse() {
    System.out.println("serverChatRequestResponse");
    ChatRequestResponseMessage rspnMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerController instance = null;
    instance.serverChatRequestResponse(rspnMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheck method, of class ServerController.
   */
  @Test
  public void testServerHealthCheck() {
    System.out.println("serverHealthCheck");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerController instance = null;
    instance.serverHealthCheck(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheckFail method, of class ServerController.
   */
  @Test
  public void testServerHealthCheckFail() {
    System.out.println("serverHealthCheckFail");
    InetAddress address = null;
    int port = 0;
    ServerController instance = null;
    instance.serverHealthCheckFail(address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheckResponse method, of class ServerController.
   */
  @Test
  public void testServerHealthCheckResponse() {
    System.out.println("serverHealthCheckResponse");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerController instance = null;
    instance.serverHealthCheckResponse(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
