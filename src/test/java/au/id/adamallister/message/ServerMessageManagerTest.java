/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.packet.Packet;
import java.net.InetAddress;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ServerMessageManagerTest {
  
  public ServerMessageManagerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of subscribe method, of class ServerMessageManager.
   */
  @Test
  public void testSubscribe() {
    System.out.println("subscribe");
    ServerMessageSubscriber sub = null;
    ServerMessageManager instance = null;
    instance.subscribe(sub);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onFailedHealthPacket method, of class ServerMessageManager.
   */
  @Test
  public void testOnFailedHealthPacket() {
    System.out.println("onFailedHealthPacket");
    Packet failedPacket = null;
    ServerMessageManager instance = null;
    instance.onFailedHealthPacket(failedPacket);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onHealthPacketResponse method, of class ServerMessageManager.
   */
  @Test
  public void testOnHealthPacketResponse() {
    System.out.println("onHealthPacketResponse");
    Packet packet = null;
    ServerMessageManager instance = null;
    instance.onHealthPacketResponse(packet);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of processJSONObject method, of class ServerMessageManager.
   */
  @Test
  public void testProcessJSONObject() {
    System.out.println("processJSONObject");
    JSONObject json = null;
    InetAddress address = null;
    int port = 0;
    ServerMessageManager instance = null;
    instance.processJSONObject(json, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
