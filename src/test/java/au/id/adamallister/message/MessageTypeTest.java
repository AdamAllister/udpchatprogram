/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class MessageTypeTest {
  
  public MessageTypeTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of valueOf method, of class MessageType.
   */
  @Test
  public void testValueOf() {
    System.out.println("valueOf");
    String name = "HAND";
    MessageType expResult = MessageType.HAND;
    MessageType result = MessageType.valueOf(name);
    assertEquals(expResult, result);
  }

  /**
   * Test of values method, of class MessageType.
   */
  @Test
  public void testValues() {
    System.out.println("values");
    MessageType[] expResult = null;
    MessageType[] result = MessageType.values();
    assertArrayEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
  
}
