/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({au.id.adamallister.message.MessageManagerTest.class, au.id.adamallister.message.ClientMessageSubscriberTest.class, au.id.adamallister.message.JSONMessageUtilityTest.class, au.id.adamallister.message.ServerMessageManagerTest.class, au.id.adamallister.message.ServerMessageSubscriberTest.class, au.id.adamallister.message.ClientMessageManagerTest.class, au.id.adamallister.message.MessageTypeTest.class})
public class MessageSuite {
  
}
