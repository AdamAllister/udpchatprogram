/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class JSONMessageUtilityTest {

  public JSONMessageUtilityTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObject() {
    System.out.println("fromJSONObject");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.HAND);
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("username", "Test");
    InetAddress address = null;
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException ex) {
      Logger.getLogger(JSONMessageUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    int port = 57330;
    Message expResult = new HandShakeMessage(uid, "Test");
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(JSONMessageUtility.toJSON(expResult).toString(), JSONMessageUtility.toJSON(result).toString());
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObjectWithEmptyType() {
    System.out.println("fromJSONObjectWithEmptyType");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", "");
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("username", "Test");
    InetAddress address = null;
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException ex) {
      Logger.getLogger(JSONMessageUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    int port = 57330;
    Message expResult = null;
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(expResult, result);
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObjectWithWrongType() {
    System.out.println("fromJSONObjectWithEmptyType");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.UPD);
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("username", "Test");
    InetAddress address = null;
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException ex) {
      Logger.getLogger(JSONMessageUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    int port = 57330;
    Message expResult = null;
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(expResult, result);
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObjectWithMissingStringData() {
    System.out.println("fromJSONObjectWithMissingStringData");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.HAND);
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    InetAddress address = null;
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException ex) {
      Logger.getLogger(JSONMessageUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    int port = 57330;
    Message expResult = null;
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(expResult, result);
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObjectWithMissingIDData() {
    System.out.println("fromJSONObjectWithMissingIDData");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.HAND);
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    InetAddress address = null;
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException ex) {
      Logger.getLogger(JSONMessageUtilityTest.class.getName()).log(Level.SEVERE, null, ex);
    }
    int port = 57330;
    Message expResult = null;
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(expResult, result);
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObjectWithNullAddress() {
    System.out.println("fromJSONObjectWithNullAddress");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.HAND);
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    InetAddress address = null;
    int port = 57330;
    Message expResult = null;
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(expResult, result);
  }

  /**
   * Test of fromJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testFromJSONObjectWithMissingArray() {
    System.out.println("testFromJSONObjectWithMissingArray");
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.REG);
    UUID uid = UUID.randomUUID();
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("username", "Test");
    jsonObject.put("connecting", true);
    int[] ports = new int[1];
    ports[0] = 57330;
    jsonObject.put("ports", ports);

    InetAddress address = null;
    int port = 57330;
    Message expResult = null;
    Message result = JSONMessageUtility.fromJSONObject(jsonObject, address, port);
    assertEquals(expResult, result);
  }

  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithUpdateMessage() {
    System.out.println("testToJSONWithUpdateMessage");
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.UPD);
    
    UUID uid = UUID.randomUUID();
    
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    
    String[] addresses = new String[2];
    addresses[0] = "192.168.0.101";
    addresses[1] = "192.168.0.102";
    Integer[] ports = new Integer[2];
    ports[0] = 57330;
    ports[1] = 57331;
    
    jsonObject.put("addresses", addresses);
    jsonObject.put("ports", ports);
    jsonObject.put("username", "Test");
    jsonObject.put("connecting", true);
    
    InetAddress address = null;
    int port = 57330;
    ClientUpdateMessage msg = new ClientUpdateMessage(uid, addresses, ports, true, "Test");
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }
  
  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithChatMessage() {
    System.out.println("testToJSONWithChatMessage");
    
    
    
    UUID uid = UUID.randomUUID();
    UUID chatID = UUID.randomUUID();
    Date time = new Date();
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.MSG);
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("from"     , "Test");
    jsonObject.put("chat-id-1", chatID.getMostSignificantBits());
    jsonObject.put("chat-id-2", chatID.getLeastSignificantBits());
    jsonObject.put("time-utc" , time.getTime());
    jsonObject.put("msg"      , "Hi");
    
    ChatMessage msg = new ChatMessage(uid, "Test", chatID, time.getTime(), "Hi");
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }
  
  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithHealthCheckMessage() {
    System.out.println("testToJSONWithHealthCheckMessage");
    
    UUID uid = UUID.randomUUID();
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.HCHK);
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("username" , "Test");
    
    HealthCheckMessage msg = new HealthCheckMessage(uid, "Test");
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }
  
  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithRegistrationMessage() {
    System.out.println("testToJSONWithRegistrationMessage");
    
    UUID uid = UUID.randomUUID();
    
    String[] addresses = new String[2];
    addresses[0] = "192.168.0.101";
    addresses[1] = "192.168.0.102";
    Integer[] ports = new Integer[2];
    ports[0] = 57330;
    ports[1] = 57331;
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.REG);
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("addresses", addresses);
    jsonObject.put("ports"    , ports);
    jsonObject.put("status"   , 0);
    jsonObject.put("username" , "Test");
    
    ClientRegistrationMessage msg = new ClientRegistrationMessage(uid, addresses, ports, "Test", 0);
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }  
  
  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithRequestMessage() {
    System.out.println("testToJSONWithRequestMessage");
    
    UUID uid = UUID.randomUUID();
    UUID chatID = UUID.randomUUID();
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.RQST);
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("requester", "Test1");
    jsonObject.put("responder", "Test2");
    jsonObject.put("chat-id-1", chatID.getMostSignificantBits());
    jsonObject.put("chat-id-2", chatID.getLeastSignificantBits());
    
    ChatRequestMessage msg = new ChatRequestMessage(uid, "Test1", "Test2", chatID);
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }
  
  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithRequestResponseMessage() {
    System.out.println("testToJSONWithRequestResponseMessage");
    
    UUID uid = UUID.randomUUID();
    UUID chatID = UUID.randomUUID();
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.RSPN);
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("requester", "Test1");
    jsonObject.put("responder", "Test2");
    jsonObject.put("response" , true);
    jsonObject.put("chat-id-1", chatID.getMostSignificantBits());
    jsonObject.put("chat-id-2", chatID.getLeastSignificantBits());
    
    ChatRequestResponseMessage msg = new ChatRequestResponseMessage(uid, "Test1", "Test2", true, chatID);
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }   
  
  /**
   * Test of toJSONObject method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSONWithHandShakeMessage() {
    System.out.println("testToJSONWithHandShakeMessage");
    
    UUID uid = UUID.randomUUID();
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("type", MessageType.HAND);
    jsonObject.put("uid-1", uid.getMostSignificantBits());
    jsonObject.put("uid-2", uid.getLeastSignificantBits());
    jsonObject.put("username" , "Test");
    
    HandShakeMessage msg = new HandShakeMessage(uid, "Test");
    
    JSONObject expResult = jsonObject;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    
    assertEquals(expResult.toString(), result.toString());
  }  

  /**
   * Test of toJSON method, of class JSONMessageUtility.
   */
  @Test
  public void testToJSON() {
    System.out.println("toJSON");
    Message msg = null;
    JSONObject expResult = null;
    JSONObject result = JSONMessageUtility.toJSON(msg);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
}
