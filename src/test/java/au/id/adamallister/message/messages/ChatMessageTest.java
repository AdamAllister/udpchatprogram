/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ChatMessageTest {
  
  private ChatMessage testGoodMsg;
  private long testGoodTimeSent;
  private String testGoodFromName;
  private String testGoodMessage;
  private UUID testGoodChatID;
  private UUID testGoodID;
  private String testGoodTimeString;
  
  public ChatMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testGoodID = UUID.randomUUID();
    testGoodChatID = UUID.randomUUID();
    testGoodFromName = "Fred";
    Date testTime = new Date();
    DateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
    testGoodTimeString = format.format(testTime);
    testGoodTimeSent = testTime.getTime();
    testGoodMessage = "Hello There";
    
    testGoodMsg = new ChatMessage(testGoodID, testGoodFromName, testGoodChatID, 
        testGoodTimeSent, testGoodMessage);
  }
  
  @After
  public void tearDown() {
    testGoodID = null;
    testGoodChatID = null;
    testGoodFromName = null;
    testGoodTimeSent = 0;
    testGoodMessage = null;
    testGoodMsg = null;
  }

  /**
   * Test of getChatID method, of class ChatMessage.
   */
  @Test
  public void testGetChatID() {
    System.out.println("getChatID");
    ChatMessage instance = testGoodMsg;
    UUID expResult = testGoodChatID;
    UUID result = instance.getChatID();
    assertEquals(expResult, result);
  }

  /**
   * Test of getMessage method, of class ChatMessage.
   */
  @Test
  public void testGetMessage() {
    System.out.println("getMessage");
    ChatMessage instance = testGoodMsg;
    String expResult = testGoodMessage;
    String result = instance.getMessage();
    assertEquals(expResult, result);
  }

  /**
   * Test of getFromName method, of class ChatMessage.
   */
  @Test
  public void testGetFromName() {
    System.out.println("getFromName");
    ChatMessage instance = testGoodMsg;
    String expResult = testGoodFromName;
    String result = instance.getFromName();
    assertEquals(expResult, result);
  }

  /**
   * Test of getTimeMillis method, of class ChatMessage.
   */
  @Test
  public void testGetTimeMillis() {
    System.out.println("getTimeMillis");
    ChatMessage instance = testGoodMsg;
    long expResult = testGoodTimeSent;
    long result = instance.getTimeMillis();
    assertEquals(expResult, result);
  }

  /**
   * Test of getTimeString method, of class ChatMessage.
   */
  @Test
  public void testGetTimeString() {
    System.out.println("getTimeString");
    ChatMessage instance = testGoodMsg;
    String expResult = testGoodTimeString;
    String result = instance.getTimeString();
    assertEquals(expResult, result);
  }

  /**
   * Test of toString method, of class ChatMessage.
   */
  @Test
  public void testToString() {
    System.out.println("toString");
    ChatMessage instance = testGoodMsg;
    String expResult = testGoodTimeString + " " + testGoodFromName + ": " + testGoodMessage;
    String result = instance.toString();
    assertEquals(expResult, result);
  }
  
}
