/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ChatRequestMessageTest {
  
  private ChatRequestMessage testGoodMsg;
  private UUID testGoodID;
  private String testGoodRequester; 
  private String testGoodResponder;
  private UUID testGoodChatID;
  
  public ChatRequestMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testGoodID = UUID.randomUUID();
    testGoodRequester = "Fred";
    testGoodResponder = "Bob";
    testGoodChatID = UUID.randomUUID();
    
    testGoodMsg = new ChatRequestMessage(testGoodID, testGoodRequester, 
        testGoodResponder, testGoodChatID);
  }
  
  @After
  public void tearDown() {
    testGoodID = null;
    testGoodRequester = null;
    testGoodResponder = null;
    testGoodChatID = null;
    
    testGoodMsg = null;
  }

  /**
   * Test of setResponder method, of class ChatRequestMessage.
   */
  @Test
  public void testSetResponder() {
    System.out.println("setResponder");
    String responder = "Test";
    ChatRequestMessage instance = testGoodMsg;
    instance.setResponder(responder);
    assertEquals("Test", instance.getResponderUsername());
  }

  /**
   * Test of getResponderUsername method, of class ChatRequestMessage.
   */
  @Test
  public void testGetResponderUsername() {
    System.out.println("getResponderUsername");
    ChatRequestMessage instance = testGoodMsg;
    String expResult = testGoodResponder;
    String result = instance.getResponderUsername();
    assertEquals(expResult, result);
  }

  /**
   * Test of getRequesterUsername method, of class ChatRequestMessage.
   */
  @Test
  public void testGetRequesterUsername() {
    System.out.println("getRequesterUsername");
    ChatRequestMessage instance = testGoodMsg;
    String expResult = testGoodRequester;
    String result = instance.getRequesterUsername();
    assertEquals(expResult, result);
  }

  /**
   * Test of getChatID method, of class ChatRequestMessage.
   */
  @Test
  public void testGetChatID() {
    System.out.println("getChatID");
    ChatRequestMessage instance = testGoodMsg;
    UUID expResult = testGoodChatID;
    UUID result = instance.getChatID();
    assertEquals(expResult, result);
  }
  
}
