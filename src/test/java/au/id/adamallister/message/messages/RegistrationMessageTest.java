/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.packet.SessionEndpoint;
import java.util.ArrayList;
import java.util.UUID;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class RegistrationMessageTest {
  private ClientRegistrationMessage testGoodMsg;
  private String testGoodUsername;
  private UUID testGoodID;
  private int testGoodStatus;
  private String testGoodAddress;
  private Integer testGoodPort;
  
  public RegistrationMessageTest() {
    
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testGoodID = UUID.randomUUID();
    testGoodUsername = "Fred";
    testGoodStatus = 0;
    testGoodAddress = "192.168.0.1";
    testGoodPort = 80;
    String[] ips = new String[1];
    ips[0] = testGoodAddress;
    Integer[] ports = new Integer[1];
    ports[0] = testGoodPort;
    testGoodMsg = new ClientRegistrationMessage(testGoodID, ips, ports, testGoodUsername, testGoodStatus);
  }
  
  @After
  public void tearDown() {
    testGoodID = null;
    testGoodMsg = null;
    testGoodUsername = null;
  }

  /**
   * Test of getEndpoints method, of class ClientRegistrationMessage.
   */
  @Test
  public void testGetEndpoints() {
    System.out.println("getEndpoints");    
    ClientRegistrationMessage instance = testGoodMsg;
    ArrayList<SessionEndpoint> expResult = null;
    ArrayList<SessionEndpoint> result = instance.getEndpoints();
    assertEquals(1, result.size());
    assertEquals((long)testGoodPort, (long)result.get(0).port());
    assertEquals(testGoodAddress, result.get(0).ip().getHostAddress());
    
  }

  /**
   * Test of getUsername method, of class ClientRegistrationMessage.
   */
  @Test
  public void testGetName() {
    System.out.println("getName");
    ClientRegistrationMessage instance = testGoodMsg;
    String expResult = testGoodUsername;
    String result = instance.getUsername();
    assertEquals(expResult, result);
  }

  /**
   * Test of getStatus method, of class ClientRegistrationMessage.
   */
  @Test
  public void testGetStatus() {
    System.out.println("getStatus");
    ClientRegistrationMessage instance = testGoodMsg;
    int expResult = testGoodStatus;
    int result = instance.getStatus();
    assertEquals(expResult, result);
  }

  /**
   * Test of getUsername method, of class RegistrationMessage.
   */
  @Test
  public void testGetUsername() {
    System.out.println("getUsername");
    RegistrationMessage instance = null;
    String expResult = "";
    String result = instance.getUsername();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class RegistrationMessageImpl extends RegistrationMessage {

    public RegistrationMessageImpl() {
      super(null, null, "");
    }
  }

  public class RegistrationMessageImpl extends RegistrationMessage {

    public RegistrationMessageImpl() {
      super(null, null, "");
    }
  }
  
}
