/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class HealthCheckMessageTest {
  
  private HealthCheckMessage testMsg;
  private MessageType testMsgType;
  private String testUsername;
  private UUID testMsgID;
  
  public HealthCheckMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testMsgID = UUID.randomUUID();
    testUsername = "Fred";
    testMsgType = MessageType.HCHK;
    testMsg = new HealthCheckMessage(testMsgID, testUsername);
  }
  
  @After
  public void tearDown() {
    testMsgID = null;
    testMsgType = null;
    testMsg = null;
    testUsername = null;
  }

  /**
   * Test of getUsername method, of class HealthCheckMessage.
   */
  @Test
  public void testGetUsername() {
    System.out.println("getUsername");
    HealthCheckMessage instance = testMsg;
    String expResult = testUsername;
    String result = instance.getUsername();
    assertEquals(expResult, result);
  }
  
}
