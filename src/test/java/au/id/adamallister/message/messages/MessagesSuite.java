/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({au.id.adamallister.message.messages.ChatMessageTest.class, au.id.adamallister.message.messages.HealthCheckMessageTest.class, au.id.adamallister.message.messages.ChatRequestMessageTest.class, au.id.adamallister.message.messages.ChatRequestResponseMessageTest.class, au.id.adamallister.message.messages.ClientUpdateMessageTest.class, au.id.adamallister.message.messages.JSONMessageTest.class, au.id.adamallister.message.messages.HandShakeMessageTest.class, au.id.adamallister.message.messages.RegistrationMessageTest.class})
public class MessagesSuite {

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
}
