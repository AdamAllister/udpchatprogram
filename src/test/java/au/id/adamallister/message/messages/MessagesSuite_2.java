/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({au.id.adamallister.message.messages.MessageTest.class, au.id.adamallister.message.messages.ChatMessageTest.class, au.id.adamallister.message.messages.ServerRegistrationMessageTest.class, au.id.adamallister.message.messages.HealthCheckMessageTest.class, au.id.adamallister.message.messages.ChatRequestMessageTest.class, au.id.adamallister.message.messages.ChatRequestResponseMessageTest.class, au.id.adamallister.message.messages.ClientUpdateMessageTest.class, au.id.adamallister.message.messages.HandShakeMessageTest.class, au.id.adamallister.message.messages.RegistrationMessageTest.class, au.id.adamallister.message.messages.ClientRegistrationMessageTest.class})
public class MessagesSuite {
  
}
