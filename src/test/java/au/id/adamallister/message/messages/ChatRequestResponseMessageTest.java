/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ChatRequestResponseMessageTest {
  
  private ChatRequestResponseMessage testGoodMsg;
  private UUID testGoodID;
  private String testGoodRequester; 
  private String testGoodResponder; 
  private boolean testGoodResponse;
  private UUID testGoodChatID;
  
  public ChatRequestResponseMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testGoodID = UUID.randomUUID();
    testGoodRequester = "Fred";
    testGoodResponder = "Bob";
    testGoodResponse = true;
    testGoodChatID = UUID.randomUUID();
    
    testGoodMsg = new ChatRequestResponseMessage(testGoodID, testGoodRequester, 
        testGoodResponder, testGoodResponse, testGoodChatID);
    
  }
  
  @After
  public void tearDown() {
    testGoodID = null;
    testGoodRequester = null;
    testGoodResponder = null;
    testGoodResponse = false;
    testGoodChatID = null;
    testGoodMsg = null;
  }

  /**
   * Test of getResponse method, of class ChatRequestResponseMessage.
   */
  @Test
  public void testGetResponse() {
    System.out.println("getResponse");
    ChatRequestResponseMessage instance = testGoodMsg;
    boolean expResult = true;
    boolean result = instance.getResponse();
    assertEquals(expResult, result);
  }
  
}
