/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class JSONMessageTest {
  
  private Message testMsg;
  private MessageType testMsgType;
  private UUID testMsgID;
  
  public JSONMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {

  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testMsgID = UUID.randomUUID();
    testMsgType = MessageType.HAND;
    testMsg = new JSONMessageImpl(testMsgID, testMsgType);
  }
  
  @After
  public void tearDown() {
    testMsgID = null;
    testMsgType = null;
    testMsg = null;
  }

  /**
   * Test of getType method, of class Message.
   */
  @Test
  public void testGetType() {
    System.out.println("getType");
    Message instance = testMsg;
    MessageType expResult = MessageType.HAND;
    MessageType result = instance.getType();
    assertEquals(expResult, result);
  }

  /**
   * Test of getMessageUID method, of class Message.
   */
  @Test
  public void testGetMessageUID() {
    System.out.println("getMessageUID");
    Message instance = testMsg;
    UUID expResult = testMsgID;
    UUID result = instance.getMessageUID();
    assertEquals(expResult, result);
  }

  public class JSONMessageImpl extends Message {

    public JSONMessageImpl(UUID testID, MessageType type) {
      super(type, testID);
    }
  }  
}
