/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.packet.SessionEndpoint;
import java.util.ArrayList;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ClientUpdateMessageTest {
  
  private ClientUpdateMessage testGoodMsg;
  private UUID testGoodID;
  private String[] testGoodAddresses; 
  private Integer[] testGoodPorts; 
  private boolean testGoodStatus; 
  private String testGoodUsername;
  
  public ClientUpdateMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testGoodID = UUID.randomUUID();
    testGoodAddresses = new String[1];
    testGoodAddresses[0] = "192.168.0.1";
    testGoodPorts = new Integer[1];
    testGoodPorts[0] = 80;
    testGoodStatus = true;
    testGoodUsername = "Fred";
    
    testGoodMsg = new ClientUpdateMessage(testGoodID, testGoodAddresses, 
        testGoodPorts, testGoodStatus, testGoodUsername);
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of isOnline method, of class ClientUpdateMessage.
   */
  @Test
  public void testIsOnline() {
    System.out.println("isOnline");
    ClientUpdateMessage instance = testGoodMsg;
    boolean expResult = true;
    boolean result = instance.isOnline();
    assertEquals(expResult, result);
  }

  /**
   * Test of getEndpoints method, of class ClientUpdateMessage.
   */
  @Test
  public void testGetEndpoints() {
    System.out.println("getEndpoints");
    ClientUpdateMessage instance = testGoodMsg;
    ArrayList<SessionEndpoint> expResult = null;
    ArrayList<SessionEndpoint> result = instance.getEndpoints();
    assertEquals(1, result.size());
    assertEquals(testGoodAddresses[0] ,result.get(0).ip().getHostName());
    assertTrue(testGoodPorts[0] == result.get(0).port());
  }

  /**
   * Test of getName method, of class ClientUpdateMessage.
   */
  @Test
  public void testGetName() {
    System.out.println("getName");
    ClientUpdateMessage instance = testGoodMsg;
    String expResult = testGoodUsername;
    String result = instance.getName();
    assertEquals(expResult, result);
  }
  
}
