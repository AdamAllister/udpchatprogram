/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class HandShakeMessageTest {
  
  private HandShakeMessage testMsg;
  private MessageType testMsgType;
  private String testUsername;
  private UUID testMsgID;
  
  public HandShakeMessageTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    testMsgID = UUID.randomUUID();
    testUsername = "Fred";
    testMsgType = MessageType.HAND;
    testMsg = new HandShakeMessage(testMsgID, testUsername);
  }
  
  @After
  public void tearDown() {
    testMsgID = null;
    testMsgType = null;
    testMsg = null;
    testUsername = null;
  }

  /**
   * Test of getUsername method, of class HandShakeMessage.
   */
  @Test
  public void testGetUsername() {
    System.out.println("getUsername");
    HandShakeMessage instance = null;
    String expResult = "";
    String result = instance.getUsername();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }
}
