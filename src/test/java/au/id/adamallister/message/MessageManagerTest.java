/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.Message;
import au.id.adamallister.packet.PacketGroup;
import java.net.InetAddress;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class MessageManagerTest {
  
  public MessageManagerTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of send method, of class MessageManager.
   */
  @Test
  public void testSend() {
    System.out.println("send");
    Message msg = null;
    InetAddress address = null;
    int port = 0;
    MessageManager instance = null;
    instance.send(msg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of isRunning method, of class MessageManager.
   */
  @Test
  public void testIsRunning() {
    System.out.println("isRunning");
    MessageManager instance = null;
    boolean expResult = false;
    boolean result = instance.isRunning();
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of kill method, of class MessageManager.
   */
  @Test
  public void testKill() {
    System.out.println("kill");
    MessageManager instance = null;
    instance.kill();
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of onNewPacketGroup method, of class MessageManager.
   */
  @Test
  public void testOnNewPacketGroup() {
    System.out.println("onNewPacketGroup");
    PacketGroup pg = null;
    MessageManager instance = null;
    instance.onNewPacketGroup(pg);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of processJSONObject method, of class MessageManager.
   */
  @Test
  public void testProcessJSONObject() {
    System.out.println("processJSONObject");
    JSONObject json = null;
    InetAddress address = null;
    int port = 0;
    MessageManager instance = null;
    instance.processJSONObject(json, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of fromPacketGroupToJSON method, of class MessageManager.
   */
  @Test
  public void testFromPacketGroupToJSON() {
    System.out.println("fromPacketGroupToJSON");
    PacketGroup pg = null;
    MessageManager instance = null;
    JSONObject expResult = null;
    JSONObject result = instance.fromPacketGroupToJSON(pg);
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class MessageManagerImpl extends MessageManager {

    public MessageManagerImpl() {
      super(null, 0, 0L);
    }

    public void processJSONObject(JSONObject json, InetAddress address, int port) {
    }
  }
  
}
