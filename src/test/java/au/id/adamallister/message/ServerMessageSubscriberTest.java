/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.ChatRequestMessage;
import au.id.adamallister.message.messages.ChatRequestResponseMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import au.id.adamallister.message.messages.RegistrationMessage;
import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public class ServerMessageSubscriberTest {
  
  public ServerMessageSubscriberTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of serverRegistration method, of class ServerMessageSubscriber.
   */
  @Test
  public void testServerRegistration() {
    System.out.println("serverRegistration");
    RegistrationMessage regMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerMessageSubscriber instance = new ServerMessageSubscriberImpl();
    instance.serverRegistration(regMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverChatRequest method, of class ServerMessageSubscriber.
   */
  @Test
  public void testServerChatRequest() {
    System.out.println("serverChatRequest");
    ChatRequestMessage rqstMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerMessageSubscriber instance = new ServerMessageSubscriberImpl();
    instance.serverChatRequest(rqstMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverChatRequestResponse method, of class ServerMessageSubscriber.
   */
  @Test
  public void testServerChatRequestResponse() {
    System.out.println("serverChatRequestResponse");
    ChatRequestResponseMessage rspnMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerMessageSubscriber instance = new ServerMessageSubscriberImpl();
    instance.serverChatRequestResponse(rspnMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheck method, of class ServerMessageSubscriber.
   */
  @Test
  public void testServerHealthCheck() {
    System.out.println("serverHealthCheck");
    HealthCheckMessage hckMsg = null;
    InetAddress address = null;
    int port = 0;
    ServerMessageSubscriber instance = new ServerMessageSubscriberImpl();
    instance.serverHealthCheck(hckMsg, address, port);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheckFail method, of class ServerMessageSubscriber.
   */
  @Test
  public void testServerHealthCheckFail() {
    System.out.println("serverHealthCheckFail");
    InetAddress serverAddress = null;
    int serverPort = 0;
    ServerMessageSubscriber instance = new ServerMessageSubscriberImpl();
    instance.serverHealthCheckFail(serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of serverHealthCheckResponse method, of class ServerMessageSubscriber.
   */
  @Test
  public void testServerHealthCheckResponse() {
    System.out.println("serverHealthCheckResponse");
    HealthCheckMessage hckMsg = null;
    InetAddress serverAddress = null;
    int serverPort = 0;
    ServerMessageSubscriber instance = new ServerMessageSubscriberImpl();
    instance.serverHealthCheckResponse(hckMsg, serverAddress, serverPort);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  public class ServerMessageSubscriberImpl implements ServerMessageSubscriber {

    public void serverRegistration(RegistrationMessage regMsg, InetAddress address, int port) {
    }

    public void serverChatRequest(ChatRequestMessage rqstMsg, InetAddress address, int port) {
    }

    public void serverChatRequestResponse(ChatRequestResponseMessage rspnMsg, InetAddress address, int port) {
    }

    public void serverHealthCheck(HealthCheckMessage hckMsg, InetAddress address, int port) {
    }

    public void serverHealthCheckFail(InetAddress serverAddress, int serverPort) {
    }

    public void serverHealthCheckResponse(HealthCheckMessage hckMsg, InetAddress serverAddress, int serverPort) {
    }
  }
  
}
