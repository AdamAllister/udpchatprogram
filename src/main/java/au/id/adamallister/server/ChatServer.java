/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.server;

import au.id.adamallister.connection.ClientManager;
import au.id.adamallister.health.HealthCheckManager;
import au.id.adamallister.health.HealthCheckResendTimer;
import au.id.adamallister.message.ServerMessageManager;
import au.id.adamallister.packet.PacketManager;
import au.id.adamallister.packet.PacketReceiver;
import au.id.adamallister.util.UUIDWrapper;
import au.id.adamallister.util.timer.Timer;
import java.io.IOException;
import java.net.DatagramSocket;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Server Package - Chat Server (Main) Entry Point for the server. 
 * Sets up server socket and creates threads for new connections.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  09-April-2016 
 * Date Modified: 05-July-2016
 */
public class ChatServer {
  //Default port used if port not specified

  private static final int DEFAULT_PORT = 57330;
  private static final int PACKET_SIZE = 512;
  private static HashMap<String, Integer> command;

  /**
   * Main Entry Point for Server Side.
   * Sets up packets sizes, objects for server controller, Sockets, and loops
   * over console input until a quit command is sent or the server controller
   * stops running.
   * @param args The port to listen on.
   */
  @SuppressWarnings("null")
  public static void main(String[] args) {
    //Port to be used if specified
    int port;
    boolean running = true;

    //If there is at least one command argument set port as the first argument
    if (args.length > 0) {
      try {
        port = Integer.parseInt(args[0]);
      } catch (Exception e) {
        System.out.println("Could not parse arg[0] as int");
        System.out.println("Defaulting to port: " + DEFAULT_PORT);
        port = DEFAULT_PORT;
      }
    } else {
      System.out.println("No argurments passed");
      System.out.println("Defaulting to port: " + DEFAULT_PORT);
      port = DEFAULT_PORT;
    }

    DatagramSocket server = null;
    try {
      //Creates welcome socket for new connections
      server = new DatagramSocket(port);
      System.out.println("Welcoming socket created on port " + port);
    } catch (IOException e) {
      System.err.println("Welcoming socket creation has failed");
      running = false;
    }

    command = new HashMap<>();
    command.put("quit", 1);
    command.put("q", 1);
    command.put("exit", 1);
    command.put("end", 1);

    Timer<UUIDWrapper> healthET = new Timer();
    Timer<String> hcrtTimer = new Timer<>();
    hcrtTimer.start();
    HealthCheckResendTimer hcrt = new HealthCheckResendTimer(hcrtTimer);
    HealthCheckManager hcm = new HealthCheckManager(healthET, hcrt, 1500000000L, 4500000000L, 1000000000L);
    healthET.subscribe(hcm);
    ClientManager cm = new ClientManager();
    
    PacketReceiver serverPR = new PacketReceiver(server, 512);
    Timer<String> serverPacketExpTimer = new Timer<>();
    serverPacketExpTimer.start();
    PacketManager serverPM = new PacketManager(server, 512, serverPacketExpTimer, 2000000000L, serverPR);
    serverPR.subscribeToPackets(serverPM);
    serverPacketExpTimer.subscribe(serverPM);
    ServerMessageManager mngr = new ServerMessageManager(serverPM);
    serverPM.subscribeToCompletePacketGroups(mngr);
    
    ServerController controller = new ServerController(mngr, hcm, cm);
    mngr.subscribe(controller);
    
    Scanner scanner = new Scanner(System.in);
    
    
    String input = "";
    Integer commandCode;
    while (controller.isRunning()) {
      input = "";
      if(scanner.hasNext() && (input = scanner.next()) != null){
        input = input.toLowerCase();
      }

      commandCode = command.get(input);
      if (commandCode != null && commandCode == 1) {
        controller.kill();
        break;
      }
    }
  }
}
