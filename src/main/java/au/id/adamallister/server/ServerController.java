/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.server;

import au.id.adamallister.connection.SessionEndpoint;
import au.id.adamallister.health.HealthCheckManager;
import au.id.adamallister.message.messages.*;
import au.id.adamallister.connection.Client;
import au.id.adamallister.connection.ClientManager;
import au.id.adamallister.message.*;
import au.id.adamallister.util.UUIDWrapper;
import java.net.InetAddress;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.UUID;


//TODO Set up communication with web server
//TODO Set up database on web server
//TODO Set up TLS to web server
//TODO Set up
/**
 * Chat Server - Server Controller.
 * Manages incoming and outgoing messages and clients.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-May-2016 
 * Date Modified: 05-July-2016
 */
public class ServerController implements ServerMessageSubscriber {

  private ClientManager currentClients;

  private MessageManager serverMessageManager;
  
  private HealthCheckManager healthCheckMngr;

  /**
   * Constructor - Sets all fields to passed parameters.
   * @param msgManager - The server message manager that will send and receive messages.
   * @param hcm - The health check manager that will manage the lives of the clients in
   * conjunction with health checks.
   * @param cm - The client manager will manage the clients usernames, endpoints and
   * status.
   */
  public ServerController(ServerMessageManager msgManager, HealthCheckManager hcm, ClientManager cm) {
    if(msgManager == null)
      throw new InvalidParameterException("ServerMessageManager cannot be null.");
    if(hcm == null)
      throw new InvalidParameterException("HealthCheckManager cannot be null.");
    if(cm == null)
      throw new InvalidParameterException("ClientManager cannot be null.");
    currentClients = cm;
    healthCheckMngr = hcm;
    serverMessageManager = msgManager;
    
  }

  /**
   * Checks if the server message manager is still able to send and receive messages.
   * @return true if still able to send and receive.
   */
  public boolean isRunning() {
    return serverMessageManager.isRunning();
  }

  /**
   * Closes all sockets and clears the list of clients.
   */
  public void kill() {
    serverMessageManager.kill();
    currentClients.clear();
  }

  /**
   * Checks for valid message and processes registration, sending a response 
   * depending on result.
   * @param regMsg The client registration message to be processed
   * @param address Address of the client that sent the message
   * @param port port of the client that sent the message
   * @throws IllegalArgumentException if RegistrationMessage is not a ClientRegistrationMessage
   */
  @Override
  public void serverRegistration(RegistrationMessage regMsg, InetAddress address, int port) {
    ClientRegistrationMessage msg = null;
    if(regMsg.getType() == MessageType.CREG) {
      msg = (ClientRegistrationMessage)regMsg;
    } else
      throw new IllegalArgumentException("Client passed non server registration message.");
    
    ArrayList<SessionEndpoint> endpoints = msg.getEndpoints();
    Client client;

    UUIDWrapper msgId = regMsg.getMessageUID();
    ServerRegistrationMessage  response;

    if (endpoints == null) {
      response = new ServerRegistrationMessage(msgId, 10, "No endpoint sent", "");
    } else if (currentClients.getClient(regMsg.getUsername()) != null && !"Server".equals(regMsg.getUsername())) {
      response = new ServerRegistrationMessage(msgId, 20, "Username in use", "");
    } else {
      client = new Client(regMsg.getUsername(), endpoints);
      client.setOnline(true);
      ClientUpdateMessage updateMsg = new ClientUpdateMessage(new UUIDWrapper(UUID.randomUUID()), client.getEndpoints(), false, client.getUsername());
      sendToAll(updateMsg);
      for(Client onlineClient : currentClients.getClients()) {
        if(onlineClient.isOnline()) {
          ClientUpdateMessage onlineMsg = new ClientUpdateMessage(new UUIDWrapper(UUID.randomUUID()), onlineClient.getEndpoints(), onlineClient.isOnline(), onlineClient.getUsername());
          serverMessageManager.send(onlineMsg, client.getAddress(), client.getPort());
        }
      }
      currentClients.addClient(client);
      response = new ServerRegistrationMessage(msgId, 0, "Successfully signed in", regMsg.getUsername());
    }
    serverMessageManager.send(response, address, port);
    System.out.println("Registration from: " + address.getHostAddress() + ":" + port);
  }

  /**
   * Sends a message to all connected clients.
   * @param msg the message to send.
   */
  public void sendToAll(Message msg){
    for(Client client : currentClients.getClients()) {
      serverMessageManager.send(msg, client.getAddress(), client.getPort());
    }
  }
  
  /**
   * Gets a chat request from a client and passes it on to the responder client.
   * If either user is not officially online or the requester sends a message from
   * an endpoint they are not registered too, the message is ignored.
   * Part of the ServerMessageSubscriber interface.
   * @param rqstMsg - the message requesting a chat.
   * @param address - the address of the sender.
   * @param port - the port of the sender.
   */
  @Override
  public void serverChatRequest(ChatRequestRequestMessage rqstMsg, InetAddress address, int port) {
    Client responder = currentClients.getClient(rqstMsg.getResponderUsername());
    Client requester = currentClients.getClient(rqstMsg.getRequesterUsername());
    if (responder != null && requester != null && requester.isOnline() && requester.isActiveEndpoint(address, port) && responder.isOnline()) {
      serverMessageManager.send(rqstMsg, responder.getAddress(), responder.getPort());
    }
  }

  /**
   * Gets a chat response from a client and passes it on to the requesting client.
   * If either user is not officially online or the requester sends a message from
   * an endpoint they are not registered too, the message is ignored.
   * Part of the ServerMessageSubscriber interface.
   * @param rspnMsg - the message responding to a chat.
   * @param address - the address of the responder.
   * @param port - the port of the responder.
   */
  @Override
  public void serverChatRequestResponse(ChatRequestResponseMessage rspnMsg, InetAddress address, int port) {
    Client responder = currentClients.getClient(rspnMsg.getResponderUsername());
    Client requester = currentClients.getClient(rspnMsg.getRequesterUsername());
    if (responder != null && requester != null && responder.isOnline() && responder.isActiveEndpoint(address, port) && requester.isOnline()) {
      serverMessageManager.send(rspnMsg, requester.getAddress(), requester.getPort());
    }
  }

  /**
   * Gets a HealthCheckMessage from a client processes it if its a response to a
   * server health check or returns a message if it is not.
   * Drops the message if not from a client in the client manager.
   * Part of the ServerMessageSubscriber interface.
   * @param hckMsg - the HealthCheckMessage from the client.
   * @param address - the clients address.
   * @param port - the clients port.
   */
  @Override
  public void serverHealthCheck(HealthCheckMessage hckMsg, InetAddress address, int port) {
    Client client = currentClients.getClient(address, port);
    if (client != null && client.isOnline()) {
      if(healthCheckMngr.isResponse(hckMsg.getMessageUID(), client.getUsername()))
        healthCheckMngr.healthCheckResponse(hckMsg.getMessageUID(), client.getUsername());
      else
        this.serverMessageManager.sendOnce(hckMsg, address, port);
    }
    System.out.println("Health Check from: " + address.getHostAddress() + ":" + port);
  }

  /**
   * Receives a handshake message from a client and sends a response handshake back.
   * The response handshake has the same message id as the received one.
   * @param hndMsg the clients handshake message
   * @param address the clients address
   * @param port the clients port
   */
  @Override
  public void serverHandshake(HandShakeMessage hndMsg, InetAddress address, int port) {
    HandShakeMessage rsp = new HandShakeMessage(hndMsg.getMessageUID(), "server");
    serverMessageManager.sendOnce(rsp, address, port);
    System.out.println("Handshake from: " + address.getHostAddress() + ":" + port);
  }

  /**
   * Processes a client update message from a client.
   * Used by a client to sign out.
   * @param updMsg The clients Client Update Message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  @Override
  public void serverClientUpdate(ClientUpdateMessage updMsg, InetAddress address, int port) {
    Client client = currentClients.getClient(address, port);
    if(client != null && client.getUsername().equals(updMsg.getName())) {
      if(updMsg.isOnline() == false)
        client.setOnline(false);
    } 
  }
}