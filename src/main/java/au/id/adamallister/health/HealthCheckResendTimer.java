/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.health;

import au.id.adamallister.util.timer.DoTaskSubscriber;
import au.id.adamallister.util.timer.Timer;
import java.util.ArrayList;

/**
 * Health Package - HealthCheckResendTimer
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 28-June-2016 
 * Date Modified: 04-July-2016
 */
public class HealthCheckResendTimer implements DoTaskSubscriber<String> {

  private Timer<String> resendTimer;
  private HealthCheckHandler observer;

  /**
   * Constructor
   * @param timer The timer object
   */
  public HealthCheckResendTimer(Timer<String> timer) {
    resendTimer = timer;
  }
  
  /**
   * Kills the timer and removes the observer
   */
  public void kill(){
    resendTimer.kill();
    observer = null;
  }
  
  /**
   * Sets the observer to the passed HealthCheckHandler
   * @param observer
   */
  public void subscribe(HealthCheckHandler observer) {
    this.observer = observer;
  }
  
  /**
   * Adds an connection identifier to the resend timer to resend a health check
   * in nextSend nanoseconds.
   * @param name The connection identifier of a connection object.
   * @param nextSend The time in nanoseconds until the next health check should
   * be sent.
   */
  public void addItemToResend(String name, long nextSend){
    resendTimer.addItem(name, nextSend);
  }

  /**
   * Gets a list of connection identifiers to resend health checks to and notifies
   * observer.
   * @param resendItems A list of connection identifiers.
   */
  @Override
  public void doTask(ArrayList<String> resendItems) {
    for(String name : resendItems) {
      observer.sendHealthCheck(name);
    }
  }

}
