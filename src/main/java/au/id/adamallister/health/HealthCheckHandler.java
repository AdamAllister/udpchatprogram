/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.health;

/**
 * Health Package - HealthCheckHandler - An observer interface for health check
 * changes.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  28-June-2016
 * Date Modified: 04-July-2016
 */
public interface HealthCheckHandler {

  /**
   * Called when a connection object has no lives left.
   * @param name - The identifier of the connection object.
   */
  public void outOfLives(String name);

  /**
   * Called when it is time to send a new health check to a connected object.
   * @param name - The identifier of the connection object.
   */
  public void sendHealthCheck(String name);
}
