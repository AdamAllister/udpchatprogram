/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.health;

import au.id.adamallister.util.timer.Timer;
import au.id.adamallister.message.messages.HealthCheckMessage;
import au.id.adamallister.util.UUIDWrapper;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import au.id.adamallister.util.timer.DoTaskSubscriber;

/**
 * Health Package - Health Check Manager - Keeps track of the lives of connections,
 * the expiration of health checks and informs observers when to resend health 
 * checks.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 24-June-2016 
 * Date Modified: 04-July-2016
 */
public class HealthCheckManager implements DoTaskSubscriber<UUIDWrapper> {
  
  private ConcurrentHashMap<UUIDWrapper, String> healthChecks;
  private ConcurrentHashMap<UUIDWrapper, String> tempExpiredHealthChecks;
  private ConcurrentHashMap<String, Byte> lives;
  private Timer<UUIDWrapper> timer;
  private HealthCheckResendTimer resendTimer;
  private static final byte DEFAULT_LIVES = 3;
  private final long timeoutNanoseconds;
  private final long resendTimeNanoseconds;
  private final long keepExpiredNanoseconds;
  private HealthCheckHandler sub;

  /**
   * Constructor - Initializes hash maps and sets timer object, and timeout settings.
   * @param timer - The timer that will manage the expiration of health checks.
   * @param resendTimer - The timer that will manage the sending of new health checks.
   * @param timeoutNanoseconds - The expiration time of a health check.
   * @param keepExpiredNanoseconds - How long to keep expired health checks.
   * @param resendTimeNanoseconds - How long to wait before sending another 
   * health check to a client.
   */
  public HealthCheckManager(Timer<UUIDWrapper> timer, HealthCheckResendTimer resendTimer, long timeoutNanoseconds, long keepExpiredNanoseconds, long resendTimeNanoseconds) {
    healthChecks = new ConcurrentHashMap();
    tempExpiredHealthChecks = new ConcurrentHashMap();
    lives = new ConcurrentHashMap();
    this.timer = timer;
    this.resendTimer = resendTimer;
    this.timeoutNanoseconds = timeoutNanoseconds;
    this.keepExpiredNanoseconds = keepExpiredNanoseconds;
    this.resendTimeNanoseconds = resendTimeNanoseconds;
  }
  
  /**
   * Removes life from mapped name if not zero and removes the health check for that name-.
   * @param name the name of an object with lives.
   * @return true if name still has lives left.
   */
  private synchronized void healthCheckExpired(UUIDWrapper uuid) {
    if (healthChecks.containsKey(uuid)) {
      String name = healthChecks.get(uuid);
      healthChecks.remove(uuid);
      tempExpiredHealthChecks.put(uuid, name);
      timer.addItem(uuid, keepExpiredNanoseconds);
      if(lives.containsKey(name)) {
        byte lifeLeft = lives.get(name);
        if (lifeLeft > 0) {
          lifeLeft -= 1;
          lives.put(name, lifeLeft);
        }
        if(lifeLeft == 0 ){
          lives.remove(name);
          if(sub != null)
            sub.outOfLives(name);
        }
      }
    }
  }
  
  /**
   * Checks if the passed uuid is a health check the manager made and that it is
   * for the correct connection object.
   * Checks both current health checks and expired ones; if they are still in 
   * tempExpiredHealthChecks.
   * @param uuid The uuid of the health check.
   * @param name The identifier of the connection object.
   * @return True if the manager has the uuid and it's for the connection object.
   */
  public boolean isResponse(UUIDWrapper uuid, String name){    
    return (healthChecks.containsKey(uuid) 
        && (name != null && healthChecks.get(uuid).equals(name))) 
        || (tempExpiredHealthChecks.containsKey(uuid)
        && (name != null && tempExpiredHealthChecks.get(uuid).equals(name)));
  }
  
  /**
   * Looks for matching UUID in health checks and if found resets lives for name.
   * This code does nothing if the name is not in health checks or the uuid is different
   * @param uuid the UUID wrapper of the health check message.
   * @param name the name of an object with lives.
   */  
  public void healthCheckResponse(UUIDWrapper uuid, String name) {
    if (healthChecks.containsKey(uuid) && healthChecks.get(uuid).equals(name) && lives.contains(name)) {
      healthChecks.remove(uuid);
      byte newLives = DEFAULT_LIVES;
      lives.put(name, newLives);
    }
  }

  /**
   * Attempts to create a health check message and add it to the health check
   * timer.
   * @param uuid The UUID of the health check.
   * @param name The identifier of the connection object.
   * @return A health check message with the passed UUID.
   * @throws IllegalArgumentException if the UUID or name is null.
   */
  public HealthCheckMessage createHealthCheckMessage(UUIDWrapper uuid, String name) {
    if (name == null || uuid == null) {
      throw new IllegalArgumentException("Null parameter passed");
    }
    HealthCheckMessage msg = new HealthCheckMessage(uuid);
    healthChecks.put(uuid, name);
    timer.addItem(uuid, timeoutNanoseconds);
    resendTimer.addItemToResend(name, resendTimeNanoseconds);
    return msg;
  }
  
  /**
   * Adds or Resets the lives of the connection object, and sets the health check
   * resend time.
   * @param name The identifier of the connection object.
   */
  public void addOrRenewConnection(String name) {
    lives.put(name, DEFAULT_LIVES); 
    resendTimer.addItemToResend(name, resendTimeNanoseconds);
  }
  
  /**
   * Removes a connection object from the manager.
   * Note: Does not remove current health checks or remove any entries
   * in the resend timer.
   * @param name The identifier of the connection object.
   */
  public void removeConnection(String name) {
    if(lives.containsKey(name)) {
      lives.remove(name); 
    }
  }
  
  ////TODO Make manager restartable or prevent use after kill called.
  /**
   * Stops all timers and clears all health checks and lives entries.
   * Note: Object no longer functional after this method is called.
   */
  public void kill(){
    timer.kill();
    resendTimer.kill();
    healthChecks.clear();
    tempExpiredHealthChecks.clear();
    lives.clear();
  }

  /**
   * Sets the observer to the passed handler.
   * The handler(observer) will be called when a client is out of lives or if the
   * resend timer reaches the required wait time to send a health check.
   * @param handler The observer of the health check manager.
   */
  public void subscribe(HealthCheckHandler handler){
    sub = handler;
    resendTimer.subscribe(handler);
  }
  
  /**
   * Called by the health check timer when one or more health checks expire.
   * @param exp A list of expired health checks UUIDs.
   */
  @Override
  public void doTask(ArrayList<UUIDWrapper> exp) {
    for(UUIDWrapper uuid : exp) {
      if(tempExpiredHealthChecks.containsKey(uuid))
        tempExpiredHealthChecks.remove(uuid);
      else if(healthChecks.containsKey(uuid))
        healthCheckExpired(uuid);
    }
  }
}
