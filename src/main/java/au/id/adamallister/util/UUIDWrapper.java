/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.util;

import java.util.UUID;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 29-May-2016 Date Modified: 29-May-2016
 */
public class UUIDWrapper implements Comparable<UUID>{

  private UUID uuid;

  public UUIDWrapper(UUID uuid){
    this.uuid = uuid;
  }

  public UUID getUUID() {
    return uuid;
  }
  
  public String toString() {
    return uuid.toString();
  }

  public long getMostSignificantBits() {
    return uuid.getMostSignificantBits();
  }
  
  public long getLeastSignificantBits() {
    return uuid.getLeastSignificantBits();
  }

  @Override
  public int compareTo(UUID o) {
    return uuid.compareTo(o);
  }
}
