/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.util.timer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Timer Package - Timer - A general class that keeps track of items and passes
 * them to subscribers when their time is reached.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 27-June-2016
 * Date Modified: 04-July-2016
 * @param <T> The type of item that will be put in the timer.
 */
public class Timer<T> extends Thread {

  private ConcurrentHashMap<T, Long> timedItems;
  private boolean running;
  private ArrayList<DoTaskSubscriber<T>> subs;

  /**
   * Constructor
   */
  public Timer() {
    running = false;
    timedItems = new ConcurrentHashMap<>();
    subs = new ArrayList<>();
  }

  /**
   * Loops through the items and checks which ones have expired, then notifies
   * any subscribers.
   * When it stops running, all items and subscribers are cleared.
   */
  @Override
  public void run() {
    running = true;
    ArrayList<T> expired = new ArrayList<>();
    while (running) {
      expired.clear();
      long currentTime = System.nanoTime();
      synchronized (this) {
        Iterator it = timedItems.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry pair = (Map.Entry) it.next();
          if ((currentTime >= (Long) (pair.getValue()))) {
            expired.add((T) (pair.getKey()));
            it.remove(); // avoids a ConcurrentModificationException
          }
        }
        notifySubscribers(expired);
      }
    }
    timedItems.clear();
    subs.clear();
  }

  private void notifySubscribers(ArrayList<T> expired) {
    if (expired.size() > 0 && subs.size() > 0) {
      for (DoTaskSubscriber sub : subs) {
        sub.doTask(expired);
      }
    }
  }
  
  /**
   * Adds a DoTaskSubscriber to the subscriber list if not already in there
   * @param sub The subscriber to add.
   */
  public void subscribe(DoTaskSubscriber<T> sub){
    if(!subs.contains(sub))
      subs.add(sub);
  }
  
  /**
   * Removes a DoTaskSubscriber from the subscriber list if already in there
   * @param sub The subscriber to remove.
   */
  public void unSubscribe(DoTaskSubscriber<T> sub){
    if(subs.contains(sub))
      subs.remove(sub);
  }

  /**
   * Adds an item to the timer if not already in it and sets its timeout time 
   * to the current time plus the passed time.
   * @param identifier The object related or a value to identify the object.
   * @param timeoutTime The amount of time to wait in nanoseconds.
   */
  public void addItem(T identifier, long timeoutTime) {
    if(!timedItems.containsKey(identifier))
      timedItems.put(identifier, System.nanoTime() + timeoutTime);
    else
      throw new IllegalArgumentException("Attempted to add item to expiration timer with existing identifier");
  }

  /**
   * Returns the state of timer.
   * @return True if the timer is still looping.
   */
  public boolean isRunning(){
    return this.running;
  }
  
  /**
   * Breaks the timer loop.
   */
  public void kill(){
    this.running = false;
  }
}
