/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.util.timer;

import java.util.ArrayList;

/**
 * Timer Package - DoTaskSubscriber - An observer interface for the Timer object.
 * @author Adam Allister <adam.allister@outlook.com>
 * @param <T> The type of the items. 
 * Date Created:  26-June-2016
 * Date Modified: 04-July-2016
 */
public interface DoTaskSubscriber<T> {

  /**
   * Do something using the passed items.
   * @param items A list of items whose time was reached.
   */
  public void doTask(ArrayList<T> items);
}