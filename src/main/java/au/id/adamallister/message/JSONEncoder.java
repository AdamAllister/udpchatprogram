/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.*;
import au.id.adamallister.util.UUIDWrapper;
import java.nio.charset.Charset;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Message Package - JSONEncoder - A class used to convert messages to
 JSON Objects and vice versa.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  22-APR-2016
 * Date Modified: 06-JUL-2016
 */
public class JSONEncoder implements MessageEncoder{

  /**
   * Converts a JSON Object to a Message Object.
   * @param jsonObject The JSON Object to convert.
   * @return A message object if JSON has the correct fields for one message
   * type, or otherwise null.
   */
  public Message decode(byte[] jsonBytes) {
    JSONObject jsonObject = new JSONObject(new String(jsonBytes, java.nio.charset.StandardCharsets.UTF_8));
    MessageType type;
    UUIDWrapper id;
    Message msg = null;

    try {
      type = jsonObject.getEnum(MessageType.class, "type");
    } catch (JSONException e) {
      System.out.println("fromJSONObject - Failed to find valid message type in JSON Object");
      return null;
    }

    if (!hasCorrectFields(type, jsonObject)) {
      System.out.println("fromJSONObject - JSON Object does not have required fields");
      return null;
    }

    try {
      id = new UUIDWrapper(new UUID(jsonObject.getLong("uid-1"), jsonObject.getLong("uid-2")));
    } catch (JSONException e) {
      System.out.println("fromJSONObject - JSON Object does not have valid ID");
      return null;
    }

    String[] addressList;
    Integer[] portList;
    switch (type) {
      case MSG:
        msg = new ChatMessage(id, jsonObject.getString("from"),
            new UUIDWrapper(new UUID(jsonObject.getLong("chat-uid-1"), jsonObject.getLong("chat-uid-2"))),
            jsonObject.getLong("time-utc"), jsonObject.getString("msg"));
        break;
      case HCHK:
        msg = new HealthCheckMessage(id);
        break;
      case UPD:
        addressList = new String[jsonObject.getJSONArray("addresses").length()];
        portList = new Integer[jsonObject.getJSONArray("ports").length()];
        jsonObject.getJSONArray("addresses").toList().toArray(addressList);
        jsonObject.getJSONArray("ports").toList().toArray(portList);
        msg = new ClientUpdateMessage(id, addressList, portList,
            jsonObject.getBoolean("connecting"), jsonObject.getString("username"));
        break;
      case CREG:
        addressList = new String[jsonObject.getJSONArray("addresses").length()];
        portList = new Integer[jsonObject.getJSONArray("ports").length()];
        jsonObject.getJSONArray("addresses").toList().toArray(addressList);
        jsonObject.getJSONArray("ports").toList().toArray(portList);
        msg = new ClientRegistrationMessage(id, addressList, portList,
            jsonObject.getString("username"));
        break;
      case SREG:
        msg = new ServerRegistrationMessage(id, jsonObject.getInt("code"), jsonObject.getString("msg"), jsonObject.getString("username"));
        break;
      case RQST:
        msg = new ChatRequestRequestMessage(id,
            jsonObject.getString("responder"),
            jsonObject.getString("requester"),
            new UUIDWrapper(new UUID(jsonObject.getLong("chat-uid-1"), jsonObject.getLong("chat-uid-2"))));
        break;
      case RSPN:
        msg = new ChatRequestResponseMessage(id,
            jsonObject.getString("responder"),
            jsonObject.getString("requester"),
            jsonObject.getBoolean("response"),
            new UUIDWrapper(new UUID(jsonObject.getLong("chat-uid-1"), jsonObject.getLong("chat-uid-2"))));
        break;
      case HAND:
        msg = new HandShakeMessage(id, jsonObject.getString("username"));
        break;
      default:
        throw new UnsupportedOperationException("Message Type not supported");
    }
    return msg;
  }

  /**
   * Converts a Message Object to a JSON Object.
   * Creates a JSON Object based on what kind of message it is.
   * @param msg The message object to convert.
   * @return The JSON Object if Message type supported, or null if not.
   */
  public byte[] encode(Message msg) {
    if(msg == null)
      return null;
    
    MessageType type = msg.getType();
    JSONObject json = new JSONObject();
    UUIDWrapper id = msg.getMessageUID();
    json.put("type", type);
    json.put("uid-1", id.getMostSignificantBits());
    json.put("uid-2", id.getLeastSignificantBits());
    
    switch (type) {
      case MSG:
        ChatMessage chat = (ChatMessage)msg;
        json.put("from"     , chat.getFromName());
        json.put("chat-uid-1", chat.getChatID().getMostSignificantBits());
        json.put("chat-uid-2", chat.getChatID().getLeastSignificantBits());
        json.put("time-utc" , chat.getTimeMillis());
        json.put("msg"      , chat.getMessage());
        break;
      case HCHK:
        HealthCheckMessage health = (HealthCheckMessage)msg;
        break;
      case UPD:
        ClientUpdateMessage upd = (ClientUpdateMessage)msg;
        String[] updIps = new String[upd.getEndpoints().size()];
        int[] updPorts = new int[upd.getEndpoints().size()];
        for(int i = 0; i < upd.getEndpoints().size(); i++) {
          updIps[i] = upd.getEndpoints().get(i).ip().getHostAddress();
          updPorts[i] = upd.getEndpoints().get(i).port();
        }
        json.put("addresses"  , updIps);
        json.put("ports"      , updPorts);
        json.put("connecting" , upd.isOnline());
        json.put("username"   , upd.getName());
        break;
      case CREG:
        ClientRegistrationMessage cReg = (ClientRegistrationMessage)msg;
        String[] regIps = new String[cReg.getEndpoints().size()];
        int[] regPorts = new int[cReg.getEndpoints().size()];
        for(int i = 0; i < cReg.getEndpoints().size(); i++) {
          regIps[i] = cReg.getEndpoints().get(i).ip().getHostAddress();
          regPorts[i] = cReg.getEndpoints().get(i).port();
        }
        json.put("addresses", regIps);
        json.put("ports"    , regPorts);
        json.put("username" , cReg.getUsername());
        break;
      case SREG:
        ServerRegistrationMessage sReg = (ServerRegistrationMessage)msg;
        json.put("code", sReg.getCode());
        json.put("msg" , sReg.getMsg());
        json.put("username" , sReg.getUsername());
        break;  
      case RQST:
        ChatRequestRequestMessage rqst = (ChatRequestRequestMessage)msg;
        json.put("requester", rqst.getRequesterUsername());
        json.put("responder", rqst.getResponderUsername());
        json.put("chat-uid-1", rqst.getChatID().getMostSignificantBits());
        json.put("chat-uid-2", rqst.getChatID().getLeastSignificantBits());
        break;
      case RSPN:
        ChatRequestResponseMessage rspn = (ChatRequestResponseMessage)msg;
        json.put("requester", rspn.getRequesterUsername());
        json.put("responder", rspn.getResponderUsername());
        json.put("response" , rspn.getResponse());
        json.put("chat-uid-1", rspn.getChatID().getMostSignificantBits());
        json.put("chat-uid-2", rspn.getChatID().getLeastSignificantBits());
        break;
      case HAND:
        HandShakeMessage hand = (HandShakeMessage)msg;
        json.put("username" , hand.getUsername());
        break;
      default:
        json = null;
        throw new UnsupportedOperationException("Message Type not supported");
    }
    return json.toString().getBytes(java.nio.charset.StandardCharsets.UTF_8);
  }
  
  private static boolean hasCorrectFields(MessageType type, JSONObject json) {
    boolean correct = json.has("uid-1") && json.has("uid-2");
    switch (type) {
      case MSG:
        correct = correct && json.has("from") && json.has("chat-uid-1") && 
            json.has("chat-uid-2") && json.has("time-utc") && json.has("msg");
        break;
      case HCHK:
        break;
      case UPD:
        correct = correct && json.has("addresses") && json.has("ports")
            && json.has("connecting") && json.has("username");
        break;
      case CREG:
        correct = correct && json.has("addresses") && json.has("ports")
            && json.has("username");
        break;
      case SREG:
        correct = correct && json.has("code") && json.has("msg") 
            && json.has("username");;
        break;
      case RQST:
        correct = correct && json.has("responder") && json.has("requester")
            && json.has("chat-uid-1") && json.has("chat-uid-2");
        break;
      case RSPN:
        correct = correct && json.has("responder") && json.has("requester")
            && json.has("chat-uid-1") && json.has("chat-uid-2") && json.has("response");
        break;
      case HAND:
        correct = correct && json.has("username");
        break;
      default:
        throw new UnsupportedOperationException("Message Type not supported");
    }
    return correct;
  }
}
