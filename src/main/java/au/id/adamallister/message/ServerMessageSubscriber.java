/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.ChatRequestRequestMessage;
import au.id.adamallister.message.messages.ChatRequestResponseMessage;
import au.id.adamallister.message.messages.ClientUpdateMessage;
import au.id.adamallister.message.messages.HandShakeMessage;
import au.id.adamallister.message.messages.HealthCheckMessage;
import au.id.adamallister.message.messages.RegistrationMessage;
import java.net.InetAddress;

/**
 * Message Package - ServerMessageSubscriber Interface
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016
 * Date Modified: 06-JUL-2016
 */
public interface ServerMessageSubscriber {

  /**
   * Process a server registration message.
   * @param regMsg The registration message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  public void serverRegistration(RegistrationMessage regMsg, InetAddress address, int port);

  /**
   * Process a server chat request message.
   * @param rqstMsg The chat request message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  public void serverChatRequest(ChatRequestRequestMessage rqstMsg, InetAddress address, int port);

  /**
   * Process a server chat response message.
   * @param rspnMsg The chat response message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  public void serverChatRequestResponse(ChatRequestResponseMessage rspnMsg, InetAddress address, int port);

  /**
   * Process a server health check message.
   * @param hckMsg The health check message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  public void serverHealthCheck(HealthCheckMessage hckMsg, InetAddress address, int port);

  /**
   * Process a server handshake message.
   * @param hndMsg The handshake message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  public void serverHandshake(HandShakeMessage hndMsg, InetAddress address, int port);

  /**
   * Process a server client update message.
   * @param updMsg The client update message.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  public void serverClientUpdate(ClientUpdateMessage updMsg, InetAddress address, int port);
}
