/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import java.io.Serializable;

/**
 * Chat Client - MessageType
 * An enumeration for different kinds of messages sent between client and server
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  13-APR-2016
 * Date Modified: 05-JUL-2016
 */
public enum MessageType implements Serializable{

  /**
   * CHAT MESSAGE.
   */
  MSG, 

  /**
   * HEALTH CHECK.
   */
    HCHK,

  /**
   * CLIENT STATUS UPDATE.
   */
    UPD,

  /**
   * SERVER REGISTRATION.
   */
    SREG,

  /**
   * CLIENT REGISTRATION.
   */
    CREG,

  /**
   * CHAT REQUEST.
   */
    RQST,

  /**
   * CHAT RESPONSE.
   */
    RSPN,

  /**
   * HANDSHAKE.
   */
    HAND,
    
  /**
   * RELAY.
   */
    RLY,
    
  /**
   * RELAY.
   */
  UND  
}