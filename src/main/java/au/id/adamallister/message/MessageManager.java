/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.Message;
import au.id.adamallister.packet.PacketGroup;
import au.id.adamallister.packet.PacketGroupSubscriber;
import au.id.adamallister.packet.PacketManager;
import au.id.adamallister.packet.PacketUtility;
import java.net.InetAddress;
import java.security.InvalidParameterException;

/**
 * Message Package - MessageManager - Manages the sending, receiving and
 * converting of packets to messages and vice versa for a particular packet
 * manager.
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 11-MAY-2016 Date Modified: 06-JUL-2016
 */
public abstract class MessageManager implements PacketGroupSubscriber {

  /**
   * The packet manager that sends and receives packet objects.
   */
  protected PacketManager pm;

  /**
   * Constructor
   *
   * @param pm The packet manager that will send and receive packet groups
   * through the message manager.
   */
  public MessageManager(PacketManager pm) {
    if(pm == null)
      throw new InvalidParameterException("PacketManager must not be null");
    this.pm = pm;
  }

  /**
   * Sends a message to the supplied endpoint. Converts the message to JSON,
   * which is then converted to a group of packets. Stops the packet manager if
   * an IOException is caught.
   *
   * @param msg The message to send.
   * @param address The address of the recipient.
   * @param port The port of the recipient.
   */
  public void send(Message msg, InetAddress address, int port) {
    if (pm.isRunning()) {
      pm.send(PacketUtility.generatePacketGroup(MessageUtility.encode(msg), pm.pSize, address, port));
    }
  }

  /**
   * Sends a message to the supplied endpoint once. There will be no attempts to
   * resend the message if it was not successfully received by the recipient.
   * Converts the message to JSON, which is then converted to a group of
   * packets. Stops the packet manager if an IOException is caught.
   *
   * @param msg The message to send.
   * @param address The address of the recipient.
   * @param port The port of the recipient.
   */
  public void sendOnce(Message msg, InetAddress address, int port) {
    if (pm.isRunning()) {
      pm.sendOnce(PacketUtility.generatePacketGroup(MessageUtility.encode(msg), pm.pSize, address, port));
    }
  }

  /**
   * Gets the state of the packet manager.
   *
   * @return
   */
  public boolean isRunning() {
    return pm.isRunning();
  }

  /**
   * Kills the packet manager.
   */
  public void kill() {
    pm.kill();
  }

  ////////////////////////////////////////////////
  ////                                        ////
  ////  Interface - Packet Group Subscriber   ////
  ////                                        ////
  ////////////////////////////////////////////////
  /**
   * Converts a PacketGroup to JSON and processes it if successful.
   *
   * @param pg The completed packet group.
   */
  @Override
  public void onNewPacketGroup(PacketGroup pg) {
    InetAddress packetAddress = pg.getAddress();
    int packetPort = pg.getPort();

    byte[] msg = pg.getCompleteData();
    if (msg != null) {
      processMessage(msg, packetAddress, packetPort);
    }
  }

  ////////////////////////////////////////////////  
  /**
   * Does something with a received JSON Object.
   *
   * @param msgBytes Encoded message as byte array.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  protected abstract void processMessage(byte[] msgBytes, InetAddress address, int port);

}
