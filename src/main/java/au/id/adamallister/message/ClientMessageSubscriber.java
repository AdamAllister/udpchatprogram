/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.message;

import au.id.adamallister.message.messages.*;
import java.net.InetAddress;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  11-MAY-2016
 * Date Modified: 06-JUL-2016
 */
public interface ClientMessageSubscriber {

  /** 
   * Processes a client chat message.
   * @param chatMsg The chat message.
   * @param address The senders address.
   * @param port The senders port.
   */
  public void clientMessage(ChatMessage chatMsg, InetAddress address, int port);

  /**
   * Processes a client health check message.
   * @param hckMsg The health check message.
   * @param address The senders address.
   * @param port The senders port.
   */
  public void clientHealthCheck(HealthCheckMessage hckMsg, InetAddress address, int port);

  /**
   * Processes a client handshake message.
   * @param hndMsg The handshake message.
   * @param address The senders address.
   * @param port The senders port.
   */
  public void clientHandShake(HandShakeMessage hndMsg, InetAddress address, int port);
}
