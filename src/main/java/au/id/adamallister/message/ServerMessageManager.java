/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.*;
import au.id.adamallister.packet.PacketManager;
import java.net.InetAddress;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 * Message Package - ServerMessageManager - Manages the sending, receiving and 
 * converting of packets to messages and vice versa for the server.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  09-APR-2016
 * Date Modified: 06-JUL-2016
 */
public class ServerMessageManager extends MessageManager {

  private ArrayList<ServerMessageSubscriber> serverMsgSubscribers;
  
  /**
   * Constructor.
   * @param pm
   */
  public ServerMessageManager(PacketManager pm) {
    super(pm);
    serverMsgSubscribers = new ArrayList<>();
  }

  /**
   * Add an observer to the message manager.
   * @param sub An observer that will process messages from the server.
   */
  public synchronized void subscribe(ServerMessageSubscriber sub){
    if(sub == null)
      throw new InvalidParameterException("ServerMessageSubscriber must not be null");
    if(!serverMsgSubscribers.contains(sub)) {
      serverMsgSubscribers.add(sub);
    }
  }

  ////////////////////////////////////////////////
  ////                                        ////
  ////      Abstract - Message Manager        ////
  ////                                        ////
  ////////////////////////////////////////////////
  
  @Override
  protected void processMessage(byte[] msgBytes, InetAddress address, int port) {
    Message msg = MessageUtility.decode(msgBytes);
    switch (msg.getType()) {
      case CREG:
        ClientRegistrationMessage regMsg = (ClientRegistrationMessage) msg;
        notifyRegistrationMessage(regMsg, address, port);
        break;
      case RQST:
        ChatRequestRequestMessage rqstMsg = (ChatRequestRequestMessage) msg;
        notifyChatRequestMessage(rqstMsg, address, port);
        break;
      case RSPN:
        ChatRequestResponseMessage rspnMsg = (ChatRequestResponseMessage) msg;
        notifyChatRequestResponseMessage(rspnMsg, address, port);
        break;
      case HCHK:
        notifyHealthCheck((HealthCheckMessage) msg, address, port);
        break;
    }
  }

  ////////////////////////////////////////////////
  
  private synchronized void notifyRegistrationMessage(ClientRegistrationMessage registration, InetAddress address, int port) {
    for (ServerMessageSubscriber sub : serverMsgSubscribers) {
      sub.serverRegistration(registration, address, port);
    }
  }

  private synchronized void notifyChatRequestMessage(ChatRequestRequestMessage request, InetAddress address, int port) {
    for (ServerMessageSubscriber sub : serverMsgSubscribers) {
      sub.serverChatRequest(request, address, port);
    }
  }

  private synchronized void notifyChatRequestResponseMessage(ChatRequestResponseMessage response, InetAddress address, int port) {
    for (ServerMessageSubscriber sub : serverMsgSubscribers) {
      sub.serverChatRequestResponse(response, address, port);
    }
  }

  private synchronized void notifyHealthCheck(HealthCheckMessage healthCheck, InetAddress address, int port) {
    for (ServerMessageSubscriber sub : serverMsgSubscribers) {
      sub.serverHealthCheck(healthCheck, address, port);
    }
  }
}
