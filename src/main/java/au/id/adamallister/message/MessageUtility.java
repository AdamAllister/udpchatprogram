/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.message;

import au.id.adamallister.lmt.LMTObject;
import au.id.adamallister.message.messages.Message;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 17-Sep-2016
 * Date Modified: 17-Sep-2016
 */
public class MessageUtility {

  private static final MessageEncoder encoder = new LMTEncoder();

  public static Message decode(byte[] msgBytes) {
   return encoder.decode(msgBytes);
  }


  public static byte[] encode(Message msg) {
    return encoder.encode(msg);
  }
}
