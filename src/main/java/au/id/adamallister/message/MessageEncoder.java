/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.Message;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 */
public interface MessageEncoder {
    public Message decode(byte[] msgBytes);
    public byte[] encode(Message msg);
}
