/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 16-Sep-2016
 * Date Modified: 18-Sep-2016
 */
public class RelayMessage extends Message{
  
  private String recipient;
  private Message message;
  
    public RelayMessage(UUIDWrapper id, String aRecipient, Message msgToRelay){
      super(MessageType.RLY, id);
      recipient = aRecipient;
      message = msgToRelay;
    }

    public String getRecipient() {
      return recipient;
    }
    
    public Message getRelayedMessage() {
      return message;
    }
}
