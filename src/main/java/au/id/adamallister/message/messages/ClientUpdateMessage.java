/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.connection.SessionEndpoint;
import au.id.adamallister.util.UUIDWrapper;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Messages Package - ClientUpdateMessage - A message containing a clients online
 * status, endpoints and username.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  02-MAY-2016 
 * Date Modified: 05-JUL-2016
 */
public class ClientUpdateMessage extends Message {

  String username;
  ArrayList<SessionEndpoint> clientEndpoints;
  boolean online;
  
  /**
   * Constructor - Takes an array list of session endpoints.
   * @param id The UUID of the message.
   * @param endpoints The possible endpoints of the client.
   * @param aStatus The online status of the client.
   * @param aUsername The username of the client.
   */
  public ClientUpdateMessage(UUIDWrapper id, ArrayList<SessionEndpoint> endpoints, boolean aStatus, String aUsername) {
    super(MessageType.UPD, id);
    online = aStatus;
    username = aUsername;
    clientEndpoints = endpoints;
    if(clientEndpoints!= null && endpoints.isEmpty()){
      clientEndpoints = null;
    }
  }
  
  /**
   * Constructor - Takes an array of string addresses and an array of Integer 
   * ports and tries to convert them into session endpoints.
   * The addresses array and port array must be the same size.
   * @param id The UUID of the message.
   * @param addresses A string array of client addresses.
   * @param ports An integer array of client ports.
   * @param aStatus The online status of the client.
   * @param aUsername The username of the client.
   */
  public ClientUpdateMessage(UUIDWrapper id, String[] addresses, int[] ports, boolean aStatus, String aUsername) {
    super(MessageType.UPD, id);
    online = aStatus;
    username = aUsername;
    clientEndpoints = new ArrayList<>();
    if((addresses != null && ports != null) && addresses.length == ports.length && addresses.length != 0){
      for(int i = 0; i < addresses.length; i++) {
        SessionEndpoint endpoint;
        try {
          endpoint = new SessionEndpoint(InetAddress.getByName(addresses[i]),ports[i]);
        } catch (UnknownHostException ex) {
          System.err.println("Client Update Message: Client Address not valid");
          endpoint = null;
        }
        if(endpoint != null) {
          clientEndpoints.add(endpoint);
        }
      }
    } else {
      clientEndpoints = null;
    }
  }
  
  /**
   * Gets the online status of the client
   * @return 
   */
  public boolean isOnline(){
    return online;
  }
  
  /**
   * Gets the client endpoints as an array list.
   * @return 
   */
  public ArrayList<SessionEndpoint> getEndpoints(){
    return clientEndpoints;
  }
  
  /**
   * Gets the clients username.
   * @return 
   */
  public String getName(){
    return username;
  }
}
