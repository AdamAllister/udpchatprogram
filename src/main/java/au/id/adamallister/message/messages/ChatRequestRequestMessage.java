/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - ChatRequestMessage - An object representing a request for
 * a chat with another client.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  05-JUL-2016
 * Date Modified: 05-JUL-2016
 */
public class ChatRequestRequestMessage extends ChatRequestMessage {
  
  /**
   * Constructor
   * @param id The UUID of the message.
   * @param requester The client requesting the chat.
   * @param responder The client responding to the request.
   * @param aChatID The UUID of the chat.
   */
  public ChatRequestRequestMessage(UUIDWrapper id, String requester, String responder, UUIDWrapper aChatID) {
    super(MessageType.RQST, id, requester, responder, aChatID);
  }
}
