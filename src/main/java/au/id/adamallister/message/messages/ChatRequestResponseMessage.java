/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - ChatRequestResponseMessage - A response to a chat request.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  09-MAY-2016 
 * Date Modified: 05-JUL-2016
 */
public class ChatRequestResponseMessage extends ChatRequestMessage {
  private boolean response;

  /**
   * Constructor - Sets super class as a response and sets a response.
   * @param id The UUID of the message.
   * @param requester The client requesting the chat.
   * @param responder The client responding to the request.
   * @param accept Was the request accepted.
   * @param aChatID The UUID of the chat.
   */
  public ChatRequestResponseMessage(UUIDWrapper id, String requester, String responder, boolean accept, UUIDWrapper aChatID) {
    super(MessageType.RSPN, id, requester, responder, aChatID);
    response = accept;
  }

  /**
   * Gets the request response.
   * @return true if request was accepted.
   */
  public boolean getResponse(){
    return response;
  }

}
