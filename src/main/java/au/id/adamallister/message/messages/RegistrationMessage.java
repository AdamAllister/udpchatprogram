/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - RegistrationMessage - An abstract class representing a
 * registration message.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  03-JUN-2016 
 * Date Modified: 05-JUL-2016
 */
public abstract class RegistrationMessage extends Message {

  private String username;

  /**
   * Constructor.
   * @param type The type of Registration message.
   * @param id The UUID of the message.
   * @param aUsername The username of the client, or an empty string if not set.
   * @throws IllegalArgumentException If type is not a Registration type.
   */
  protected RegistrationMessage(MessageType type, UUIDWrapper id, String aUsername) {
    super(type, id);
    if(type.equals(MessageType.CREG) || type.equals(MessageType.SREG)) {
      username = aUsername;
    } else 
      throw new IllegalArgumentException("Tried to make registration message for non registration message");
  }

  /**
   * Gets the username of the client.
   * @return
   */
  public String getUsername() {
    return username;
  }
}
