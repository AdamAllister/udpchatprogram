/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - HandShakeMessage - A message used for establishing an
 * initial connection.
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  16-MAY-2016 
 * Date Modified: 05-JUL-2016
 */
public class HandShakeMessage extends Message {

  private String username;

  /**
   * Constructor.
   * @param id The UUID of the message.
   * @param aUsername The username of the sender.
   */
  public HandShakeMessage(UUIDWrapper id, String aUsername) {
    super(MessageType.HAND, id);
    username = aUsername;
  }

  /**
   * Gets the username of the sender.
   * @return
   */
  public String getUsername() {
    return username;
  }
}
