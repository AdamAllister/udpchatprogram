/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.connection.SessionEndpoint;
import au.id.adamallister.util.UUIDWrapper;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Messages Package - ClientRegistrationMessage - A message for registering
 * a client to the server.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  09-MAY-2016
 * Date Modified: 05-JUL-2016
 */
public class ClientRegistrationMessage extends RegistrationMessage{

  ArrayList<SessionEndpoint> endpoints;
  
  private ClientRegistrationMessage(UUIDWrapper id, String username) {
    super(MessageType.CREG, id, username);
  }
  
  /**
   * Constructor - Takes an array of addresses in string representation, and tries
   * to convert them to InetAddresses.
   * @param id The UUID of the message.
   * @param addresses The addresses the client can be reached on.
   * @param ports The ports for each address.
   * @param alias The username the client wants to use.
   */
  public ClientRegistrationMessage (UUIDWrapper id, String[] addresses, int[] ports, String alias) {
    this(id, alias);
    endpoints = new ArrayList<>();
    if(addresses != null & ports != null) {
      if(addresses.length == ports.length && addresses.length != 0){
        for(int i = 0; i < addresses.length; i++) {
          SessionEndpoint endpoint = null;
          try {
            endpoint = new SessionEndpoint(InetAddress.getByName(addresses[i]),ports[i]);
          } catch (UnknownHostException ex) {
            System.out.println("Client Update Message: Client Address not valid");
          }
          if(endpoint != null) {
            endpoints.add(endpoint);
          }
        }
      }
    }
    if(endpoints.isEmpty()) {
      endpoints = null;
    }
  }
  
  /**
   * Constructor - Takes an array of addresses as InetAddress.
   * @param id The UUID of the message.
   * @param addresses The addresses the client can be reached on.
   * @param ports The ports for each address.
   * @param alias The username the client wants to use.
   */
  public ClientRegistrationMessage (UUIDWrapper id, InetAddress[] addresses, int[] ports, String alias) {
    this(id, alias);
    endpoints = new ArrayList<>();
    if(addresses != null & ports != null) {
      if(addresses.length == ports.length && addresses.length != 0){
        for(int i = 0; i < addresses.length; i++) {
          SessionEndpoint endpoint = new SessionEndpoint(addresses[i],ports[i]);;
        }
      }
    }
    if(endpoints.isEmpty()) {
      endpoints = null;
    }
  }
  
  /**
   * Gets the list of Session Endpoints the client can be reached at.
   * @return
   */
  public ArrayList<SessionEndpoint> getEndpoints(){
    return endpoints;
  }
}
