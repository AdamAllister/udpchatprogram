/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - ServerRegistrationMessage - A registration message from 
 * the server, giving a response code and message.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  02-JUN-2016
 * Date Modified: 02-JUL-2016
 */
public class ServerRegistrationMessage extends RegistrationMessage {

  private int code;
  private String msg;

  /**
   * Constructor.
   * @param uid The UUID of the message.
   * @param responseCode A number representing a response.
   * @param responseMsg A message indicating the reason for the response.
   * @param username The username confirmed or an empty string.
   */
  public ServerRegistrationMessage(UUIDWrapper uid, int responseCode, String responseMsg, String username) {
    super(MessageType.SREG, uid, username);
    code = responseCode;
    msg = responseMsg;
  }

  /**
   * Gets the response code.
   * @return
   */
  public int getCode() {
    return code;
  }

  /**
   * Gets the response message.
   * @return
   */
  public String getMsg() {
    return msg;
  }
}
