/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - HealthCheckMessage - Represents a health check to a client.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  22-APR-2016
 * Date Modified: 07-JUL-2016
 */
public class HealthCheckMessage extends Message {
  
  /**
   * Constructor.
   * @param id The UUID of the message.
   */
  public HealthCheckMessage(UUIDWrapper id) {
    super(MessageType.HCHK, id);
  }
  
}
