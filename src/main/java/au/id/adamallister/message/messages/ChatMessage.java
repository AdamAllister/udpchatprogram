/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Message - Message A container containing read only text message details.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  14-APR-2016
 * Date Modified: 05-JUL-2016
 */
public class ChatMessage extends Message {

  private final long timeSent;
  private final String fromName;
  private final String message;
  private final UUIDWrapper chatID;
  private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");

  /**
   * Constructor - Used to reform messages that already exist, or create message
   * with specific UUIDs and times.
   * @param id The UUID of the message.
   * @param aFromName The username of the sender
   * @param aChatID The UUID of the chat the message belongs to.
   * @param aTimeSent The time the message was sent.
   * @param aMessage The string message.
   */
  public ChatMessage(UUIDWrapper id, String aFromName, UUIDWrapper aChatID, long aTimeSent, String aMessage) {
    super(MessageType.MSG, id);
    timeSent = aTimeSent;
    fromName = aFromName;
    message = aMessage;
    chatID = aChatID;
  }
  
  /**
   * Constructor - Used to create new messages with new UUIDs and with the current
   * time.
   * @param aFromName The username of the sender 
   * @param aChatID The UUID of the chat the message belongs to.
   * @param aMessage The string message.
   */
  public ChatMessage(String aFromName, UUIDWrapper aChatID, String aMessage) {
    super(MessageType.MSG, new UUIDWrapper(UUID.randomUUID()));
    Date time = new Date();
    timeSent = time.getTime();
    fromName = aFromName;
    message = aMessage;
    chatID = aChatID;
  }

  /**
   * @return The UUID of the chat the message belongs to.
   */
  public UUIDWrapper getChatID(){
    return chatID;
  }
  
  /**
   * @return The string message.
   */
  public String getMessage() {
    return message;
  }

  /**
   * @return The name of the sender.
   */
  public String getFromName() {
    return fromName;
  }

  /**
   * @return The time the message was sent.
   */
  public long getTimeMillis() {
    return timeSent;
  }

  /**
   * Returns a string representation of the time sent.
   * @return The time it was sent as a string representation.
   */
  public String getTimeString() {
    Date time = new Date();
    time.setTime(timeSent);
    String dateString = TIME_FORMAT.format(time);
    return dateString;
  }

  @Override
  public String toString() {
    String asString = getTimeString() + " " + fromName + ": " + message;
    return asString;
  }
}
