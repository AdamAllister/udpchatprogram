/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;
import java.util.Objects;

/**
 * Messages Package - Message - An abstract message object that takes a type and
 * an UUID.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  22-APR-2016 
 * Date Modified: 05-JUL-2016
 */
public abstract class Message {

  private MessageType type;
  private UUIDWrapper msgID;

  /**
   * Constructor.
   * @param aType The type of message.
   * @param aMsgID The UUID of the message.
   */
  public Message(MessageType aType, UUIDWrapper aMsgID) {
    type = aType;
    msgID = aMsgID;
  }

  /**
   * Gets the type of message this message is.
   * @return
   */
  public MessageType getType() {
    return type;
  }

  /**
   * Gets the UUID of the message.
   * @return
   */
  public UUIDWrapper getMessageUID() {
    return msgID;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 19 * hash + Objects.hashCode(this.type);
    hash = 19 * hash + Objects.hashCode(this.msgID);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Message other = (Message) obj;
    if (this.type != other.type) {
      return false;
    }
    if (!Objects.equals(this.msgID, other.msgID)) {
      return false;
    }
    return true;
  }
}
