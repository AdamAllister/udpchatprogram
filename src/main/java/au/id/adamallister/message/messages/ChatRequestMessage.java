/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message.messages;

import au.id.adamallister.message.MessageType;
import au.id.adamallister.util.UUIDWrapper;

/**
 * Messages Package - ChatRequestMessage - An object representing a request or
 * response for a chat with another client.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  09-MAY-2016 
 * Date Modified: 05-JUL-2016
 */
public abstract class ChatRequestMessage extends Message {

  private String requesterUsername;
  private String responderUsername;
  private UUIDWrapper chatID;
  
  /**
   * Constructor
   * @param requestType The type of request the message is.
   * @param id The UUID of the message.
   * @param requester The client requesting the chat.
   * @param responder The client responding to the request.
   * @param aChatID The UUID of the chat.
   */
  protected ChatRequestMessage(MessageType requestType, UUIDWrapper id, String requester, String responder, UUIDWrapper aChatID){
    super(requestType, id);
    chatID = aChatID;
    responderUsername = responder;
    requesterUsername = requester;
  }
  
  /**
   * Set the client responding to the request.
   * @param responder
   */
  public void setResponder(String responder){
    responderUsername = responder;
  }
  
  /**
   * Get the client responding to the request.
   * @return
   */
  public String getResponderUsername(){
    return responderUsername;
  }

  /**
   * Get the client requesting the chat.
   * @return
   */
  public String getRequesterUsername(){
    return requesterUsername;
  }
  
  /**
   * Get the UUID of the chat.
   * @return
   */
  public UUIDWrapper getChatID(){
    return chatID;
  }

  
}
