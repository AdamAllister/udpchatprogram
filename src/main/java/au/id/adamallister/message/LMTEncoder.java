/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.lmt.LMTObject;
import au.id.adamallister.message.messages.*;
import au.id.adamallister.util.UUIDWrapper;
import java.util.UUID;
import org.json.JSONException;

/**
 * Message Package - LMTEncoder - A class used to convert messages to
 Lightweight Message Translation and vice versa.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  22-APR-2016
 * Date Modified: 06-JUL-2016
 */
public class LMTEncoder implements MessageEncoder{

  /**
   * Converts a LMT Object to a Message Object.
   * @param encodedBytes
   * @return 
   */
  public Message decode(byte[] encodedBytes) {
    String items = "";
    LMTObject lmt = new LMTObject(encodedBytes);
    
    Message msg = null;
    MessageType type;
    UUIDWrapper id;

    
    
    try {
      type = lmt.getEnum(MessageType.class, "type");
    } catch (JSONException e) {
      System.err.println("Error LMTMessageUtility: LMTDecode failed to find valid message type in LMTObject");
      return null;
    }

    if (!hasCorrectFields(type, lmt)) {
      System.err.println("fromJSONObject - JSON Object does not have required fields");
      return null;
    }

    try {
      id = new UUIDWrapper(new UUID(lmt.getLong("uid-1"), lmt.getLong("uid-2")));
    } catch (JSONException e) {
      System.err.println("fromJSONObject - JSON Object does not have valid ID");
      return null;
    }

    String[] addressList;
    int[] portList;
    switch (type) {
      case MSG:
        msg = new ChatMessage(id, lmt.getString("from"),
            new UUIDWrapper(new UUID(lmt.getLong("chat-uid-1"), lmt.getLong("chat-uid-2"))),
            lmt.getLong("time-utc"), lmt.getString("msg"));
        break;
      case HCHK:
        msg = new HealthCheckMessage(id);
        break;
      case UPD:
        addressList = lmt.getStringArray("addresses");
        portList = lmt.getIntArray("ports");
        msg = new ClientUpdateMessage(id, addressList, portList,
            lmt.getBoolean("connecting"), lmt.getString("username"));
        break;
      case CREG:
        addressList = lmt.getStringArray("addresses");
        portList = lmt.getIntArray("ports");
        msg = new ClientRegistrationMessage(id, addressList, portList,
            lmt.getString("username"));
        break;
      case SREG:
        msg = new ServerRegistrationMessage(id, lmt.getInt("code"), lmt.getString("msg"), lmt.getString("username"));
        break;
      case RQST:
        msg = new ChatRequestRequestMessage(id,
            lmt.getString("responder"),
            lmt.getString("requester"),
            new UUIDWrapper(new UUID(lmt.getLong("chat-uid-1"), lmt.getLong("chat-uid-2"))));
        break;
      case RSPN:
        msg = new ChatRequestResponseMessage(id,
            lmt.getString("responder"),
            lmt.getString("requester"),
            lmt.getBoolean("response"),
            new UUIDWrapper(new UUID(lmt.getLong("chat-uid-1"), lmt.getLong("chat-uid-2"))));
        break;
      case HAND:
        msg = new HandShakeMessage(id, lmt.getString("username"));
        break;
      case RLY:
        msg = new RelayMessage(id, lmt.getString("recipient"), MessageUtility.decode(lmt.getByteArray("message")));
        break;
      default:
        throw new UnsupportedOperationException("Message Type not supported");
    }
    return msg;
  }

  /**
   * Converts a Message Object to a JSON Object.
   * Creates a JSON Object based on what kind of message it is.
   * @param msg The message object to convert.
   * @return The JSON Object if Message type supported, or null if not.
   */
  public byte[] encode(Message msg) {
    if(msg == null)
      return null;
    
    MessageType type = msg.getType();
    LMTObject lmt = new LMTObject();
    UUIDWrapper id = msg.getMessageUID();
    lmt.putEnum("type", type);
    lmt.putLong("uid-1", id.getMostSignificantBits());
    lmt.putLong("uid-2", id.getLeastSignificantBits());
    
    switch (type) {
      case MSG:
        ChatMessage chat = (ChatMessage)msg;
        lmt.putString("from"     , chat.getFromName());
        lmt.putLong("chat-uid-1", chat.getChatID().getMostSignificantBits());
        lmt.putLong("chat-uid-2", chat.getChatID().getLeastSignificantBits());
        lmt.putLong("time-utc" , chat.getTimeMillis());
        lmt.putString("msg"      , chat.getMessage());
        break;
      case HCHK:
        HealthCheckMessage health = (HealthCheckMessage)msg;
        break;
      case UPD:
        ClientUpdateMessage upd = (ClientUpdateMessage)msg;
        String[] updIps = new String[upd.getEndpoints().size()];
        int[] updPorts = new int[upd.getEndpoints().size()];
        for(int i = 0; i < upd.getEndpoints().size(); i++) {
          updIps[i] = upd.getEndpoints().get(i).ip().getHostAddress();
          updPorts[i] = upd.getEndpoints().get(i).port();
        }
        lmt.putArray("addresses"  , updIps);
        lmt.putArray("ports"      , updPorts);
        lmt.putBoolean("connecting" , upd.isOnline());
        lmt.putString("username"   , upd.getName());
        break;
      case CREG:
        ClientRegistrationMessage cReg = (ClientRegistrationMessage)msg;
        String[] regIps = new String[cReg.getEndpoints().size()];
        int[] regPorts = new int[cReg.getEndpoints().size()];
        for(int i = 0; i < cReg.getEndpoints().size(); i++) {
          regIps[i] = cReg.getEndpoints().get(i).ip().getHostAddress();
          regPorts[i] = cReg.getEndpoints().get(i).port();
        }
        lmt.putArray("addresses", regIps);
        lmt.putArray("ports"    , regPorts);
        lmt.putString("username" , cReg.getUsername());
        break;
      case SREG:
        ServerRegistrationMessage sReg = (ServerRegistrationMessage)msg;
        lmt.putInt("code", sReg.getCode());
        lmt.putString("msg" , sReg.getMsg());
        lmt.putString("username" , sReg.getUsername());
        break;  
      case RQST:
        ChatRequestRequestMessage rqst = (ChatRequestRequestMessage)msg;
        lmt.putString("requester", rqst.getRequesterUsername());
        lmt.putString("responder", rqst.getResponderUsername());
        lmt.putLong("chat-uid-1", rqst.getChatID().getMostSignificantBits());
        lmt.putLong("chat-uid-2", rqst.getChatID().getLeastSignificantBits());
        break;
      case RSPN:
        ChatRequestResponseMessage rspn = (ChatRequestResponseMessage)msg;
        lmt.putString("requester", rspn.getRequesterUsername());
        lmt.putString("responder", rspn.getResponderUsername());
        lmt.putBoolean("response" , rspn.getResponse());
        lmt.putLong("chat-uid-1", rspn.getChatID().getMostSignificantBits());
        lmt.putLong("chat-uid-2", rspn.getChatID().getLeastSignificantBits());
        break;
      case HAND:
        HandShakeMessage hand = (HandShakeMessage)msg;
        lmt.putString("username" , hand.getUsername());
        break;
      case RLY:
        RelayMessage relay = (RelayMessage)msg;
        lmt.putString("recipient" , relay.getRecipient());
        lmt.putArray("message" , MessageUtility.encode(relay.getRelayedMessage()));
        break;
      default:
        lmt = null;
        throw new UnsupportedOperationException("Message Type not supported");
    }
    return lmt == null ? null : lmt.getBytes();
  }
  
  private static boolean hasCorrectFields(MessageType type, LMTObject lmt) {
    boolean correct = lmt.has("uid-1") && lmt.has("uid-2");
    switch (type) {
      case MSG:
        correct = correct && lmt.has("from") && lmt.has("chat-uid-1") && 
            lmt.has("chat-uid-2") && lmt.has("time-utc") && lmt.has("msg");
        break;
      case HCHK:
        break;
      case UPD:
        correct = correct && lmt.has("addresses") && lmt.has("ports")
            && lmt.has("connecting") && lmt.has("username");
        break;
      case CREG:
        correct = correct && lmt.has("addresses") && lmt.has("ports")
            && lmt.has("username");
        break;
      case SREG:
        correct = correct && lmt.has("code") && lmt.has("msg") 
            && lmt.has("username");
        break;
      case RQST:
        correct = correct && lmt.has("responder") && lmt.has("requester")
            && lmt.has("chat-uid-1") && lmt.has("chat-uid-2");
        break;
      case RSPN:
        correct = correct && lmt.has("responder") && lmt.has("requester")
            && lmt.has("chat-uid-1") && lmt.has("chat-uid-2") && lmt.has("response");
        break;
      case HAND:
        correct = correct && lmt.has("username");
        break;
      case RLY:
        correct = correct && lmt.has("recipient") && lmt.has("message");
        break;
      default:
        throw new UnsupportedOperationException("Message Type not supported");
    }
    return correct;
  }
}
