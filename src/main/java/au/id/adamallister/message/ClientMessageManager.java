/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.message;

import au.id.adamallister.message.messages.*;
import au.id.adamallister.packet.PacketManager;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import org.json.JSONObject;

/**
 * Message Package - ClientMessageManager - Manages the sending, receiving and 
 * converting of packets to messages and vice versa for clients.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  11-MAY-2016 
 * Date Modified: 06-JUL-2016
 */
public class ClientMessageManager extends MessageManager {

  private ArrayList<ClientMessageSubscriber> clientMsgSubscribers;

  /**
   * Constructor
   * @param pm The PacketManager that will manage sending and receiving of
   * packets from and to the client message manager.
   */
  public ClientMessageManager(PacketManager pm) {
    super(pm);
    clientMsgSubscribers = new ArrayList<>();
  }
  
  /**
   * Adds an observer to a list of observers if not already added.
   * @param sub The observer to add.
   */
  public void subscribe(ClientMessageSubscriber sub) {
    if(!clientMsgSubscribers.contains(sub))
      clientMsgSubscribers.add(sub);
  }
  
  /**
   * Takes a JSONObject, tries to convert it to a message and then notifies
   * observers.
   * @param json A JSON object containing message data.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  @Override
  protected void processMessage(byte[] msgBytes, InetAddress address, int port) {
    Message msg = MessageUtility.decode(msgBytes);
    switch (msg.getType()) {
      case MSG:
        notifyChatMessage((ChatMessage) msg, address, port);
        break;
      case HAND:
        notifyHandShake((HandShakeMessage)msg,address, port);
        break;
      case HCHK:
        notifyHealthCheck((HealthCheckMessage) msg, address, port);
        break;
    }
  }
  
  private void notifyChatMessage(ChatMessage chatMsg, InetAddress address, int port) {
    for (ClientMessageSubscriber sub : clientMsgSubscribers) {
      sub.clientMessage(chatMsg, address, port);
    }
  }

  private void notifyHealthCheck(HealthCheckMessage healthCheck, InetAddress address, int port) {
    for (ClientMessageSubscriber sub : clientMsgSubscribers) {
      sub.clientHealthCheck(healthCheck, address, port);
    }
  }
  
  private void notifyHandShake(HandShakeMessage hndMsg, InetAddress address, int port) {
    for (ClientMessageSubscriber sub : clientMsgSubscribers) {
      sub.clientHandShake(hndMsg, address, port);
    }
  }
}
