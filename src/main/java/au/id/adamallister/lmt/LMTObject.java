/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.lmt;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 16-Sep-2016
 * Date Modified: 16-Sep-2016
 */
public class LMTObject {

  private static final Logger LOGGER = Logger.getLogger(LMTObject.class.getName());
  private static Charset DEFAULT_CHARSET = java.nio.charset.StandardCharsets.UTF_8;
  
  static{
    LOGGER.addHandler(new ConsoleHandler());
  }
  
  public static enum LENGTH_MODE { 
    INT((byte)0x4), SHORT((byte)0x2);
    private final byte readMode;
    LENGTH_MODE(byte readMode) { this.readMode = readMode; }
    public byte getValue() { return readMode; }
  };
  
  private HashMap<String, byte[]> items;
  private Charset charset;
  private LENGTH_MODE mode;
  
  public LMTObject(Charset aCharset, LENGTH_MODE aMode){
    charset = aCharset;
    items = new HashMap<>();
    mode = aMode;
  }
  
  public LMTObject(LENGTH_MODE aMode){
    this(DEFAULT_CHARSET, aMode);
  }
  
  public LMTObject(Charset aCharset){
    this(aCharset, LENGTH_MODE.INT);
  } 
  
  public LMTObject(){
    this(DEFAULT_CHARSET, LENGTH_MODE.INT);
  }
  
  public LMTObject(byte[] source){
    this(null, LENGTH_MODE.INT);
    DataInputStream stream = new DataInputStream(new ByteArrayInputStream(source));
    try{
      byte lengthMode = stream.readByte();
      setMode(lengthMode);
      byte[] charsetNameBytes = new byte[readLengthOfItem(stream, mode).intValue()];
      stream.read(charsetNameBytes);
      String charsetName = new String(charsetNameBytes, DEFAULT_CHARSET);
      charset = Charset.forName(charsetName);
      int itemListLength = readLengthOfItem(stream, mode).intValue();
      byte[] itemListBytes = new byte[itemListLength];
      stream.read(itemListBytes);
      String itemList = new String(itemListBytes, charset);
      String[] itemNames = itemList.split(",");
      for (String itemName : itemNames) {
        int dataLength = readLengthOfItem(stream, mode).intValue();
        byte[] data = new byte[dataLength];
        stream.read(data);
        items.put(itemName, data);
      }
    } catch(IOException e){
      items = new HashMap<>();
      LOGGER.log(Level.WARNING, "Could not be constructed from source bytes, creating empty",e);
    }
  }
  
  private void setMode(byte lengthMode) {
    if(lengthMode == LMTObject.LENGTH_MODE.INT.getValue()) mode= LENGTH_MODE.INT;
    else if(lengthMode == LMTObject.LENGTH_MODE.SHORT.getValue()) mode= LENGTH_MODE.SHORT;
  }
  
  private static Number readLengthOfItem(DataInputStream stream, LENGTH_MODE mode) throws IOException{
    if(mode == LMTObject.LENGTH_MODE.INT) return stream.readInt();
    if(mode == LMTObject.LENGTH_MODE.SHORT) return stream.readShort();
    return 0;
  }
  
  private static Number readLengthOfItem(ByteBuffer stream, LENGTH_MODE mode){
    if(mode == LMTObject.LENGTH_MODE.INT) return stream.getInt();
    if(mode == LMTObject.LENGTH_MODE.SHORT) return stream.getShort();
    return 0;
  }
  
  public boolean has(String key) {
    return items.containsKey(key);
  }
  
  public String getString(String key) {
    return getString(key,charset);
  }
  
  private String getString(String key, Charset characterSet) {
    CharsetDecoder cd = characterSet.newDecoder();
    cd.onMalformedInput(CodingErrorAction.REPORT);
    cd.onUnmappableCharacter(CodingErrorAction.REPORT);
    try {
      CharBuffer characters = cd.decode(ByteBuffer.wrap(items.get(key)));
      String string = new String(characters.array());
      return string;
    } catch (Exception e) {
      LOGGER.log(Level.WARNING, "Could not decode " + key + " object as " + characterSet.displayName() + " encoded String", e);
    }
    return null;
  }
  
  public int getInt(String key){
    byte[] bytes = items.get(key);
    if(bytes != null && bytes.length == 4) {
      ByteBuffer wrapped = ByteBuffer.wrap(bytes);
      return wrapped.getInt();
    }
    return 0;
  }

  public short getShort(String key){
    if(items.get(key) != null && items.get(key).length == 2){
      ByteBuffer wrapped = ByteBuffer.wrap(items.get(key));
      return wrapped.getShort();
    }
    return 0;
  }
 
  public float getFloat(String key){
    if(items.get(key) != null && items.get(key).length == 4){
      ByteBuffer wrapped = ByteBuffer.wrap(items.get(key));
      return wrapped.getFloat();
    }
    return 0;
  }
  
  public double getDouble(String key){
    if(items.get(key) != null && items.get(key).length == 8){
      ByteBuffer wrapped = ByteBuffer.wrap(items.get(key));
      return wrapped.getDouble();
    }
    return 0.0;
  }
  
  public long getLong(String key){
    if(items.get(key) != null && items.get(key).length == 8){
      ByteBuffer wrapped = ByteBuffer.wrap(items.get(key));
      return wrapped.getLong();
    }
    return 0;
  }
  
  public byte getByte(String key){
    if(items.get(key) != null && items.get(key).length == 1){
      ByteBuffer wrapped = ByteBuffer.wrap(items.get(key));
      return wrapped.get();
    }
    return 0;
  }
  
  public boolean getBoolean(String key){
    return items.get(key) != null && items.get(key).length == 1 && items.get(key)[0] == (byte)1;
  }  
  
  public <E extends Enum<E>> E getEnum(Class<E> clazz, String key, E defaultValue) {
    String enumString = getString(key);
    if(enumString != null)  {
      try {
        return E.valueOf(clazz, enumString);
      } catch (IllegalArgumentException | NullPointerException e) {
        LOGGER.log(Level.WARNING, "Could not find enum of type " + 
            clazz.getCanonicalName() + " for key: " + key, e); 
      }
    }
    return defaultValue;
  }
  
  public <E extends Enum<E>> E getEnum(Class<E> clazz, String key) {
    return  getEnum(clazz, key, null);
  }
  
  public int[] getIntArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue() && (item.length - mode.getValue()) % Integer.BYTES == 0) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      int[] elements = new int[nElements];
      LOGGER.log(Level.INFO, "Get created int array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        elements[i] = buffer.getInt();
      }
      return elements;
    }
    return null;
  }
  
  public short[] getShortArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue() && (item.length - mode.getValue()) % Short.BYTES == 0) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      short[] elements = new short[nElements];
      LOGGER.log(Level.INFO, "Get created short array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        elements[i] = buffer.getShort();
      }
      return elements;
    }
    return null;
  }
  
  public float[] getFloatArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue() && (item.length - mode.getValue()) % Float.BYTES == 0) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      float[] elements = new float[nElements];
      LOGGER.log(Level.INFO, "Get created float array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
          elements[i] = buffer.getFloat();
      }
      return elements;
    }
    return null;
  }
  
  public long[] getLongArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue() && (item.length - mode.getValue()) % Long.BYTES == 0) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      long[] elements = new long[nElements];
      LOGGER.log(Level.INFO, "Get created long array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        elements[i] = buffer.getLong();
      }
      return elements;
    }
    return null;
  }
  
  public double[] getDoubleArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue() && (item.length - mode.getValue()) % Double.BYTES == 0) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      double[] elements = new double[nElements];
      LOGGER.log(Level.INFO, "Get created double array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        elements[i] = buffer.getDouble();
      }
      return elements;
    }
    return null;
  }
   
  public boolean[] getBooleanArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue()) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      boolean[] elements = new boolean[nElements];
      LOGGER.log(Level.INFO, "Get created boolean array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        elements[i] = buffer.get() == 1;
      }
      return elements;
    }
    return null;
  }  
  
  public byte[] getByteArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue()) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      byte[] elements = new byte[nElements];
      LOGGER.log(Level.INFO, "Get created byte array with {0} elements for {1}", new Object[]{nElements, key});
      buffer.get(elements);
      return elements;
    }
    return null;
  }    
  
  public byte[][] getByteArrayArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue()) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      byte[][] elements = new byte[nElements][];
      LOGGER.log(Level.INFO, "Get created byte array array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        int elementSize = (int)readLengthOfItem(buffer, mode);
        byte[] data = new byte[elementSize];
        buffer.get(data);
        elements[i] = data;
      }
      return elements;
    }
    return null;
  }   
  
  public String[] getStringArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue()) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      String[] elements = new String[nElements];
      LOGGER.log(Level.INFO, "Get created String array array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        int elementSize = (int)readLengthOfItem(buffer, mode);
        byte[] data = new byte[elementSize];
        buffer.get(data);
        elements[i] = new String(data, charset);
      }
      return elements;
    }
    return null;
  }   
  
  public LMTObject[] getLMTObjectArray(String key) {
    byte[] item = items.get(key);
    ByteBuffer buffer; 
    if(item != null && item.length > mode.getValue()) {
      buffer = ByteBuffer.wrap(item);
      int nElements = (int)readLengthOfItem(buffer, mode);
      LMTObject[] elements = new LMTObject[nElements];
      LOGGER.log(Level.INFO, "Get created LMTObject array array with {0} elements for {1}", new Object[]{nElements, key});
      for(int i = 0; i < nElements; i++){
        int elementSize = (int)readLengthOfItem(buffer, mode);
        byte[] data = new byte[elementSize];
        buffer.get(data);
        elements[i] = new LMTObject(data);
      }
      return elements;
    }
    return null;
  } 
  
  public LMTObject getLMTObject(String key) {
    byte[] item = items.get(key);
    if(has(key))
      return new LMTObject(item);
    return null;
  }   
  
  public void putInt(String key, int value){
    items.put(key, ByteBuffer.allocate(4).putInt(value).array());
  }
  
  public void putShort(String key, short value){
    items.put(key, ByteBuffer.allocate(2).putShort(value).array());
  }
  
  public void putFloat(String key, float value){
    items.put(key, ByteBuffer.allocate(4).putFloat(value).array());
  }
  
  public void putDouble(String key, double value){
    items.put(key, ByteBuffer.allocate(8).putDouble(value).array());
  }
  
  public void putLong(String key, long value){
    items.put(key, ByteBuffer.allocate(8).putLong(value).array());
  }
  
  public void putBoolean(String key, boolean value){
    putByte(key, (byte)(value ? 1 : 0));
  }
  
  public void putByte(String key, byte value){
    items.put(key, ByteBuffer.allocate(1).put(value).array());
  }
  
  public void putLMTObject(String key, LMTObject value){
    items.put(key, value.getBytes());
  }
  
  public void putArray(String key, String[] values){
    int length = (int)mode.getValue();
    byte[][] data = new byte[values.length][];
    for(int i = 0; i < values.length; i++) {
      data[i] = values[i].getBytes(charset);
      length += (int)mode.getValue();
      length += data[i].length;
    }
    byte[] outputBytes = new byte[length];
    ByteBuffer buf = ByteBuffer.wrap(outputBytes);
    buf.putInt(data.length);
    for(byte[] bytes : data) {
      buf.putInt(bytes.length);
      buf.put(bytes);
    }
    LOGGER.log(Level.INFO, "Created array {0} of size {1} for {2}", new Object[]{Arrays.toString(outputBytes), outputBytes.length, key});
    items.put(key, buf.array());
  }
  
  public void putArray(String key, LMTObject[] values){
    int length = (int)mode.getValue();
    byte[][] data = new byte[values.length][];
    for(int i = 0; i < values.length; i++) {
      data[i] = values[i].getBytes( );
      length += (int)mode.getValue();
      length += data[i].length;
    }
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(data.length);
    for(byte[] bytes : data) {
      buf.putInt(bytes.length);
      buf.put(bytes);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, byte[][] values){
    int length = (int)mode.getValue();
    for (byte[] value : values) {
      length += (int)mode.getValue();
      length += value.length;
    }
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(byte[] bytes : values) {
      buf.putInt(bytes.length);
      buf.put(bytes);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, int[] values){
    int length = (int)mode.getValue();
    length += values.length * 4;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(int value : values) {
      buf.putInt(value);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, byte[] values){
    int length = (int)mode.getValue();
    length += values.length;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(byte value : values) {
      buf.put(value);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, boolean[] values){
    int length = (int)mode.getValue();
    length += values.length;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(boolean value : values) {
      buf.put(value ? (byte)1 : (byte)0);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, short[] values){
    int length = (int)mode.getValue();
    length += values.length * 2;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(short value : values) {
      buf.putShort(value);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, float[] values){
    int length = (int)mode.getValue();
    length += values.length * 4;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(float value : values) {
      buf.putFloat(value);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, long[] values){
    int length = (int)mode.getValue();
    length += values.length * 8;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(long value : values) {
      buf.putLong(value);
    }
    items.put(key, buf.array());
  }
  
  public void putArray(String key, double[] values){
    int length = (int)mode.getValue();
    length += values.length * 8;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.putInt(values.length);
    for(double value : values) {
      buf.putDouble(value);
    }
    items.put(key, buf.array());
  }
  
  public <E extends Enum<E>> void putEnum(String key, E enumValue){
    items.put(key, enumValue.name().getBytes(charset));
  }
  
  public void putString(String key, String value){
    items.put(key, value.getBytes(charset));
  }
  
  public byte[] getBytes(){
    String keys = "";
    for(String key : items.keySet()) {
      keys += key + ",";
    }
    keys = keys.substring(0, keys.length() - 1);
    byte[] keyBytes = keys.getBytes(charset);
    byte[] charsetBytes = charset.name().getBytes(charset);
    byte[] headerBytes = ByteBuffer.allocate(1 + mode.getValue() + charsetBytes.length + mode.getValue() + keyBytes.length).put(mode.getValue()).putInt(charsetBytes.length).put(charsetBytes).putInt(keyBytes.length).put(keyBytes).array();
    
    
    int length = items.values().size() * (int)mode.getValue();
    for(byte[] data : items.values()) {
      length += data.length;
    }
    ByteBuffer buffer = ByteBuffer.allocate(headerBytes.length + length);
    buffer.put(headerBytes);
    for(byte[] data : items.values()) {
      writeItem(buffer, mode, data);
    }
    return buffer.array();
  }
  
  private static void writeItem(ByteBuffer buffer, LENGTH_MODE mode, byte[] data) {
    if(mode == LMTObject.LENGTH_MODE.INT) buffer.putInt(data.length);
    if(mode == LMTObject.LENGTH_MODE.SHORT) buffer.putShort((short)data.length);
    buffer.put(data);
  }
}
