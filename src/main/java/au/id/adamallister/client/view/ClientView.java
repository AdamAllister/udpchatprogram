/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import java.util.ArrayList;

/**
 * View Package - ClientView Interface - An interface for connecting the view
 * and the controller.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016
 * Date Modified: 08-JUL-2016
 */
public interface ClientView {
  
  /**
   * Gives the view a new chat message.
   * @param formattedMessage The message.
   * @param chatId The UUID of the chat the message belongs to.
   */
  public void chatUpdate(String formattedMessage, String chatId);
  
  ////TODO 1: Replace clients online with hashmap<String, Boolean> of online offline
  /**
   * Gives the view a list of clients currently online.
   * @param clientsOnline A list of clients.
   */
    public void clientsUpdate(ArrayList<String> clientsOnline);

  /**
   * Gives the view a error or message to do with the server connection.
   * @param connected Is the client still connected.
   * @param error The error or information.
   */
  public void serverUpdate(boolean connected, String error);

  /**
   * Tells the view to create a new Tab for the given chat
   * @param chatName The name of the chat tab.
   * @param chatId The UUID of the chat.
   */
  public void createChatView(String chatName, String chatId);

  /**
   * Gives the view a message containing the state of their registration to the
   * server.
   * @param error The message.
   */
  public void registrationStatus(String error);
    
}
