/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import au.id.adamallister.client.ClientController;
import au.id.adamallister.connection.ClientManager;
import au.id.adamallister.health.HealthCheckManager;
import au.id.adamallister.health.HealthCheckResendTimer;
import au.id.adamallister.util.UUIDWrapper;
import au.id.adamallister.util.timer.Timer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.UUID;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.web.WebView;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

/**
 * View Package - ChatApplication - A window that allows interaction with the
 * controller.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  05-JUN-2016 
 * Date Modified: 08-JUL-2016
 */
public class ChatApplication extends Application implements ClientView {

  
  private HashMap<String, Tab> chats;
  private TabPane tabs;
  private ChatMessageEditor chatInput;
  private ListView clients;
  private Scene scene;
  private ClientController controller;
  private static final String DEFAULT_CHAT_HTML = "<!DOCTYPE html><html><head><style>body{width:99%;word-wrap:break-word;} .even{} .odd{background-color:gray}</style></head><body id=\"contents\"></body></html>";
  private static final String ADD_MSG_SCRIPT = "function addMsg(msg) { var body = document.getElementById(\"contents\"); body.innerhtml += msg;}addMsg(";
  
  /**
   * Constructor.
   */
  public ChatApplication() {
    chatInput = new ChatMessageEditor();
    chats = new HashMap<>();
    tabs = new TabPane();
    clients = new ListView();
  }

  @Override
  public void start(Stage primaryStage) {
    Timer<UUIDWrapper> hcmTimer = new Timer<>();
    hcmTimer.start();
    Timer<String> hcrtTimer = new Timer<>();
    hcrtTimer.start();
    HealthCheckResendTimer hcrt = new HealthCheckResendTimer(hcrtTimer);
    hcrtTimer.subscribe(hcrt);
    HealthCheckManager hcm = new HealthCheckManager(hcmTimer, hcrt, 1500000000L, 4500000000L, 1000000000L);
    hcmTimer.subscribe(hcm);
    ClientManager cm = new ClientManager();
    InetAddress serverAddr = null;
    try {
      serverAddr = InetAddress.getByName("49.199.28.147");
    } catch (UnknownHostException ex) {
      System.err.println("Could not find server host address");
      System.exit(404);
    }
    System.out.println(serverAddr);
    
    controller = new ClientController(this, hcm, cm, serverAddr, 57330);
    
//    VBox container = new VBox();
//    container.getChildren().addAll(tabs, chatInput);
//    VBox.setVgrow(tabs, Priority.ALWAYS);
//    chatInput.setPrefHeight(300);
    
    Button button = (Button)chatInput.lookup(".chat-input-send");
    button.setOnAction(e -> {
      send();
    });

    HBox container2 = new HBox();
    container2.getChildren().addAll(clients, tabs);
    HBox.setHgrow(clients, Priority.SOMETIMES);
    clients.setPrefWidth(350);
    
    VBox welcomeContainer = new VBox();
    TextField username = new TextField();
    Button btn = new Button();
    btn.setOnAction(e -> {
      controller.connectToServer(username.getText());
    });
    welcomeContainer.getChildren().addAll(username, btn);
    
    Tab tab = new Tab();
    tab.setContent(welcomeContainer);
    tab.setText("Welcome");
    tabs.getTabs().add(tab);
        
    addTab("Chat", UUID.randomUUID().toString());
    
    scene = new Scene(container2, 1000, 700);
    primaryStage.setScene(scene);
    primaryStage.show();
  }
  
  @Override
  public void stop(){
    controller.disconnectAll();
  }
  
  /**
   * Adds a new tab chat tabs.
   * @param name The name of the tab.
   * @param chatId The id of the chat.
   * @return The created tab.
   */
  public Tab addTab(String name, String chatId) {
    Tab tab = new Tab();
    tab.setText(name);
    tab.getProperties().put("chatID", chatId);
    WebView chat = new WebView();
    chat.getEngine().loadContent(DEFAULT_CHAT_HTML);
    
    VBox test = new VBox();
    test.getChildren().addAll(chat, chatInput);
    VBox.setVgrow(chat, Priority.ALWAYS);
    chatInput.setPrefHeight(300);
    
    tab.setContent(test);
    chats.put(chatId, tab);
    tabs.getTabs().add(tab);
    return tab;
  }

  /**
   * Converts emoticons to emoji img tags and sends the message and chat id to
   * the controller to send to everyone in the chat.
   */
  public void send(){
    String strippedString = chatInput.getUnwrappedHTMLText().replaceAll("</?(?:p|font|br)(.|\n)*?>", "");
      if(!"".equals(strippedString)) {
        Tab tab = tabs.getSelectionModel().getSelectedItem();
        if(tab != null) {
          String input = chatInput.getUnwrappedHTMLText();
          String id = (String)tab.getProperties().get("chatID");
          controller.sendChatMessage(id, input);
         
          chatInput.setHtmlText("");
          chatInput.requestFocus();
          chatInput.lookup("input-area").requestFocus();
        }
      }
  }
  
  /**
   * Entry Point.
   * @param args
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Gets the tab for the chat id and puts the message in the chat list.
   * @param formattedMessage The message received.
   * @param chatId The UUID of the chat.
   */
  @Override
  public void chatUpdate(String formattedMessage, String chatId) {
    Tab chatTab = null;
    for (Tab tab : tabs.getTabs()) {
      if(tab.getProperties().get("chatID").equals(chatId)){
        chatTab = tab;
        break;
      }
    }
    if(chatTab == null) {
      chatTab = addTab(controller.getChat(chatId).getName(), chatId);
      formattedMessage = controller.getChatMessages(chatId) + formattedMessage;
    } 
    WebView chat = (WebView)chatTab.getContent();
    chat.getEngine().executeScript(ADD_MSG_SCRIPT + formattedMessage + ");");
  }

  /**
   * Updates the client list to the new list.
   * @param clientsOnline A list of clients that are online.
   */
  @Override
  public void clientsUpdate(ArrayList<String> clientsOnline) {
    clients.getItems().clear();
    for(String client : clientsOnline) {
      clients.getItems().add(client);
    }
  }

  /**
   * Connection messages from the server.
   * @param connected Is the user connected to the server.
   * @param error Any related information.
   */
  @Override
  public void serverUpdate(boolean connected, String error) {
    System.out.println(error + connected);
    
  }

  /**
   * Creates a chat tab for the given chat UUID if it doesn't already exist.
   * @param chatName The name of the tab.
   * @param chatId The chat UUID.
   */
  @Override
  public void createChatView(String chatName, String chatId) {
    if(!chats.containsKey(chatId))
      addTab(chatName, chatId);
    else {
      throw new java.lang.IllegalArgumentException();
    }
  }

  /**
   * 
   * @param error 
   */
  @Override
  public void registrationStatus(String error) {
    System.out.println(error);
  }

}
