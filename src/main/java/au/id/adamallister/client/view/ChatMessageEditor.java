/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import javafx.css.StyleableProperty;

import javafx.scene.control.Control;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Skin;

/**
 * View Package - ChatMessageEditor - A limited HTML input object.
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 05-JUN-2016 Date Modified: 08-JUL-2016
 */
public class ChatMessageEditor extends Control {

  private final static String PATTERN_STRING = "<body.*?(?=>)>(.*?(?=<\\/body))";
  private static Pattern pattern = Pattern.compile(PATTERN_STRING);

  /**
   * Constructor - Creates the editor based off the ChatMessageEditorSkin
   */
  public ChatMessageEditor() {
    ((StyleableProperty) super.skinClassNameProperty()).applyStyle(
        null,
        "au.id.adamallister.client.view.ChatMessageEditorSkin"
    );
    getStyleClass().add("chat-editor");
  }

  /**
   * Gets the contents of the body.
   *
   * @return
   */
  public String getUnwrappedHTMLText() {
    Matcher result = pattern.matcher(getHtmlText());
    if (result.find()) {
      return result.group(1);
    } else {
      return "";
    }
  }

  @Override
  protected Skin<?> createDefaultSkin() {
    return new ChatMessageEditorSkin(this);
  }

  /**
   * Gets the entire HTML contents.
   * @return
   */
  public String getHtmlText() {
    return ((ChatMessageEditorSkin) getSkin()).getHTMLText();
  }

  /**
   * Sets the the HTML content.
   * @param htmlText 
   */
  public void setHtmlText(String htmlText) {
    ((ChatMessageEditorSkin) getSkin()).setHTMLText(htmlText);
  }


}
