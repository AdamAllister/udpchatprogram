/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.client.view;

import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

/**
 * View Package - EmojiResourceBundle - Contains links to resource for 
 * emojis mapped to text keys.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016 
 * Date Modified: 08-JUL-2016
 */
public class EmojiResourceBundle extends ResourceBundle {
private static final ConcurrentHashMap<String, String> emojiRsc;
  
  static {
    emojiRsc = new ConcurrentHashMap();
    emojiRsc.put(":)", "emoji/png/png_64/1f642.png");
    emojiRsc.put(":(", "emoji/png/png_64/1f641.png");
    emojiRsc.put(":o", "emoji/png/png_64/1f632.png");
    emojiRsc.put("XD", "emoji/png/png_64/1f606.png");
    emojiRsc.put("-_-", "emoji/png/png_64/1f611.png");
    emojiRsc.put("^_^", "emoji/png/png_64/1f604.png");
    //emojiRsc.put(":1f603:", "emoji/png/png_64/1f603.png");
    //emojiRsc.put(":smiley1:", "emoji/png/png_64/1f603.png");
  }

  /**
   * Gets an object with the given key.
   * @param key The key to look for.
   * @return An object mapped to the key if found or null.
   */
  @Override
  protected Object handleGetObject(String key) {
    return emojiRsc.get(key);
  }

  /**
   * Gets a list of keys.
   * @return 
   */
  @Override
  public Enumeration<String> getKeys() {
    return emojiRsc.keys();
  }
}
