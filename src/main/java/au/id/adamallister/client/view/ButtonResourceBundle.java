/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 * Icons Provided Free By Emoji One.
 */
package au.id.adamallister.client.view;

import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

/**
 * View Package - ButtonResourceBundle - Contains links to resource for 
 * buttons mapped to text keys.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016 
 * Date Modified: 08-JUL-2016
 */
public class ButtonResourceBundle extends ResourceBundle {

  private static final ConcurrentHashMap<String, String> buttonRsc;
  
  static {
    buttonRsc = new ConcurrentHashMap();
    buttonRsc.put("sendIcon", "btns/sendIcon.png");
    buttonRsc.put("attachIcon", "btns/attachIcon.png");
    buttonRsc.put("emojiIcon", "btns/emojiIcon.png");
    buttonRsc.put("send", "Send Message");
    buttonRsc.put("attach", "Attach Image");
    buttonRsc.put("emoji", "Emoji");
  }

  /**
   * Gets an object with the given key.
   * @param key The key to look for.
   * @return An object mapped to the key if found or null.
   */
  @Override
  protected Object handleGetObject(String key) {
    return buttonRsc.get(key);
  }

  /**
   * Gets a list of keys.
   * @return 
   */
  @Override
  public Enumeration<String> getKeys() {
    return buttonRsc.keys();
  }
}
