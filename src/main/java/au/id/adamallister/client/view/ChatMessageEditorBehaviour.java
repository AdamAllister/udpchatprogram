/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.client.view;

import java.util.ArrayList;
import java.util.List;
import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.behavior.KeyBinding;
import static javafx.scene.input.KeyCode.B;
import static javafx.scene.input.KeyCode.I;
import static javafx.scene.input.KeyCode.U;

/**
 * View Package - ChatMessageEditorBehaviour - Defines key bindings and behavior
 * for the ChatMessageEditor.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016
 * Date Modified: 06-JUN-2016
 */
public class ChatMessageEditorBehaviour extends BehaviorBase<ChatMessageEditor> {

  /**
   * List of key bindings.
   */
  protected static final List<KeyBinding> CHAT_MESSAGE_EDITOR_BINDINGS = new ArrayList<>();

    static {
      CHAT_MESSAGE_EDITOR_BINDINGS.add(new KeyBinding(B, "bold").shortcut());
      CHAT_MESSAGE_EDITOR_BINDINGS.add(new KeyBinding(I, "italic").shortcut());
      CHAT_MESSAGE_EDITOR_BINDINGS.add(new KeyBinding(U, "underline").shortcut());
    }

  /**
   * Constructor - Passes bindings to BehaviourBase.
   * @param chatMessageEditor
   */
  public ChatMessageEditorBehaviour(ChatMessageEditor chatMessageEditor) {
        super(chatMessageEditor, CHAT_MESSAGE_EDITOR_BINDINGS);
    }

    @Override
    protected void callAction(String name) {

      super.callAction(name);
    }
}
