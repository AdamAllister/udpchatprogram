/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client.view;

import com.sun.javafx.application.PlatformImpl;
import com.sun.javafx.scene.control.skin.BehaviorSkinBase;
import com.sun.javafx.scene.control.skin.FXVK;
import com.sun.javafx.scene.traversal.ParentTraversalEngine;
import com.sun.javafx.webkit.Accessor;
import com.sun.webkit.WebPage;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ResourceBundle;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.css.StyleableProperty;
import javafx.geometry.NodeOrientation;
import static javafx.geometry.NodeOrientation.RIGHT_TO_LEFT;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import org.w3c.dom.html.HTMLDocument;
import org.w3c.dom.html.HTMLElement;

/**
 * View Package - ChatMessageEditorSkin - A skin for the ChatMessageEditor that
 * implements behavior, tool bars, etc.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016 
 * Date Modified: 08-JUL-2016
 */
public class ChatMessageEditorSkin extends BehaviorSkinBase<ChatMessageEditor, ChatMessageEditorBehaviour> {

  private ToolBar toolbar;

  //private GridPane gridPane;
  private HBox hBox;

  private Button sendButton;
  private Button attachButton;
  private Button emojiButton;

  private WebView webView;
  private WebPage webPage;

  private static final String FORMAT_PARAGRAPH = "<p>";
  private static final String FORMAT_COMMAND = "formatblock";
  private static final String INSERT_NEW_LINE_COMMAND = "insertnewline";
  private static final String INSERT_TAB_COMMAND = "inserttab";

  private static final int EMOJI_MENU_WIDTH = 100;

  private static final String SEND_COMMAND = "send";
  private static final String ATTACH_COMMAND = "attach";
  private static final String EMOJI_COMMAND = "emoji";

  //private static final String BOLD_COMMAND = "bold";
  //private static final String ITALIC_COMMAND = "italic";
  //private static final String UNDERLINE_COMMAND = "underline";
  //private static final String STRIKETHROUGH_COMMAND = "strikethrough";
  private static final Color DEFAULT_BG_COLOR = Color.WHITE;
  private static final Color DEFAULT_FG_COLOR = Color.BLACK;

  ////TODO REMOVE IF NOT REQUIRED
  // As per RT-16379: default OS -> font mappings:
  private static final String[] DEFAULT_WINDOWS_7_MAPPINGS = {
    "Windows 7", "Segoe UI", "12px", "", "120"
  };
  private static final String[][] DEFAULT_OS_MAPPINGS = {
    // OS               Font name           size      weight  DPI
    {"Windows XP", "Tahoma", "12px", "", "96"},
    {"Windows Vista", "Segoe UI", "12px", "", "96"},
    DEFAULT_WINDOWS_7_MAPPINGS,
    {"Mac OS X", "Lucida Grande", "12px", "", "72"},
    {"Linux", "Lucida Sans", "12px", "", "96"},};
  private static final String DEFAULT_OS_FONT = getOSMappings()[1];

  ////TODO REMOVE IF NOT REQUIRED
  private static String[] getOSMappings() {
    String os = System.getProperty("os.name");
    for (int i = 0; i < DEFAULT_OS_MAPPINGS.length; i++) {
      if (os.equals(DEFAULT_OS_MAPPINGS[i][0])) {
        return DEFAULT_OS_MAPPINGS[i];
      }
    }
    return DEFAULT_WINDOWS_7_MAPPINGS;
  }

  private ParentTraversalEngine engine;
  private boolean resetToolbarState = false;
  private String cachedHTMLText
      = "<html>"
      + "<head></head><body contenteditable=\"true\"></body></html>";

  private ListChangeListener<Node> itemsListener = c -> {
    while (c.next()) {
      if (c.getRemovedSize() > 0) {
        for (Node n : c.getList()) {
          if (n instanceof WebView) {
            // RT-28611 webView removed - set associated webPage to null
            webPage.dispose();
          }
        }
      }
    }
  };

  /**
   * Constructor - Creates layout, listeners and objects it the editor needs.
   * @param editor The editor the skin belongs to.
   */
  public ChatMessageEditorSkin(ChatMessageEditor editor) {
    super(editor, new ChatMessageEditorBehaviour(editor));

    getChildren().clear();

    //gridPane = new GridPane();
    //gridPane.getStyleClass().add("grid");
    hBox = new HBox();
    hBox.getStyleClass().add("hbox");

    getChildren().addAll(hBox);

    toolbar = new ToolBar();
    toolbar.getStyleClass().add("side-toolbar");
    toolbar.setOrientation(Orientation.VERTICAL);
    toolbar.setMaxWidth(100);
    //gridPane.add(toolbar, 1, 0);

    populateToolbars();
    isFirstRun = false;
    webView = new WebView();
    webView.getStyleClass().add("input-area");
    //gridPane.add(webView, 0, 0);
    webView.maxWidth(Double.MAX_VALUE);
    webView.setFocusTraversable(true);
    getChildren().addAll(webView, toolbar);

    HBox.setHgrow(toolbar, Priority.ALWAYS);
    HBox.setHgrow(webView, Priority.ALWAYS);

    hBox.getChildren().addAll(webView, toolbar);

    //ColumnConstraints column = new ColumnConstraints();
    //column.setHgrow(Priority.ALWAYS);
    //gridPane.getColumnConstraints().add(column);
    //gridPane.getColumnConstraints().add(column);
    webPage = Accessor.getPageFor(webView.getEngine());

    webView.addEventHandler(MouseEvent.MOUSE_RELEASED, event2 -> {
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
          updateToolbarState(true);
        }
      });
    });

    getSkinnable().focusedProperty().addListener((observable, oldValue, newValue) -> {
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
          if (newValue) {
            webView.requestFocus();
          }
        }
      });
    });

    // On Enter press
    webView.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
      applyTextFormatting();
      //replaceEmoticons();             
      if (event.getCode() == KeyCode.ENTER && !event.isShiftDown()) {
        sendButton.fire();
        event.consume();
      }

    });

    // On Enter press
    webView.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
      //switchEmoticons();
    });
    
    webView.addEventFilter(KeyEvent.KEY_RELEASED, event -> {
      //switchEmoticons();
    });

    webView.focusedProperty().addListener((observable, oldValue, newValue) -> {
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
          updateToolbarState(true);

          if (PlatformImpl.isSupported(ConditionalFeature.VIRTUAL_KEYBOARD)) {
            Scene scene = getSkinnable().getScene();
            if (newValue) {
              FXVK.attach(webView);
            } else if (scene == null
                || scene.getWindow() == null
                || !scene.getWindow().isFocused()
                || !(scene.getFocusOwner() instanceof TextInputControl /*||
                                     getScene().getFocusOwner() instanceof WebView*/)) {
              FXVK.detach();
            }
          }
        }
      });
    });

    webView.getEngine().getLoadWorker().workDoneProperty().addListener((observable, oldValue, newValue) -> {
      Platform.runLater(() -> {
        webView.requestLayout();
      });

      double totalWork = webView.getEngine().getLoadWorker().getTotalWork();
      if (newValue.doubleValue() == totalWork) {

        cachedHTMLText = null;
        Platform.runLater(() -> {
          setContentEditable(true);
          updateToolbarState(true);
          updateNodeOrientation();
        });
      }
    });

    enableToolbar(true);
    setHTMLText(cachedHTMLText);

    webView.setFocusTraversable(true);
    hBox.getChildren().addListener(itemsListener);
    //hBox.getChildren().addListener(itemsListener);

  }

  private void applyTextFormatting() {

    if (webPage.getClientCommittedTextLength() == 0) {

      executeCommand(FORMAT_COMMAND, FORMAT_PARAGRAPH);

    }
  }

  /**
   * Returns the cached HTML if new HTML still loading or the HTML from the 
   * web engine.
   * @return
   */
  public final String getHTMLText() {
    // RT17203 setHTMLText is asynchronous.  We use the cached version of
    // the html text until the page finishes loading.        
    return cachedHTMLText != null ? cachedHTMLText : webPage.getHtml(webPage.getMainFrame());
  }

  /**
   * Sets the HTML of the webPage.
   * @param htmlText 
   */
  public final void setHTMLText(String htmlText) {
    webPage.load(webPage.getMainFrame(), htmlText, "text/html");

    Platform.runLater(() -> {
      updateToolbarState(true);
    });
  }

  /**
   * Processes keyboard shortcuts.
   * @param name Shortcut name.
   */
  public void keyboardShortcuts(String name) {

  }

  private ResourceBundle resources;

  private void populateToolbars() {
    resources = ResourceBundle.getBundle(ButtonResourceBundle.class.getName());
    // Toolbar 1
    sendButton = addButton(toolbar, resources.getString("sendIcon"), resources.getString("send"), null, "chat-input-send");
    attachButton = addButton(toolbar, resources.getString("attachIcon"), resources.getString("attach"), null, "chat-input-attach");
    emojiButton = addButton(toolbar, resources.getString("emojiIcon"), resources.getString("emoji"), null, "chat-input-emoji");
    getChildren().addAll(sendButton, attachButton, emojiButton);
  }

  private boolean executeCommand(String command, String value) {
    return webPage.executeCommand(command, value);
  }

  private Button addButton(ToolBar toolbar, final String iconName, String tooltipText,
      final String command, final String styleClass) {
    Button button = new Button();
    button.setFocusTraversable(false);
    button.getStyleClass().add(styleClass);
    toolbar.getItems().add(button);
    ClassLoader cl = ChatMessageEditorSkin.class.getClassLoader();
    Image icon = AccessController.doPrivileged((PrivilegedAction<Image>) () -> new Image(cl.getResource(iconName).toString()));
//        button.setGraphic(new ImageView(icon));
    ((StyleableProperty) button.graphicProperty()).applyStyle(null, new ImageView(icon));
    button.setTooltip(new Tooltip(tooltipText));

    return button;
  }

  private ToggleButton addToggleButton(ToolBar toolbar, ToggleGroup toggleGroup,
      final String iconName, String tooltipText, final String command, final String styleClass) {
    ToggleButton toggleButton = new ToggleButton();
    toggleButton.setUserData(command);
    toggleButton.setFocusTraversable(false);
    toggleButton.getStyleClass().add(styleClass);
    toolbar.getItems().add(toggleButton);
    if (toggleGroup != null) {
      toggleButton.setToggleGroup(toggleGroup);
    }

    Image icon = AccessController.doPrivileged((PrivilegedAction<Image>) () -> new Image(ChatMessageEditorSkin.class.getClassLoader().getResource(iconName).toString()));
    ((StyleableProperty) toggleButton.graphicProperty()).applyStyle(null, new ImageView(icon));

    toggleButton.setTooltip(new Tooltip(tooltipText));

    return toggleButton;
  }

  private void updateNodeOrientation() {
    NodeOrientation orientation = getSkinnable().getEffectiveNodeOrientation();

    HTMLDocument htmlDocument = (HTMLDocument) webPage.getDocument(webPage.getMainFrame());
    HTMLElement htmlDocumentElement = (HTMLElement) htmlDocument.getDocumentElement();
    if (htmlDocumentElement.getAttribute("dir") == null) {
      htmlDocumentElement.setAttribute("dir", (orientation == RIGHT_TO_LEFT) ? "rtl" : "ltr");
    }

  }

  private void updateToolbarState(final boolean updateAlignment) {
    if (!webView.isFocused()) {
      return;
    }

    // These command aways return true.
    sendButton.setDisable(!isCommandEnabled(SEND_COMMAND));
    emojiButton.setDisable(!isCommandEnabled(EMOJI_COMMAND));
    attachButton.setDisable(!isCommandEnabled(ATTACH_COMMAND));
  }

  private void enableToolbar(final boolean enable) {
    Platform.runLater(() -> {

      // Make sure buttons have been created to avoid NPE
      if (sendButton == null) {
        return;
      }

      /*
            ** if we're to enable, we still only enable
            ** the cut/copy/paste buttons that make sense
       */
      if (enable) {
        sendButton.setDisable(!isCommandEnabled(SEND_COMMAND));
        emojiButton.setDisable(!isCommandEnabled(EMOJI_COMMAND));
        attachButton.setDisable(!isCommandEnabled(ATTACH_COMMAND));
      } else {
        sendButton.setDisable(true);
        emojiButton.setDisable(true);
        attachButton.setDisable(true);
      }

      //insertHorizontalRuleButton.setDisable(!enable);
    });
  }

  private boolean isCommandEnabled(String command) {
    return command == SEND_COMMAND;
  }

  private void setContentEditable(boolean b) {
    HTMLDocument htmlDocument = (HTMLDocument) webPage.getDocument(webPage.getMainFrame());
    HTMLElement htmlDocumentElement = (HTMLElement) htmlDocument.getDocumentElement();
    HTMLElement htmlBodyElement = (HTMLElement) htmlDocumentElement.getElementsByTagName("body").item(0);
    htmlBodyElement.setAttribute("contenteditable", Boolean.toString(b));
  }

//  private String tempSetScript
//      = "function setCaret(child) {\n"
//      + "    var el = document.getElementById(\"caret\");\n"
//      + "    var img = document.getElementById(\"lastSwitched\");\n"
//      + "    var range = document.createRange();\n"
//      + "    var sel = document.getSelection();\n"
//      + "    range.setStart(img.nextSibling, 0);\n"
//      + "    range.collapse(true);\n"
//      + "    sel.removeAllRanges();\n"
//      + "    sel.addRange(range);\n"
//      + "    el.focus();\n"
//      + "    img.removeAttribute(\"id\")"
//      + "}setCaret();";
//  
//  private int replaceEmoticonsInParagraph(HTMLDocument htmlDocument, HTMLElement p) {
//    EmojiResourceBundle erb = new EmojiResourceBundle();
//    Enumeration<String> keys = erb.getKeys();
//    boolean changed = false;
//    ClassLoader cl = ChatMessageEditorSkin.class.getClassLoader();
//    ArrayList<org.w3c.dom.Node> newNodes = new ArrayList<>();
//    
//    NodeList oldNodes = p.getChildNodes();
//    int finalNodePos = -1;
//    for (int i = 0; i < oldNodes.getLength(); i++) {
//      newNodes.add(oldNodes.item(i));
//    }
//    while (keys.hasMoreElements()) {
//      String emoticon = keys.nextElement();
//      String rscLocation = erb.getString(emoticon);
//      if (rscLocation != null) {
//        String imgLocation = getClass().getClassLoader().getResource(rscLocation).toString();
//        ArrayList<org.w3c.dom.Node> tempNodes = new ArrayList<>();
//        for (int j = 0; j < newNodes.size(); j++) {
//          if (newNodes.get(j).getNodeType() == org.w3c.dom.Node.TEXT_NODE) {
//            Text textNode = (Text) newNodes.get(j);
//            System.out.println("Textnode "+ j + ": " + textNode.getWholeText());
//            String[] splitNode = textNode.getWholeText().split(emoticon);
//            if (splitNode.length > 1) {
//              changed = true;
//              //System.out.println(Arrays.toString(splitNode));
//              for (int k = 0; k < splitNode.length; k++) {
//                if(!splitNode[k].equals(" ")){
//                  org.w3c.dom.Node newNode = htmlDocument.createTextNode(splitNode[k]);
//                  tempNodes.add(newNode);
//                }
//                finalNodePos = tempNodes.size() - 1;
//                if (k != splitNode.length - 1) {
//                  HTMLElement imgNode = (HTMLElement) htmlDocument.createElement("img");
//                  imgNode.setAttribute("src", imgLocation);
//                  imgNode.setAttribute("width", "16px");
//                  tempNodes.add((org.w3c.dom.Node) imgNode);
//                }
//              }
//            } else {
//              tempNodes.add(newNodes.get(j));
//            }
//          } else if(newNodes.get(j).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
//            tempNodes.add(newNodes.get(j));
//          }
//        }
//        newNodes = tempNodes;
//        if(changed)
//          break;
//      }
//    }
//    if (changed) {
//      while(p.getFirstChild() != null) {
//        p.removeChild(p.getFirstChild());
//      }
//      for (org.w3c.dom.Node newNode : newNodes) {
//        p.appendChild(newNode);
//      }
//    }
//    return finalNodePos;
//  }
//  
//  private boolean test(HTMLDocument htmlDocument, HTMLElement p) {
//    EmojiResourceBundle erb = new EmojiResourceBundle();
//    Enumeration<String> keys = erb.getKeys();
//    
//    
//    ClassLoader cl = ChatMessageEditorSkin.class.getClassLoader();
//    
//    ArrayList<org.w3c.dom.Node> newNodes = new ArrayList<>();
//    ArrayList<org.w3c.dom.Node> oldNodes = new ArrayList<>();
//    NodeList oldNodeList = p.getChildNodes();
//    for (int i = 0; i < oldNodeList.getLength(); i++) {
//      oldNodes.add(oldNodeList.item(i));
//    }
//    
//    boolean changed = false;
//    
//    while (keys.hasMoreElements()) {
//      newNodes.clear();
//      String emoticon = keys.nextElement();
//      String rscLocation = erb.getString(emoticon);
//      String imgLocation = "";
//      if(rscLocation != null){
//        imgLocation = getClass().getClassLoader().getResource(rscLocation).toString();
//      }
//      for(org.w3c.dom.Node oldNode : oldNodes) {
//        if(oldNode.getNodeType() == org.w3c.dom.Node.TEXT_NODE){
//          String[] splitNode = ((Text)oldNode).getWholeText().split(emoticon);
//          if (splitNode.length > 1) {
//              changed = true;
//              for (int k = 0; k < splitNode.length; k++) {
//                org.w3c.dom.Node newNode = htmlDocument.createTextNode(splitNode[k]);
//                newNodes.add(newNode);
//                if (k != splitNode.length - 1) {
//                  HTMLElement imgNode = (HTMLElement) htmlDocument.createElement("img");
//                  imgNode.setAttribute("src", imgLocation);
//                  imgNode.setAttribute("width", "16px");
//                  imgNode.setId("lastSwitched");
//                  newNodes.add((org.w3c.dom.Node) imgNode);
//                }
//              }
//          } else {
//            newNodes.add(oldNode);
//          }
//        } else if(oldNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
//          newNodes.add(oldNode);
//        }
//      }
//      oldNodes.clear();
//      oldNodes.addAll(newNodes);
//    }
//    if (changed) {
//      while(p.getFirstChild() != null) {
//        p.removeChild(p.getFirstChild());
//      }
//      for (org.w3c.dom.Node newNode : oldNodes) {
//        p.appendChild(newNode);
//      }
//      p.normalize();
//    }
//    return changed;
//  }
//
//  private void switchEmoticons() {
//    HTMLDocument htmlDocument = (HTMLDocument) webPage.getDocument(webPage.getMainFrame());
//    HTMLElement body = htmlDocument.getBody();
//    NodeList paragraphs = body.getElementsByTagName("p");
//    HTMLElement activeP = null;
//    boolean emojiChanged = false;
//    for (int i = 0; i < paragraphs.getLength(); i++) {
//      HTMLElement p = (HTMLElement) paragraphs.item(i);
//      emojiChanged = test(htmlDocument, p);
//      if (emojiChanged) {
//        activeP = p;
//        activeP.setId("caret");
//        break;
//      }
//    }
//    if(activeP != null) {
//      webView.getEngine().executeScript(tempSetScript);
//      activeP.removeAttribute("id");
//    }
//  }

  private boolean isFirstRun = true;

  @Override
  protected void layoutChildren(final double x, final double y,
      final double w, final double h) {

    if (isFirstRun) {
      populateToolbars();
      isFirstRun = false;
    }
    super.layoutChildren(x, y, w, h);
    double toolbarWidth = toolbar.prefWidth(-1);
  }
}
