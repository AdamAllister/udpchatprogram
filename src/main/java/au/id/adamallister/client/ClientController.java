/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.connection.ClientManager;
import au.id.adamallister.connection.Client;
import au.id.adamallister.connection.ServerConnection;
import au.id.adamallister.message.*;
import au.id.adamallister.client.view.ClientView;
import au.id.adamallister.client.view.EmojiResourceBundle;
import au.id.adamallister.health.HealthCheckManager;
import au.id.adamallister.message.messages.*;
import au.id.adamallister.connection.SessionEndpoint;
import au.id.adamallister.util.UUIDWrapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import au.id.adamallister.health.HealthCheckHandler;
import au.id.adamallister.packet.PacketManager;
import au.id.adamallister.packet.PacketReceiver;
import au.id.adamallister.util.timer.Timer;
import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.Enumeration;

/**
 * Chat Client - Client Controller - The controller for the client side.
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 6-April-2016 
 * Date Modified: 01-July-2016
 */
public class ClientController implements ClientMessageSubscriber, ServerMessageSubscriber, HealthCheckHandler {

  private HashMap<String, Chat> chats;
  private final ServerConnection serverConnection;

  private ServerMessageManager serverMessages;
  private ClientMessageManager clientMessages;

  /**
   * The public address of the chat server.
   */
  public final InetAddress SERVER_ADDRESS;

  /**
   * The public port of the chat server.
   */
  public final int SERVER_PORT;

  /**
   * The size of each packet sent between clients, server, etc.
   * All clients and servers must have the same packet size.
   */
  public static final int PACKET_SIZE = 512;

  private String username;
  private boolean registered = false;

  private HashMap<InetAddress, Integer> myAddresses;
  private ClientManager currentClients;
  private HealthCheckManager healthCheckMngr;

  private HashMap<UUIDWrapper, Client> handshakes;

  private ClientView view;

  /**
   * Constructor - Initializes objects from parameters and attempts to get this 
   * clients endpoints. 
   * Sets the client view, health check manager and client manager to the passed 
   * parameters.
   * Gets client local address and connects to an external source to get public
   * address.
   * @param aView The client view that will display the information to the client
   * and send information from the client.
   * @param hcm The configured health check manager that will be used to manage
   * client health checks.
   * @param cm The configured client manager that will be used to keep track of 
   * and manage clients.
   * @param pubServAddr
   * @param pubServPort
   */
  public ClientController(ClientView aView, HealthCheckManager hcm, ClientManager cm, InetAddress pubServAddr, int pubServPort) {
    if(hcm == null)
      throw new InvalidParameterException("HealthCheckManager cannot be null");
    if(cm == null)
      throw new InvalidParameterException("ClientManager cannot be null");
    serverMessages = null;
    view = aView;
    currentClients = cm;
    healthCheckMngr = hcm;
    myAddresses = new HashMap<>();

    //Attempt to get and set local endpoint
    try {
      myAddresses.put(InetAddress.getLocalHost(), pubServPort + 1);
    } catch (UnknownHostException ex) {
      ex.printStackTrace();
    }

    //Attempt to get and set public endpoint
    try {
      URL myURL = new URL("http://adamallister.id.au/address.php");
      BufferedReader in = new BufferedReader(new InputStreamReader(myURL.openStream()));
      String externalIP = in.readLine();
      in.close();
      System.out.println(externalIP);
      myAddresses.put(InetAddress.getByName(externalIP), pubServPort + 1);
    } catch (MalformedURLException e) {
      System.err.println("Address URL incorrect");
      System.exit(404);
    } catch (IOException e) {
      System.err.println("IOException");
      System.exit(10);
    }

    
    chats = new HashMap<>();
    handshakes = new HashMap<>();
    
    //Attempt to set server endpoints
    ArrayList<SessionEndpoint> serverEndpoints = new ArrayList<>();
    SERVER_ADDRESS = pubServAddr;
    SERVER_PORT = pubServPort;
    serverEndpoints.add(new SessionEndpoint(SERVER_ADDRESS, SERVER_PORT));

    serverConnection = new ServerConnection(serverEndpoints);
  }

  /**
   * Takes a list of client usernames and attempts to create a chat for those
   * clients.
   * Looks for clients with the passed usernames and adds the ones it finds.
   * If at least one client is found, it creates a chat object and sends a chat
   * request to the clients.
   * @param clientUsernames A list of client usernames this client wants to chat
   * with.
   * @return A string representation of the new chat objects UUID, or null if no
   * clients were found.
   */
  public String createChat(ArrayList<String> clientUsernames) {
    ClientManager clients = new ClientManager();
    Client client = null;
    for (String clientUsername : clientUsernames) {
      if ((client = currentClients.getClient(clientUsername)) != null) {
        clients.addClient(client);
      }
    }
    if (!clients.getClients().isEmpty()) {
      UUIDWrapper chatID = new UUIDWrapper(UUID.randomUUID());
      Chat chat = new Chat(clients, new ArrayList<>(), chatID);
      sendChatRequest(clients.getClients(), chatID);
      chats.put(chat.getID().toString(), chat);
      return chatID.toString();
    }

    return null;
  }

  /**
   * 
   * @param clients
   * @param chatID 
   */
  private void sendChatRequest(Collection<Client> clients, UUIDWrapper chatID) {
    ChatRequestRequestMessage rqst = new ChatRequestRequestMessage(new UUIDWrapper(UUID.randomUUID()), username, "", chatID);
    for (Client client : clients) {
      rqst.setResponder(client.getUsername());
      serverMessages.send(rqst, SERVER_ADDRESS, SERVER_PORT);
    }
  }

  /**
   * Creates and sends a chat message to all clients in chat.
   * Creates a new chat message and looks for the chat. If chat is found
   * it sends the message to all clients in the chat and adds that message to the
   * clients own chat messages.
   * Passes a formatted string to view to display the message.
   * @param chatID a string representation of the chat UUID
   * @param msg The message to be sent
   * @throws IllegalArgumentException if the chat id is not for a valid chat.
   */
  public void sendChatMessage(String chatID, String msg) {
    ChatMessage chatMsg = new ChatMessage(username, new UUIDWrapper(UUID.fromString(chatID)), msg);
    Chat chat = chats.get(chatID);
    if(chat != null) {
      ArrayList<Client> clients = chat.getClients();
      for (Client client : clients) {
        if (client.isConnected()) {
          clientMessages.send(chatMsg, client.getAddress(), client.getPort());
        }
      }
      chat.addMessage(chatMsg);
      String formattedMessage = formatMessage(chatMsg.getMessage());
      if (chat.getMessages().size() % 2 == 0) {
        view.chatUpdate("<div class=\"even\">" + chatMsg.getFromName() + ":"
            + formattedMessage + " -" + chatMsg.getTimeString() + "</div>",
            chatMsg.getChatID().toString());
      } else {
        view.chatUpdate("<div class=\"odd\">" + chatMsg.getFromName() + ":"
            + formattedMessage + " -" + chatMsg.getTimeString() + "</div>",
            chatMsg.getChatID().toString());
      }
    } else {
      throw new IllegalArgumentException("Tried to send chat message for chat that does not exist.");
    }
  }

  /**
   * Returns a chat object with the passed chat id.
   * @param chatID A string representation of the chat id.
   * @return the chat that has the same chat id or null if none found.
   */
  public Chat getChat(String chatID) {
    return chats.get(chatID);
  }

  private String formatMessage(String originalMsg) {
    EmojiResourceBundle erb = new EmojiResourceBundle();
    Enumeration<String> keys = erb.getKeys();
    String formattedMessage = originalMsg;
    while(keys.hasMoreElements()){
      String key = keys.nextElement();
      String location = getClass().getClassLoader().getResource(erb.getString(key)).toString();
      formattedMessage = formattedMessage.replace(key, "<img width=\"16px\" src=" + location + " alt=\""+ key +"\">");
    }
    return formattedMessage;
  }
  
  /**
   * Returns a string representation of all messages for the chat that has the
   * passed chat id.
   * @param chatID A string representation of the a chat id.
   * @return A formatted string representation of all chat message or an empty 
   * string if chat not found.
   */
  public String getChatMessages(String chatID) {
    List<ChatMessage> msgs = getChat(chatID).getMessages();
    String stringMessages = "";
    boolean even = true;
    for (ChatMessage msg : msgs) {
      if (even) {
        stringMessages += "<div class=\"even\">";
      } else {
        stringMessages += "<div class=\"odd\">";
      }
      stringMessages += msg.getFromName() + ":" + formatMessage(msg.getMessage()) + " -" + msg.getTimeString() + "</div>";
      even = !even;
    }
    return stringMessages;
  }

  /**
   * Is the client registered on the server.
   * @return true if the client has successfully registered on the server
   */
  public boolean isRegistered() {
    return registered;
  }

  
  ////////////////////////////////////////////////
  ////                                        ////
  ////  Interface - Client Message Subscriber ////
  ////                                        ////
  ////////////////////////////////////////////////

  /**
   * A chat message is received from a client.
   * Checks the client who sent the chat message is the same as the sender in the
   * message, as well as that they are online and connected.
   * Attempts to get the chat the message is for and checks if the client belongs
   * to the chat before adding the message.
   * If added it sends the view a string representation of the message.
   * @param chatMsg The chat message sent by a client.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  @Override
  public void clientMessage(ChatMessage chatMsg, InetAddress address, int port) {
    Client client = currentClients.getClient(chatMsg.getFromName());
    if (client.isOnline() && client.isConnected() && client.isActiveEndpoint(address, port)) {
      Chat chat = chats.get(chatMsg.getChatID().toString());
      if (chat != null && chat.getClientManager().hasClient(client.getUsername())) {
        chat.addMessage(chatMsg);
        if (chat.getMessages().size() % 2 == 0) {
          view.chatUpdate("<div class=\"even\">" + chatMsg.getFromName() + ":"
              + chatMsg.getMessage() + " -" + chatMsg.getTimeString() + "</div>",
              chatMsg.getChatID().toString());
        } else {
          view.chatUpdate("<div class=\"odd\">" + chatMsg.getFromName() + ":"
              + chatMsg.getMessage() + " -" + chatMsg.getTimeString() + "</div>",
              chatMsg.getChatID().toString());
        }
      }
    }
  }

  /**
   * Process a health check message received from a client.
   * Checks the sender is a client and is online.
   * Checks if the health check is a response to a check sent by this client, 
   * and if it is, tells the health check manager a response was received.
   * If not a response, a response is sent back to the sender.
   * @param hckMsg The health check message received.
   * @param address The address of the sender.
   * @param port The port of the sender.
   */
  @Override
  public void clientHealthCheck(HealthCheckMessage hckMsg, InetAddress address, int port) {
    Client client = currentClients.getClient(address, port);
    if (client != null && client.isOnline()) {
      if (healthCheckMngr.isResponse(hckMsg.getMessageUID(), client.getUsername())) {
        healthCheckMngr.healthCheckResponse(hckMsg.getMessageUID(), client.getUsername());
      } else {
        this.clientMessages.sendOnce(hckMsg, address, port);
      }
    }
  }

  /**
   * Process a handshake message received from a client.
   * Checks the sender is a client and is online.
   * Checks the client is waiting for a handshake.
   * If awaiting handshake response, the client removes it from the list of
   * handshakes, sets the client as connected and sends a handshake back.
   * @param hndMsg The handshake message received from a client.
   * @param address The address if the sender.
   * @param port The port of the sender.
   */
  @Override
  public void clientHandShake(HandShakeMessage hndMsg, InetAddress address, int port) {
    Client client;
    if (((client = handshakes.get(hndMsg.getMessageUID())) != null) && client.isOnline() && client.hasEndpoint(address, port)) {
      client.connected(address, port);
      clientMessages.sendOnce(new HandShakeMessage(hndMsg.getMessageUID(), username), address, port);
      handshakes.remove(hndMsg.getMessageUID());
      healthCheckMngr.addOrRenewConnection(client.getUsername());
    }
  }

  ////////////////////////////////////////////////
  ////////////////////////////////////////////////
  ////                                        ////
  ////  Interface - Server Message Subscriber ////
  ////                                        ////
  ////////////////////////////////////////////////

  /**
   * Process a registration message sent from the server.
   * It checks the message is a server registration message and its from the
   * server.
   * Status code 0 means a successful registration, 
   * all others are denials or errors.
   * @param regMsg The registration message sent from the server.
   * @param address The address of the sender. (should be the server)
   * @param port The port of the server.
   */
  @Override
  public void serverRegistration(RegistrationMessage regMsg, InetAddress address, int port) {
    ServerRegistrationMessage msg = null;
    if (regMsg.equals(MessageType.SREG)) {
      msg = (ServerRegistrationMessage) regMsg;
    } else {
      throw new IllegalArgumentException("Server passed non server registration message.");
    }

    ClientRegistrationMessage response = null;
    if (serverConnection.getAddress() == address && serverConnection.getPort() == port) {
      switch (msg.getCode()) {
        case 0:
          registered = true;
          username = regMsg.getUsername();
          break;
        //Not registered
        case 10:
          registered = false;
          if (username != null) {
            response = new ClientRegistrationMessage(regMsg.getMessageUID(), (InetAddress[]) myAddresses.keySet().toArray(),
                (Integer[]) myAddresses.values().toArray(), username);
            serverMessages.send(regMsg, address, port);
          }
          break;
        //Already Registered
        case 20:
          registered = true;
          username = regMsg.getUsername();
          view.registrationStatus("You are already registered as " + regMsg.getUsername());
          break;
        //Name Taken
        case 30:
          registered = false;
          view.registrationStatus("Already In Use");
          break;
        //Name Invalid
        case 40:
          registered = false;
          view.registrationStatus("Not a valid name");
          break;
        default:
          break;
      }
    }

  }

  /**
   * Process a chat request message relayed by the server.
   * @param rqstMsg The request message relayed by the server from a client.
   * @param address The address of the sender (the server).
   * @param port The port of the sender (the server).
   */
  @Override
  public void serverChatRequest(ChatRequestRequestMessage rqstMsg, InetAddress address, int port) {
    if (serverConnection.getAddress() == address
        && serverConnection.getPort() == port
        && currentClients.hasClient(rqstMsg.getRequesterUsername())) {
      ////TODO 1 Setup Blocking and Block list
      ////TODO 2 Setup UI REPONSDING I.E. user chooses response
      ChatRequestResponseMessage msg = new ChatRequestResponseMessage(rqstMsg.getMessageUID(),
          rqstMsg.getRequesterUsername(), rqstMsg.getResponderUsername(), true, rqstMsg.getChatID());
      serverMessages.send(msg, address, port);
      sendConnectPackets(currentClients.getClient(rqstMsg.getRequesterUsername()), rqstMsg.getMessageUID());
      handshakes.put(rqstMsg.getMessageUID(), currentClients.getClient(rqstMsg.getRequesterUsername()));
    }
  }

  ////TODO check a request was sent

  /**
   * Process a chat request response message relayed by the server.
   * @param rspnMsg The request response message relayed by the server from a client.
   * @param address The address of the sender (the server).
   * @param port The port of the sender (the server).
   */
  @Override
  public void serverChatRequestResponse(ChatRequestResponseMessage rspnMsg, InetAddress address, int port) {
    Date date = new Date();
    Client client = currentClients.getClient(username);
    if (rspnMsg.getResponse() && client != null) {
      ChatMessage msg = new ChatMessage(rspnMsg.getMessageUID(), rspnMsg.getResponderUsername(),
          rspnMsg.getChatID(), date.getTime(), rspnMsg.getResponderUsername() + " accepted chat request");
      chats.get(msg.getChatID().toString()).addMessage(msg);
      sendConnectPackets(client, rspnMsg.getMessageUID());
      handshakes.put(rspnMsg.getMessageUID(), client);
    } else {

    }
  }

  /**
   * Process a health check response message sent by the server.
   * Updates the health manager if from the server.
   * @param hckMsg The health check message from the server.
   * @param serverAddress The address of the sender (the server).
   * @param serverPort The port of the sender (the server).
   */
  @Override
  public void serverHealthCheck(HealthCheckMessage hckMsg, InetAddress serverAddress, int serverPort) {
    if (serverConnection.isActiveEndpoint(serverAddress, serverPort)) {
      if (healthCheckMngr.isResponse(hckMsg.getMessageUID(), "server")) {
        healthCheckMngr.healthCheckResponse(hckMsg.getMessageUID(), "server");
      } else {
        this.clientMessages.sendOnce(hckMsg, serverAddress, serverPort);
      }
    }
  }

  /**
   * Process a handshake response message sent by the server.
   * If the handshake is received from the server its state is marked as connected.
   * @param hndMsg The handshake message from the server.
   * @param serverAddress The address of the sender (the server).
   * @param serverPort The address of the sender (the server).
   */
  @Override
  public void serverHandshake(HandShakeMessage hndMsg, InetAddress serverAddress, int serverPort) {
    if (serverConnection.hasEndpoint(serverAddress, serverPort)) {
      healthCheckMngr.addOrRenewConnection("server");
      serverConnection.connected(serverAddress, serverPort);
    }
  }

  /**
   * Process a client update message sent by the server.
   * Updates a clients state based on the update message.
   * @param updMsg The client update message from the server.
   * @param address The address of the sender (the server).
   * @param port The address of the sender (the server).
   */
  @Override
  public void serverClientUpdate(ClientUpdateMessage updMsg, InetAddress address, int port) {
    ArrayList<SessionEndpoint> endpoints = updMsg.getEndpoints();
    Client client = currentClients.getClient(updMsg.getName());
    if (client != null) {
      client.setOnline(updMsg.isOnline());
      if(!endpoints.isEmpty())
        client.setEndpoints(endpoints);
    }
  }

  //////////////////////////////////////////////// 

  /**
   * Attempts to connect to the server and start up datagram sockets.
   * @param aUsername The username the client wishes to use.
   */
  
  public void connectToServer(String aUsername) {
    username = aUsername;
    DatagramSocket serverSocket = null;
    DatagramSocket clientSocket = null;
    boolean success = true;
    if(serverMessages != null && clientMessages != null) {
      try {
        serverSocket = new DatagramSocket(SERVER_PORT);
      } catch (IOException ex) {
        view.serverUpdate(false, "Could not open server socket listener on port" + ":" + SERVER_PORT);
        success = false;
      }

      try {
        clientSocket = new DatagramSocket(SERVER_PORT + 1);
      } catch (IOException ex) {
        view.serverUpdate(false, "Could not open client socket listener on port" + ":" + SERVER_PORT);
        success = false;
      }
    }

    if (success) {
      if(serverMessages == null && clientMessages == null) {
        PacketReceiver serverPR = new PacketReceiver(serverSocket, PACKET_SIZE);
        Timer<String> serverExpirationTimer = new Timer<>();
        serverExpirationTimer.start();
        PacketManager serverPM = new PacketManager(clientSocket, PACKET_SIZE, serverExpirationTimer, 2000000000L, serverPR);
        serverPR.subscribeToPackets(serverPM);
        serverExpirationTimer.subscribe(serverPM);
        serverMessages = new ServerMessageManager(serverPM);
        serverPM.subscribeToCompletePacketGroups(serverMessages);
        serverMessages.subscribe(this);
        
        PacketReceiver clientPR = new PacketReceiver(clientSocket, PACKET_SIZE);
        Timer<String> clientExpirationTimer = new Timer<>();
        clientExpirationTimer.start();
        
        PacketManager clientPM = new PacketManager(clientSocket, PACKET_SIZE, clientExpirationTimer, 2000000000L, clientPR);
        clientPR.subscribeToPackets(clientPM);
        clientExpirationTimer.subscribe(clientPM);
        
        clientMessages = new ClientMessageManager(clientPM);
        clientPM.subscribeToCompletePacketGroups(clientMessages);
        clientMessages.subscribe(this);
      }
      UUIDWrapper id = new UUIDWrapper(UUID.randomUUID());
      for (SessionEndpoint endpoint : serverConnection.getEndpoints()) {
        HandShakeMessage msg = new HandShakeMessage(id, "server");
        serverMessages.send(msg, endpoint.ip(), endpoint.port());
      }
      view.serverUpdate(true, "HandShakeMessages sent to server.");
    } else {
      view.serverUpdate(false, "Server connected but something went wrong. Error Code: CLI-CONTR-CON-TO-SER");
    }
  }
  
  /**
   * Closes all sockets and marks clients and sever as disconnected/offline.
   */
  public void disconnectAll(){
    if(clientMessages != null)
      clientMessages.kill();
    if(serverMessages != null)
      serverMessages.kill();
    if(currentClients != null)
      currentClients.clear();
    if(healthCheckMngr != null)
      healthCheckMngr.kill();
  }

  private void sendConnectPackets(Client client, UUIDWrapper chatID) {
    for (SessionEndpoint endpoint : client.getEndpoints()) {
      HandShakeMessage msg = new HandShakeMessage(chatID, username);
      clientMessages.send(msg, endpoint.ip(), endpoint.port());
    }
  }

  ////////////////////////////////////////////////
  ////                                        ////
  ////   Interface - Health Check Subscriber  ////
  ////                                        ////
  ////////////////////////////////////////////////  

  /**
   * Gets a client with the passed username and sets them as disconnected.
   * @param name The username of the client that ran out of lives.
   */
  @Override
  public void outOfLives(String name) {
    Client client = currentClients.getClient(name);
    if (client != null) {
      client.disconnected();
    }
  }

  /**
   * Sends a health check to the passed username.
   * If the username is "server" it is sent to the server.
   * @param name The username of the client to send to.
   */
  @Override
  public void sendHealthCheck(String name) {
    Client client = null;
    if (!"server".equals(name)) {
      client = currentClients.getClient(name);
    }
    if (client != null && client.isConnected()) {
      HealthCheckMessage msg = healthCheckMngr.createHealthCheckMessage(new UUIDWrapper(UUID.randomUUID()), name);
      clientMessages.sendOnce(msg, client.getAddress(), client.getPort());
    } else if ("server".equals(name) && serverConnection.isConnected()) {
      HealthCheckMessage msg = healthCheckMngr.createHealthCheckMessage(new UUIDWrapper(UUID.randomUUID()), name);
      serverMessages.sendOnce(msg, serverConnection.getAddress(), serverConnection.getPort());
    }
  }
  ////////////////////////////////////////////////
}
