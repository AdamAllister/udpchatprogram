/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.client;

import au.id.adamallister.connection.ClientManager;
import au.id.adamallister.connection.Client;
import au.id.adamallister.message.messages.ChatMessage;
import au.id.adamallister.util.UUIDWrapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Client Package - Chat - Keeps track of messages between users and users part
 * of the chat.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 19-April-2016 
 * Date Modified: 01-July-2016
 */
public class Chat {

  /**
   * Clients in this chat.
   */
  private final ClientManager clients;
  
  /**
   * Messages sent and received in chat.
   */
  private final ArrayList<ChatMessage> chat;
  
  /**
   * The unique identifier of the chat.
   */
  private final UUIDWrapper chatID;

  /**
   * Constructor. Requires a list of clients in the chat and a unique id for the
   * chat.
   * @param aClients - Clients in the chat.
   * @param chatMsgs - A list of chat messages or an empty list.
   * @param aChatID - Unique identifier of the chat.
   */
  public Chat(ClientManager aClients, ArrayList<ChatMessage> chatMsgs, UUIDWrapper aChatID) {
    chatID = aChatID;
    clients = aClients;
    chat = chatMsgs;
  }
  
  /**
   * Adds a new client to the chat that will receive and send messages to all in
   * the chat.
   * @param client - the new client
   */
  public void addClient(Client client) {
    clients.addClient(client);
    Date date = new Date();
    ChatMessage connect = new ChatMessage(null, "Notice", chatID, date.getTime(), 
        client.getUsername() + " has connected.");
    addMessage(connect);
  }

  /**
   * Removes a client who left the chat. Cannot send of receive messages from
   * chat.
   * @param client - the client to remove
   */
  public void removeClient(Client client) {
    clients.removeClient(client);
    Date date = new Date();
    ChatMessage disconnect = new ChatMessage(null, "Notice", chatID, date.getTime(), 
        client.getUsername() + " has left the chat.");
    addMessage(disconnect);
  }
  
  /**
   * 
   * @param client 
   */
  public void disconnectedClient(Client client){
    if(clients.hasClient(client)) {
      Date date = new Date();
      ChatMessage disconnect = new ChatMessage(null, "Notice", chatID, date.getTime(), 
        client.getUsername() + " has disconnected.");
      addMessage(disconnect);
    }
  }
  
  /**
   * Returns this chats client manager
   * @return
   */
  public ClientManager getClientManager(){
    return clients;
  }
  
  /**
   * Returns a list of clients in the chat.
   * @return an ArrayList of clients in the chat.
   */
  public ArrayList<Client> getClients() {
    ArrayList<Client> clientList = new ArrayList<>();
    clientList.addAll(clients.getClients());
    return clientList;
  }

  /**
   * Returns a string representation of the client list
   * @return - a comma separated list of clients
   */
  public String getClientsAsString() {
    String clientListString = "";
    ArrayList<Client> clientList = getClients();
    if(clientList.size() > 0){
      for(int i = 0; i < clientList.size(); i++){
        clientListString += clientList.get(i).getUsername();
        if(i+1 != clientList.size())
          clientListString += ", ";
      }
    }
    return clientListString;
  }
  
  /**
   * Returns the name representation of the chat.
   * @return if there are no client it returns an empty string, if there is one 
   * client it returns that clients name, and if there is many clients it returns 
   * them in the format "[client 1 name] + [number of other clients] Others"
   */
  public String getName(){
    String name = "";
    ArrayList<Client> clientList = getClients();
    if(clientList.size() > 1)
      name = clientList.get(0).getUsername() + " + " + (clientList.size() - 1) + " Others";
    else if (clientList.size() == 1)
      name = clientList.get(0).getUsername();
    return name;
  }
  
  /**
   * Adds a new message to the chat.
   * Places it based on its timestamp.
   * @param newMsg - A chat message
   */
  public void addMessage(ChatMessage newMsg) {
    if (newMsg.getChatID() == chatID) {
      chat.add(newMsg);
    }
  }
  
  /**
   * Returns a UUIDWrapper identifying the chat
   * @return 
   */
  public UUIDWrapper getID(){
    return chatID;
  }
  
  /**
   * Returns a read only list of chat messages.
   * @return 
   */
  public final List<ChatMessage> getMessages(){
    return Collections.unmodifiableList(chat);
  }
}
