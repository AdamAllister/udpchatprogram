/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */

package au.id.adamallister.connection;

import java.net.InetAddress;

/**
 * Connection Package - Session Endpoint - An object representing a connection 
 * endpoint.
 * Is a pair with an address and a port.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 25-April-2016
 * Date Modified: 01-July-2016
 */
public class SessionEndpoint {

    private final InetAddress ip;
    private final int port;

  /**
   * Constructor - Sets the values of the pair (Address and Port).
   * Both properties are final.
   * @param aIP
   * @param aPort
   */
  public SessionEndpoint(InetAddress aIP, int aPort)
    {
        ip   = aIP;
        port = aPort;
    }

  /**
   * Returns the address of the endpoint.
   * @return
   */
  public InetAddress ip()   { return ip; }

  /**
   * Returns the port of the endpoint.
   * @return
   */
  public int port() { return port; }

}
