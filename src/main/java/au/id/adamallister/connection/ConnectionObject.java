/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.connection;

import java.net.InetAddress;
import java.util.ArrayList;

/**
 * Connection Package - Connection Object - An object that has endpoints and can connect
 * to one of those endpoints.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 24-Jun-2016
 * Date Modified: 01-Jul-2016
 */
public abstract class ConnectionObject {

  /**
   * An array of possible endpoints for the connection.
   */
  protected ArrayList<SessionEndpoint> endpoints;

  /**
   * The endpoint of the current active connection.
   */
  protected SessionEndpoint activeEndpoint;
  private boolean connected;

  /**
   * Constructor - Sets the possible endpoints and defaults other values to the
   * equivalent of no connection.
   *
   * @param aEndpoints
   */
  public ConnectionObject(ArrayList<SessionEndpoint> aEndpoints) {
    endpoints = aEndpoints;
    connected = false;
    activeEndpoint = null;
  }

  /**
   * Returns the connection status of the connection object.
   *
   * @return true if connected.
   */
  public boolean isConnected() {
    return connected;
  }

  /**
   * Sets the connection status of the connection object as disconnected. This
   * includes clearing the current active endpoint and setting the connected
   * field as false.
   */
  public void disconnected() {
    activeEndpoint = null;
    connected = false;
  }

  /**
   * Looks for the passed endpoint and if found sets it as the active endpoint
   * and marks the object as connected. If the endpoint is invalid, the active
   * connection is disconnected (if its connected at all).
   *
   * @param address The address of the active connection.
   * @param port The port of the active connection.
   */
  public void connected(InetAddress address, int port) {
    SessionEndpoint active = null;
    for (SessionEndpoint endpoint : endpoints) {
      if (endpoint.ip() == address && endpoint.port() == port) {
        active = endpoint;
        break;
      }
    }
    if (active != null) {
      activeEndpoint = active;
      connected = true;
    } else {
      disconnected();
    }
  }

  /**
   * Goes through client endpoints
   *
   * @param address
   * @param port
   * @return
   */
  public boolean hasEndpoint(InetAddress address, int port) {
    for (SessionEndpoint endpoint : endpoints) {
      if (endpoint.ip() == address && endpoint.port() == port) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns whether the passed endpoint is the active endpoint of the
   * connection object.
   *
   * @param address the address to be compared.
   * @param port the port to be compared.
   * @return true if the active endpoint is not null and has the same address
   * and port.
   */
  public boolean isActiveEndpoint(InetAddress address, int port) {
    return activeEndpoint == null ? false : activeEndpoint.ip() == address && activeEndpoint.port() == port;
  }

  /**
   * Returns a list of the connection objects possible endpoints
   *
   * @return An ArrayList if set, or null.
   */
  public ArrayList<SessionEndpoint> getEndpoints() {
    return endpoints;
  }

  /**
   * Returns the address of the active endpoint.
   *
   * @return The address of the active endpoint, or null if not connected.
   */
  public InetAddress getAddress() {
    return (activeEndpoint == null) ? null : activeEndpoint.ip();
  }

  /**
   * Returns the port of the active endpoint.
   *
   * @return The port of the active endpoint, or null if not connected.
   */
  public int getPort() {
    return (activeEndpoint == null) ? null : activeEndpoint.port();
  }

}
