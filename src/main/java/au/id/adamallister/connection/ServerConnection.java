/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.connection;

import java.util.ArrayList;

/**
 * Connection Package - Server Connection - A connection object that represents 
 * a server connection.
 * NOTE: Currently does not provide additional functionality and exists purely 
 * for the possibility of future requirements.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 11-May-2016 
 * Date Modified: 01-July-2016
 */
public class ServerConnection extends ConnectionObject{

  public ServerConnection(ArrayList<SessionEndpoint> aEndpoints) {
    super(aEndpoints);
  }
}
