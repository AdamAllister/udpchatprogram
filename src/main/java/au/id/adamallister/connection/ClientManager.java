/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.connection;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Connection Package - ClientManager - Keeps track of messages between users and users part
 * of the chat.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 05-May-2016 
 * Date Modified: 01-July-2016
 */
public class ClientManager {

  private HashMap<String, Client> clients;

  /**
   * Constructor - Initializes the Client Manager.
   */
  public ClientManager() {
    clients = new HashMap<>();
  }

  /**
   * Returns a client with the given username.
   * Gets a client from a hash map whose key is the clients username.
   * @param username the username of the client.
   * @return the client if found, or null.
   */
  public Client getClient(String username) {
    Client client = clients.get(username);
    return client;
  }

  /**
   * Returns a client that has a connection on the given address and port.
   * The passed endpoint parameters must be an active endpoint to be found.
   * @param address the clients active endpoint address.
   * @param port the clients active endpoint port.
   * @return the client if found, or null.
   */
  public Client getClient(InetAddress address, int port) {
    for (Client client : clients.values()) {
      if (client.isActiveEndpoint(address, port)) {
        return client;
      }
    }
    return null;
  }

  /**
   * Returns true if a client who has an active endpoint with the address and
   * port is found.
   * @param address the clients active endpoint address.
   * @param port the clients active endpoint port.
   * @return true if client is found.
   */
  public boolean hasClient(InetAddress address, int port) {
    return getClient(address, port) != null;
  }

  /**
   * Returns true if a client object is found in the client manager.
   * 
   * @param client The client object to find.
   * @return true if the client object is in the manager.
   */
  public boolean hasClient(Client client) {
    return clients.values().contains(client);
  }

  /**
   * Returns true if a client with the given username is found.
   *
   * @param username The username to find.
   * @return true if a client has the username.
   */
  public boolean hasClient(String username) {
    return clients.containsKey(username);
  }

  /**
   * Returns a list of the clients.
   *
   * @return a list of client objects
   */
  public Collection<Client> getClients() {
    return clients.values();
  }

  /**
   * Adds all clients passed to the client manager.
   * Does not add clients that are already in the manager.
   * @param aClients The clients to add.
   */
  public void addClients(ArrayList<Client> aClients) {
    for (Client client : aClients) {
      addClient(client);
    }
  }

  /**
   * Add client passed to the client manager.
   * Does not add clients that are already in the manager.
   * @param client The client to add.
   * @throws IllegalArgumentException If client is not in the client manager.
   */
  public void addClient(Client client) {
    if (!clients.values().contains(client)) {
      clients.put(client.getUsername(), client);
    } else 
      throw new IllegalArgumentException("Cannot add client to client manager"
            + " that already contains client");
  }

  /**
   * Removes all clients passed to the client manager.
   * @param aClients The clients to remove.
   */
  public void removeClients(ArrayList<Client> aClients) {
    for (Client client : aClients) {
      removeClient(client);
    }
  }

  /**
   * Remove client passed to the client manager.
   *
   * @param client The client to remove.
   * @throws IllegalArgumentException If client is not in the client manager.
   */
  public void removeClient(Client client) {
    if (clients.values().contains(client)) {
        clients.remove(client.getUsername());
      } else 
        throw new IllegalArgumentException("Cannot remove client from client manager"
            + " that is not in client manager");
  }

  /**
   * Removes all clients from the client manager.
   */
  public void clear() {
    for (Client client : clients.values()) {
      client.setOnline(false);
    }
    clients.clear();
  }
}
