/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.connection;

import java.util.ArrayList;

/**
 * Connection Package - Client - A connection object that has a username and and online
 * status.
 * The online status is used to determine if the client is registered on the server.
 * The username of the client is an identifier chosen by the client and registered
 * on the server. It is used as a reference to the client.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created: 11-April-2016 
 * Date Modified: 01-July-2016
 */
public class Client extends ConnectionObject{

  private String username;
  private boolean online;

  /**
   * Constructor - Takes the clients username and an array list of the clients 
   * possible endpoints.
   * Defaults online to false.
   * @param aEndpoints All the possible endpoints a client has.
   * @param aUsername The username of the client.
   */
  public Client(String aUsername, ArrayList<SessionEndpoint> aEndpoints) {
    super(aEndpoints);
    username = aUsername;
    online = false;
  }

  /**
   * Returns the username of the client.
   * @return the clients username.
   */
  public String getUsername() {
    return username;
  }

  /**
   * Sets the username of the client.
   * @param name the new username of the client.
   */
  public void setUsername(String name) {
    username = name;
  }

  /**
   * Sets the online status of the client.
   * If offline the client is disconnected.
   * @param aOnline The online status of the client.
   */
  public void setOnline(boolean aOnline) {
    online = aOnline;
    if (!online) {
      disconnected();
    }
  }

  /**
   * Sets the possible endpoints of the client.
   * Clears all previous endpoints and disconnects client if active endpoint is 
   * no longer in the list of endpoints.
   * @param aEndpoints 
   */
  public void setEndpoints(ArrayList<SessionEndpoint> aEndpoints){
    endpoints = aEndpoints;
    if(activeEndpoint != null && !hasEndpoint(activeEndpoint.ip(), activeEndpoint.port()))
      disconnected();
  }
  
  /**
   * Returns the online status of the client.
   * @return true if client is online.
   */
  public final boolean isOnline() {
    return online;
  }

}
