/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packet Package - PacketReceiver - A listening objects that waits for packets
 * on the socket and passes them to observers.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  25-APR-2016 
 * Date Modified: 21-SEP-2016
 */
public class PacketReceiver extends Thread {
  private static final Logger LOGGER = Logger.getLogger(PacketReceiver.class.getName());
  
  static {
    LOGGER.addHandler(new ConsoleHandler());
  }
  
  /**
   * Constructor.
   * @param aSocket The socket to listen on.
   * @param aPacketSize The size of the incoming packets.
   */
  public PacketReceiver(DatagramSocket aSocket, int aPacketSize) {
    running = true;
    socket = aSocket;
    pSubs = new ArrayList<>();
    packetSize = aPacketSize;
  }

  /**
   * Stops the loop and ends the thread.
   */
  public void kill() {
    running = false;
  }

  /**
   * Gets the state of the thread.
   * @return True if still running.
   */
  public boolean isRunning() {
    return running;
  }

  /**
   * Adds an observer to the packet receiver.
   * Does not add observer if already added.
   * @param sub The observer to add.
   */
  public void subscribeToPackets(DatagramPacketSubscriber sub) {
    synchronized(this){
    if(!pSubs.contains(sub))
      pSubs.add(sub);
    }
  }

  @Override
  public void run() {
    LOGGER.log(Level.INFO, "PacketReceiver::Starting");
    byte[] packetData;
    DatagramPacket dPacket;
    while (running) {
      packetData = new byte[packetSize];
      dPacket = new DatagramPacket(packetData, packetSize);
      try {
        socket.receive(dPacket);
        LOGGER.log(Level.INFO, "PacketReceiver::Packet received from {0}:{1}", new Object[]{dPacket.getAddress().toString(), dPacket.getPort()});
        synchronized(this){
          for (DatagramPacketSubscriber sub : pSubs) {
            sub.processDatagramPacket(dPacket);
          }
        }
      } catch (IOException ex) {
        running = false;
        LOGGER.log(Level.SEVERE, "PacketReceiver::IOException while trying to receive packets", ex);
      }
    }
    if (!socket.isClosed()) {
      socket.close();
      socket = null;
      pSubs.clear();
    }
    LOGGER.log(Level.INFO, "PacketReceiver::Stopping");
  }
  
  private boolean running;
  private DatagramSocket socket;
  private final int packetSize;
  private final ArrayList<DatagramPacketSubscriber> pSubs;
}
