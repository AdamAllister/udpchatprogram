/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

/**
 * Packet Package - PacketGroupSubscriber Interface.
 * Observer Interface for new packet groups.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  06-JUN-2016 
 * Date Modified: 08-JUL-2016
 */
public interface PacketGroupSubscriber {

  /**
   * Process a new packet group.
   * @param pg The packet group to process.
   */
  public void onNewPacketGroup(PacketGroup pg);
}
