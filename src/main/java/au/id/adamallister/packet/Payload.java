/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.util.UUIDWrapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Packet Package - Payload - The data inside a packet.
 * Contains the number of packets to expect, what packet this one is, the id of
 * the packet group, and whether the data has been corrupted.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  19-APR-2016 
 * Date Modified: 06-JUL-2016
 */
public class Payload {

  private int totalPackets;
  private int packetNumber;
  private UUIDWrapper groupID;
  private byte[] data;
  private boolean corrupt;
  
  /**
   * Constructor - Creates a payload object from a udp datagram
   * @param packetBytes The contents of a UDP datagram
   */
  public Payload(byte[] packetBytes) {
    corrupt = false;
    processPacketBytes(packetBytes);
  }

  /**
   * Constructor
   * @param aTotalPackets
   * @param aPacketNumber
   * @param aGroupID
   * @param aData
   */
  public Payload(int aTotalPackets, int aPacketNumber, UUIDWrapper aGroupID, byte[] aData) {
    corrupt = false;
    totalPackets = aTotalPackets;
    packetNumber = aPacketNumber;
    groupID = aGroupID;
    data = aData;
  }

  private void processPacketBytes(byte[] packetBytes) {
    DataInputStream dis = new DataInputStream(new ByteArrayInputStream(packetBytes));
    long gid1;
    long gid2;
    short dataSize;
    try {
      totalPackets = dis.readInt();
      packetNumber = dis.readInt();
      gid1 = dis.readLong();
      gid2 = dis.readLong();
      groupID = new UUIDWrapper(new UUID(gid1, gid2));
      dataSize = dis.readShort();
      if(dataSize > 0) {
        data = new byte[dataSize];
        dis.read(data);
      } else {
        data = null;
      }
    } catch (IOException e) {
      System.err.println("Failed reading packet data");
      corrupt = true;
      totalPackets = 0;
      packetNumber = 0;
      groupID = null;
      data = null;
    }
  }

  /**
   * Gets the total number of packets for the group.
   * @return total number of packets
   */
  public int getTotal() {
    return totalPackets;
  }

  /**
   * Gets the UUID of the group this packet belongs to
   * @return a UUID
   */
  public UUIDWrapper getGroupID() {
    return groupID;
  }
  
  /**
   * Returns the UUID of the group this packet belongs to
   * @return a string representation of a UUID
   */
  public String getGroupIdAsString() {
    return groupID.toString();
  }

  /**
   * Gets the contents of the payload.
   * @return Returns the contents of the packet
   */
  public byte[] getData() {
    return data;
  }

  /**
   * Is the packet corrupt. 
   * @return true if there was a problem reading the datagram
   */
  public boolean isCorrupt() {
    return corrupt;
  }

  /**
   * Gets the order in the packet group this packet is in.
   * @return an Integer representing the packetNumber
   */
  public int getNumber() {
    return packetNumber;
  }

  /**
   * Converts all data into a single byte array.
   * @return a 512 byte array containing all fields.
   */
  public byte[] getBytes() {
    byte[] packet = null;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    try {
      dos.writeInt(totalPackets);
      dos.writeInt(packetNumber);
      dos.writeLong(groupID.getMostSignificantBits());
      dos.writeLong(groupID.getLeastSignificantBits());
      if(data != null) {
        dos.writeShort(data.length);
        dos.write(data);
      } else {
        dos.writeShort(0);
      }
      packet = baos.toByteArray();
    } catch (IOException e) {
      System.err.println("Creating byte array for packet: " + groupID);
      corrupt = true;
    }
    if (packet == null || packet.length > 512) {
      return null;
    }
    return packet;
  }
  
}
