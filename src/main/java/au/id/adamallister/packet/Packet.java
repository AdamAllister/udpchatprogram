/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.net.InetAddress;

/**
 * Packet - Packet - A packet object containing sender or receiver info and the
 * packet contents.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  21-APR-2016 
 * Date Modified: 29-APR-2016
 */
public class Packet {

  private InetAddress ip;
  private final int Port;
  private final Payload payload;

  /**
   * Constructor - Sets IP, port and packet contents.
   * @param aIP - Sender or receiver IP address.
   * @param aPort - Sender or receiver port.
   * @param aPayload - Packet Contents.
   */
  public Packet(InetAddress aIP, int aPort, Payload aPayload) {
    payload = aPayload;
    Port = aPort;
    ip = aIP;
  }

  /**
   * Returns the contents of a packet.
   * @return the packets contents.
   */
  public Payload getPayload() {
    return payload;
  }

  /**
   * Returns the port of the packets sender or the port of the receiver.
   * @return a port.
   */
  public int getPort() {
    return Port;
  }

  /**
   * Returns the IP address of the packets sender or receiver.
   * @return an IP address.
   */
  public InetAddress getIP() {
    return ip;
  }

}
