/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.util.UUIDWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * Packet Package - PacketUtility - Manages the conversion of data into packets.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  21-APR-2016 
 * Date Modified: 07-JUL-2016
 */
public final class PacketUtility {

  /**
   * Puts byte data into a number of packets.
   * Separates a byte array into individual packets.
   * @param messageData The data to put into the packets.
   * @param packetSize The size of each packet
   * @param address The address of the recipient.
   * @param port The port of the recipient.
   * @return A PacketGroup containing all the packets with the data.
   */  
  public static PacketGroup generatePacketGroup(byte[] messageData, int packetSize, InetAddress address, int port) {
    int dataSize = packetSize - 26;
    int nPackets = (int) Math.ceil((float)messageData.length / (float)dataSize);
    int finalPacketSize = (messageData.length - ((nPackets - 1) * dataSize));


    UUIDWrapper packetGroupID = new UUIDWrapper(UUID.randomUUID());
    byte[] data;

    PacketGroup packetGroup = new PacketGroup(nPackets, packetGroupID, address, port);

    for (int packetN = 1; packetN <= nPackets; packetN++) {
      if (nPackets != packetN) {
        data = ByteBuffer.allocate(dataSize).put(messageData, ((packetN - 1) * dataSize), dataSize).array();
      } else {
        data = ByteBuffer.allocate(dataSize).put(messageData,((packetN - 1) * dataSize), finalPacketSize).array();
      }
      packetGroup.addPayload(new Payload(nPackets, packetN, packetGroupID, data));
    }

    return packetGroup;
  }
}
