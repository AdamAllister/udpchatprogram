/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.util.timer.Timer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import au.id.adamallister.util.timer.DoTaskSubscriber;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packet Package - PacketManager - Manages receiving and sending of
 * packets over UDP and resending of lost packets.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  21-APR-2016 
 * Date Modified: 21-SEP-2016
 */
public class PacketManager implements DoTaskSubscriber<String>, DatagramPacketSubscriber {
  private static final Logger LOGGER = Logger.getLogger(PacketManager.class.getName());
  
  static {
    LOGGER.addHandler(new ConsoleHandler());
  }
  
  /**
   * Constructor.
   *
   * @param aClientsSocket Datagram Socket to listen on
   * @param aPacketSize Size of datagram packets contents
   * @param timer A packet response timeout timer.
   * @param packetTimeout The amount of time to wait before packet timeout.
   * @param receiver The Packet Receiver that will listen for packets.
   */
  public PacketManager(DatagramSocket aClientsSocket, int aPacketSize, Timer<String> timer, long packetTimeout, PacketReceiver receiver) {
    clientsSocket = aClientsSocket;
    packetGroups = new HashMap<>();
    pAwaitingReply = new HashMap<>();
    completePacketSubscribers = new ArrayList<>();
    completePGroups = Collections.synchronizedSet(new HashSet<>());
    pSize = aPacketSize;
    pReceiver = receiver;
    pTimer = timer;
    this.packetTimeout = packetTimeout;
  }

  /**
   * Gets the running state of the Packet Manager.
   *
   * @return True if the receiver is running and the socket is still open.
   */
  public boolean isRunning() {
    return (pReceiver != null && pReceiver.isRunning()) && (clientsSocket != null && !clientsSocket.isClosed());
  }

  /**
   * Kills and nullifies any threads and sockets.
   */
  public void kill() {
    LOGGER.log(Level.INFO, "Packet Manager::Killing");
    if (clientsSocket != null) {
      clientsSocket.close();
      clientsSocket = null;
    }
    if (pTimer != null) {
      pTimer.kill();
      pTimer = null;
    }
    if (pReceiver != null) {
      pReceiver.kill();
      pReceiver = null;
    }
  }

  /**
   * Sends all packets in a packet group.
   *
   * @param pg The packet group to send.
   */
  public void send(PacketGroup pg) {
    Packet[] packets = pg.getPackets();
    LOGGER.log(Level.INFO, "Packet Manager::Sending packet group to {0}:{1}", new Object[]{pg.getAddress().toString(), pg.getPort()});
    for (Packet packet : packets) {
      send(packet);
    }
  }

  /**
   * Sends a packet. Creates a datagram packet object and fills it with packet
   * data and padding.
   *
   * @param packet The packet to send.
   */
  public void send(Packet packet) {
    byte[] sendData = packet.getPayload().getBytes();
    byte[] paddedData = new byte[pSize];
    System.arraycopy(sendData, 0, paddedData, 0, sendData.length);
    DatagramPacket dPacket = new DatagramPacket(paddedData, paddedData.length);
    dPacket.setAddress(packet.getIP());
    dPacket.setPort(packet.getPort());
    synchronized (this) {
      try {
        clientsSocket.send(dPacket);
        pAwaitingReply.put(packet.getPayload().getGroupIdAsString() + packet.getPayload().getNumber(), packet);
        LOGGER.log(Level.INFO, "Packet Manager::Packet Sent to {0}:{1}", new Object[]{dPacket.getAddress().toString(), dPacket.getPort()});
        if (pTimer != null && pTimer.isRunning()) {
          pTimer.addItem(packet.getPayload().getGroupIdAsString() + packet.getPayload().getNumber(), packetTimeout);
        }
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, "Packet Manager::Could not send message to " + dPacket.getAddress().toString() + ":" + dPacket.getPort() + "::Killing packet manager", e);
        this.kill();
      }
    }
  }

  /**
   * Sends a packet once. Sends and forgets, it does not wait for a response or
   * timeout.
   *
   * @param pg The packet group to send.
   */
  public void sendOnce(PacketGroup pg) {
    Packet[] packets = pg.getPackets();
    for (Packet packet : packets) {
      synchronized (this) {
        byte[] sendData = packet.getPayload().getBytes();
        byte[] paddedData = new byte[pSize];
        System.arraycopy(sendData, 0, paddedData, 0, sendData.length);
        DatagramPacket dPacket = new DatagramPacket(paddedData, paddedData.length);
        dPacket.setAddress(packet.getIP());
        dPacket.setPort(packet.getPort());
        try {
          clientsSocket.send(dPacket);
          LOGGER.log(Level.INFO, "Packet Manager::Packet Sent to {0}:{1}", new Object[]{dPacket.getAddress().toString(), dPacket.getPort()});
        } catch (IOException e) {
          LOGGER.log(Level.SEVERE, "Packet Manager::Could not send message to " + dPacket.getAddress().toString() + ":" + dPacket.getPort() + "::Killing packet manager", e);
          this.kill();
        }
      }
    }
  }

  /**
   * Processes a Datagram Packet received by the Packet Receiver.
   * @param receivedPacket The packet to process.
   */
  @Override
  public void processDatagramPacket(DatagramPacket receivedPacket) {
    Payload payload = new Payload(receivedPacket.getData());
    Packet packet = new Packet(receivedPacket.getAddress(), receivedPacket.getPort(), payload);
    String ipAddress = packet.getIP().toString();

    PacketGroup group;

    synchronized (this) {
      if (!payload.isCorrupt()) {
        if ((group = packetGroups.get(payload.getGroupIdAsString())) != null) {
          group.addPacket(packet);
          sendConfirmation(packet);
        } else if (pAwaitingReply.get(payload.getGroupIdAsString() + payload.getNumber()) != null) {
          pAwaitingReply.remove(payload.getGroupIdAsString() + payload.getNumber());
        } else if (completePGroups.contains(payload.getGroupIdAsString())) {
          sendConfirmation(packet);
        } else if (payload.getData() != null) {
          group = new PacketGroup(packet);
          packetGroups.put(payload.getGroupIdAsString(), group);
          sendConfirmation(packet);
        }

        if (group != null && group.isComplete()) {
          notifyCompletePacketGroupSubscribers(group);
          packetGroups.remove(payload.getGroupIdAsString());
          LOGGER.log(Level.INFO, "Packet Manager::Packet Group Completed for {0}", group.getGroupIDAsString());
        }
      }
    }
  }

  /**
   * Adds an observer to the subscriber list, as long as they are not already in
   * it.
   * @param pgs The observer to add.
   */
  public void subscribeToCompletePacketGroups(PacketGroupSubscriber pgs) {
    if (!completePacketSubscribers.contains(pgs)) {
      completePacketSubscribers.add(pgs);
    }
  }

  /**
   * Gets the number of packets awaiting reply.
   * @return An integer of the number of packets awaiting reply.
   */
  public synchronized int getWaitingCount() {
    return pAwaitingReply.size();
  }

  /**
   * Processes expired packets.
   * @param exp An ArrayList of expired packets.
   */
  @Override
  public void doTask(ArrayList<String> exp) {
    for (String packetKey : exp) {
      Packet packet;
      if ((packet = pAwaitingReply.get(packetKey)) != null) {
        pAwaitingReply.remove(packetKey);
        LOGGER.log(Level.INFO, "Packet Manager::Packet Expired {0}", packetKey);
        send(packet);
      }
    }
  }
  
  /**
   * The size of each datagram packet contents.
   */
  public final int pSize;
  
  private void notifyCompletePacketGroupSubscribers(PacketGroup completeGroup) {
    for (PacketGroupSubscriber sub : completePacketSubscribers) {
      sub.onNewPacketGroup(completeGroup);
    }
  }
  
  private void sendConfirmation(Packet packet) {
    Payload pl = new Payload(packet.getPayload().getTotal(), packet.getPayload().getNumber(), packet.getPayload().getGroupID(), null);
    Packet okPacket = new Packet(packet.getIP(), packet.getPort(), pl);   LOGGER.log(Level.INFO, "Packet Manager::Sending confirmation packet for {0}", okPacket.getPayload().getGroupIdAsString());
    send(okPacket);
    if (pTimer != null && pTimer.isRunning()) {
      pTimer.addItem(packet.getPayload().getGroupIdAsString() + packet.getPayload().getNumber(), packetTimeout);
    }
  }
  
  private DatagramSocket clientsSocket;
  private HashMap<String, Packet> pAwaitingReply;
  private Set<String> completePGroups;
  private HashMap<String, PacketGroup> packetGroups;
  private ArrayList<PacketGroupSubscriber> completePacketSubscribers;
  private Timer<String> pTimer;
  private PacketReceiver pReceiver;
  private final long packetTimeout;
}
