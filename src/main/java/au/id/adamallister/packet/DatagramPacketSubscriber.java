/*
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import java.io.IOException;
import java.net.DatagramPacket;

/**
 * Packet Package - DatagramPacketSubscriber Interface.
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  31-MAY-2016
 * Date Modified: 06-JUL-2016
 */
public interface DatagramPacketSubscriber {

  /**
   * Process a Datagram Packet.
   * @param packet The datagram packet to process
   * @throws IOException If a response to the packet fails.
   */
  public void processDatagramPacket(DatagramPacket packet) throws IOException;
}
