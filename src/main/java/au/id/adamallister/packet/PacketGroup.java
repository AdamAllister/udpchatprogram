/**
 * Copyright 2016 Adam Allister <adam.allister@outlook.com>.
 */
package au.id.adamallister.packet;

import au.id.adamallister.util.UUIDWrapper;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 *
 * @author Adam Allister <adam.allister@outlook.com>
 * Date Created:  19-APR-2016 
 * Date Modified: 07-JUL-2016
 */
public class PacketGroup {

  private final Payload[] payloads;
  private final UUIDWrapper groupID;
  private final InetAddress ip;
  private final int port;
  private final int totalPackets;

  /**
   * Constructor - Initializes a packet group based on information from a packet.
   *
   * @param packet
   */
  public PacketGroup(Packet packet) {
    ip = packet.getIP();
    port = packet.getPort();
    totalPackets = packet.getPayload().getTotal();
    payloads = new Payload[totalPackets];
    groupID = packet.getPayload().getGroupID();
    addPacket(packet);
  }

  /**
   * Constructor.
   * @param nPackets The number of packets in the group.
   * @param aGroupId The UUID of the group.
   * @param aIP The address of the sender or recipient.
   * @param aPort The port of the sender or recipient.
   */
  public PacketGroup(int nPackets, UUIDWrapper aGroupId, InetAddress aIP, int aPort) {
    ip = aIP;
    port = aPort;
    totalPackets = nPackets;
    payloads = new Payload[totalPackets];
    groupID = aGroupId;

  }

  /**
   * Checks if all packets have been received
   * @return true if all packets exist
   */
  public boolean isComplete() {
    for (Payload payload : payloads) {
      if (payload == null) {
        return false;
      }
    }
    return true;
  }

  /**
   * Adds a packet to the group. Checks for corrupt packet and that the packet
   * belongs to the group and does not already exist.
   *
   * @param packet the packet to add
   * @return true if the packet was added or has already been added
   */
  public final boolean addPacket(Packet packet) {
    if (packet.getPayload().isCorrupt()) {
      return false;
    }
    if (packet.getPayload().getGroupID().equals(groupID) && packet.getPayload().getNumber() <= totalPackets && 
        payloads[packet.getPayload().getNumber() - 1] == null){
      payloads[packet.getPayload().getNumber() - 1] = packet.getPayload();
      return true;
    }
    return false;
  }
  
  /**
   * Adds a payload to the packet group.
   * @param payload The payload to add.
   * @return True if successfully added.
   */
  public final boolean addPayload(Payload payload) {
    if (payload.isCorrupt()) {
      return false;
    }
    if (payload.getGroupID().equals(groupID) && payload.getNumber() <= totalPackets && 
        payloads[payload.getNumber() - 1] == null) {
      payloads[payload.getNumber() - 1] = payload;
      return true;
    }
    return false;
  }
  
  /**
   * Assembles all packet data into a single byte array and returns it.
   * @return the combination of all the packets data
   */
  public byte[] getCompleteData() {
    if (isComplete()) {
      byte[] fullData = null;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      DataOutputStream dos = new DataOutputStream(baos);
      try {
        for (Payload payload : payloads) {
          dos.write(payload.getData());
        }
        fullData = baos.toByteArray();
      } catch (IOException e) {
        System.err.println("Could not combine packet data into single group: " + groupID);
      }
      if (fullData != null) {
        return fullData;
      }
    }
    
    return null;
  }

  /**
   * Get the packets in the packet group.
   * @return An array of packets.
   */
  public Packet[] getPackets() {
    Packet[] packets = new Packet[payloads.length];
    for(int i = 0; i < packets.length; i++) {
      packets[i] = new Packet(ip, port, payloads[i]);
    }
    return packets;
  }
  
  /**
   * Gets the UUID of the group.
   * @return A UUID.
   */
  public UUIDWrapper getGroupID(){
    return groupID;
  }
  
  /**
   * Gets the UUID of the group as a String.
   * @return A String representation of a UUID.
   */
  public String getGroupIDAsString(){
    return groupID.toString();
  }
  
  /**
   * Gets the address of the sender or recipient
   * @return An InetAddress of the sender or recipient
   */
  public InetAddress getAddress(){
    return ip;
  }
  
  /**
   * Gets the port of the sender or recipient
   * @return An integer representing the sender or recipient's port.
   */
  public int getPort(){
    return port;
  }
}
